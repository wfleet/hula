'use strict';
/**
 * <h4>Chain<hr></h4>
 *
 * The core object for the Anklet's Chain operation
 * <ul>
 *      <li>Anklet allows one to to arrange software components, called Links, in a Chain.
 *      <li>Chains are strings of instantiated Link components.
 *      <li>Once arranged, a Chain's Links are called on to fetch resources, and then render the final output.
 * </ul>
 *
 * @example
 * // init the chain - chainData is JSON chain descriptor
 * let thisChain = Anklet.Chain.init(chainData);
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Chain
 * @version 0.1
 * @type {{workspace: {}, init: Chain.init, fetchAll: Chain.fetchAll, renderAll: Chain.renderAll, gatherAll: Chain.gatherAll, getLinkById: Chain.getLinkById, getSegments: Chain.getSegments}}
 */

const Chain = {

    /**
     * <h4>workspace<hr></h4>
     * <p>holder for common Chain variables and objects</p>
     * @type {{}}
     */
    workspace: {},

	/**
	 * <h4>init()<hr></h4>
	 *
	 * <p>The startup function for a new Chain. Takes the data object, and builds it out
	 * into the head and list areas, defining Links and Link operations</p>
	 *
	 * @param data {Object} data a JSON object string from dispatch
	 * @returns {Chain}
	 */
	init: function(data) {
	    const extend = require('util')._extend;
		let self = this,
            chainObject = {},
			dataLink,
			thisLink;

		self.workspace = {};
		chainObject.header = extend({}, data.header);

        chainObject.links = [];
		for (dataLink of data.links) {
			thisLink = Anklet.LinkManager.loadLink(dataLink);
            chainObject.links.push(thisLink);
		}
		self.workspace.chainObject = chainObject;

		return self;
	},

    /**
     * <h4>fetchAll()<hr></h4>
     *
     * <ul>
     *     <li>calls all of the fetch promises for the links in the chain
     *     <li>returns an array of the promises, and sends them to dispatch
     * </ul>
     *
     * @param thisSegment - an array of function calls that include the link and the name of the function to invoke
     * @param workspace - the general workspace object
     * @returns {Array} array of Promises
     */
	fetchAll: function (thisSegment, workspace) {

	    const extend = require('util')._extend;
		let self = this,
			thisLink,
            newLink,
            thisLinkId,
            linkCall,
			functionList = [];

        if (!workspace.output) {
            workspace.output = {};
        }

         // promises: fetch the data for all the links
		for (thisLinkId of thisSegment) {
            thisLink = this.getLinkById(workspace, thisLinkId);
            newLink = {...thisLink};
            // check the exec flag
            if (newLink.exec !== false && newLink.exec !== 'false') {

                try {
                    Anklet.service.log('call', 'stored call ' + thisLink.id + ':fetch ' + thisLink.link + ' - ' + thisLink.title);
                    linkCall = {
                        'exec': 'fetch',
                        'module' : newLink
                    };
                    functionList.push(linkCall);
                }
                catch(error) {
                    Anklet.service.log('error', error);
                }

            }
            else {
                Anklet.service.log('call', thisLink.id + ':fetch - NOT fetching');
            }
		}
		return functionList;
	},

    /**
     * <h4>renderAll()<hr></h4>
     *
     * <ul>
     *     <li>once the Chain is initialized, its render function can be called
     *     <li>render will call each Link's render function in order, saving each output
     *     <li>the total output is concatenated in order by the gatherAll method below
     * </ul>
     *
     * @param thisSegment
     * @param workspace
     * @returns {Array}] the rendered output in Promise form
     */
	renderAll: function(thisSegment, workspace) {

	    const extend = require('util')._extend;
		let self = this,
			thisLink,
            newLink,
            thisLinkId,
            linkCall,
			functionList = [];

        if (!workspace.output) {
            workspace.output = {};
        }

        for (thisLinkId of thisSegment) {
            thisLink = this.getLinkById(workspace, thisLinkId);
            newLink = {...thisLink};
            // check the exec flag
            if (newLink.exec !== false && newLink.exec !== 'false') {

                try {

                    // TODO: do we even need newLink anymore? might also reference from workspace
                    linkCall = {
                        'exec': 'render',
                        'module' : newLink
                    };
                    functionList.push(linkCall);
                    Anklet.service.log('call', 'stored call ' + thisLink.id + ':render ' + thisLink.link + ' - ' + thisLink.title);

                }
                catch (error) {
                    Anklet.service.log('error', error);
                }

            }
            else {
                Anklet.service.log('call', thisLink.id + ':render - NOT rendering');
            }
		}
		return functionList;
	},

    /**
     * <h4>gatherAll()<hr></h4>
     *
     * <p>concatenates all of the output from the rendered links, in order</p>
     *
     * @param workspace
     * @returns {*}
     */
	gatherAll: function(workspace) {
		let self = this,
            chain = workspace.chainObject,
			thisLink,
			collectOut = [];

		if (workspace.outputStream) {
		    return workspace.outputStream;
        }

		if (!workspace.output) {
            workspace.output = {};
        }

		for (thisLink of chain.links) {
			console.log('gathering ' + thisLink.id);
			if (workspace.output[thisLink.id]) {
                collectOut.push(workspace.output[thisLink.id]);
            }
		}
        workspace.outputStream = collectOut.join('');
		return workspace;
	},

    /**
     * <h4>getLinkById()<hr></h4>
     *
     * <p>returns the individual Link object by id</p>
     *
     * @param workspace
     * @param id
     * @returns {*}
     */
	getLinkById: function(workspace, id) {
		let chain = workspace.chainObject,
			thisLink;

		for (thisLink of chain.links) {
			if (thisLink.id == parseInt(id)) {
				return thisLink;
			}
		}
	},

    /**
     * <h4>getSegments()<hr></h4>
     * <p>breaks the list of links up into executable segments, then returns a listing of them as an array of arrays</p>
     * <ul>
     *      <li>starts from the begging and moves to the end of the link list
     *      <li>links marked 'true' are included
     *      <li>links marked 'false' are not included
     *      <li>links marked 'wait' will begin a new segment
     *      <li>only link ids are listed
     * </ul>
     *
     * @param thisChain
     * @returns {Array}
     */
    getSegments: function(workspace) {

        let thisChain = workspace.chainObject,
            theseLinks = thisChain.links,
            thisLink,
            thisExec,
            theseSegments = [],
            thisSegment = [];

        for (thisLink of theseLinks) {
            // resolve any references
            thisExec = Anklet.LinkManager.resolveValue(thisLink.exec, workspace);

            if (thisExec === true || thisExec === 'true') {
                // include this link in the segment
                thisSegment.push(thisLink.id);
            }
            else if (thisLink.exec && thisLink.exec === 'wait') {
                // end segment and start a new one
                theseSegments.push(thisSegment);
                thisSegment = [];
                thisSegment.push(thisLink.id);
            }
        }
        theseSegments.push(thisSegment);

	    return theseSegments;
    }


};

module.exports.Chain = Chain;
