'use strict';
/**
 * <h4>Link<hr></h4>
 *
 * <p>the module descriptor for a basic Chain Link. is used to create Link objects as needed from dispatch data
 * and a Link descriptor.</p>
 *
 * <p>The Link object here is meant to be extended, first by the Core Link, then by the
 * Link type script from the /links folder, then by the dispatch data to instantiate a Chain Link
 * that can be rendered.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link
 * @version 0.1
 * @type {{exec: boolean, init: Link.init, fetch: Link.fetch, render: Link.render, getTable: Link.sideFetch, sideRender: Link.sideRender}}
 */

const Link = {

	/**
	 * <h4>exec<hr></h4>
	 * <p>a boolean check to see if we run this Link or not</p>
	 * <ul>
	 *      <li>defaults to true, so the link is run
	 *      <li>can be set by condition, so that it can be optional
	 *      <li>can be set to a workspace variable, which would resolve to a boolean value
	 * </ul>
	 * @type boolean
	 */
	exec: true,

	/**
	 * <h4>init()<hr></h4>
	 *
	 * <p>this takes the dispatch data (or linkData), and news up the Link object from the Link module.</p>
	 * <p>I should probably remove this, as it's mostly handled by LinkManager now</p>
	 *
	 * @param data
	 * @returns {{}}
	 */
	init: function(data) {
		const extend = require('util')._extend;
		let self = this,
			thisLink = extend({}, self);

		thisLink = extend(thisLink, data);

		return thisLink;
	},

    /**
     * <h4>fetch()<hr></h4>
     * <ul>
     *     <li>called to get any needed resources from the net or local source
     *     <li>this is called by the Chain's <tt>fetchAll()</tt> function
     *     <li>returns either a Promise or null, if a stub
     *     <li>this is s stub - if a subsequent Link needs to fetch something, it can override it
     *     <li>Note: getting a net resource is most easily done by calling <tt>getPromisedResource()</tt> below
     * </ul>
     *
     * @param workspace
     */
	fetch: function(workspace) {
	    return;
	},

    /**
     * <h4>render()<hr></h4>
     *
     * <ul>
     *     <li>called to render final output for the Link in question
     *     <li>this is called by the Chain's <tt>renderAll()</tt> function
     *     <li>returns output or an empty string
     *     <li>this is a stub - subsequent Links must override it to generate output
     * </ul>
     *
     * @param workspace
     */
	render: function(workspace) {
	    return;
	},

    /**
     * <h4>getTable()<hr></h4>
     *
     * <p>a second fetch function, for when one isn't enough</p>
     *
     * <ul>
     *     <li>called to fetch side output outside of a Chain's normal fetchAll sequence
     *     <li>scenario: Link is zombified by another Link, its fetch data will be consumed by the parent wrapper
     *     <li>returns object
     *     <li>this is a stub - subsequent Links must override it to generate output
     * </ul>
     *
     * @param workspace
     * @return {Object}
     */
	sideFetch: function(workspace) {
		return {};
	},

    /**
     * <h4>sideRender()<hr></h4>
     *
     * <p>a second render function, for when one isn't enough</p>
     *
     * <ul>
     *     <li>called to render side output outside of a Chain's normal renderAll sequence
     *     <li>scenario: Link is zombified by another Link, its output will be consumed by the parent wrapper
     *     <li>returns output or an empty string
     *     <li>this is a stub - subsequent Links must override it to generate output
     * </ul>
     *
     * @param workspace
     * @return string
     */
	sideRender: function(workspace) {
		return '';
	}

};

module.exports.Link = Link;
