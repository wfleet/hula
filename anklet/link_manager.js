'use strict';
/**
 * <h4>LinkManager<hr></h4>
 *
 * <p>Factory and manager tor links from a chain. When newing up links, this module will load a core Link, then
 * extend it with the instance Link, and load it with the chain's instance data.</p>
 *
 * <p>This class is static. It is used to load up Link modules, and new them up into Links. It also provides a number of utility functions for use across all links.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class LinkManager
 * @version 0.1
 * @type {{loadLink: LinkManager.loadLink, loadLinkDirectories: LinkManager.loadLinkDirectories, getLinkListing: LinkManager.getLinkListing, loadAllLinks: LinkManager.loadAllLinks, listOfLinkTypes: LinkManager.listOfLinkTypes, listOfLinkNodes: LinkManager.listOfLinkNodes, getLink: LinkManager.getLink, loadCore: LinkManager.loadCore, overlayLink: LinkManager.overlayLink, pseudoEncodeSafeText: LinkManager.pseudoEncodeSafeText, pseudoDecodeSafeText: LinkManager.pseudoDecodeSafeText, stripLink: LinkManager.stripLink, resolveValue: LinkManager.resolveValue, interpolateValue: LinkManager.interpolateValue, isTruthy: LinkManager.isTruthy}}
 */
const LinkManager = {

	/**
	 * <h4>loadLink()<hr></h4>
	 *
	 * <p>instantiates a Link module into a Link with the data from dispatch</p>
	 * <ol>
	 *      <li>loads base Link
	 *      <li>extends it with Core
	 *      <li>finds the named Link in the dispatch data and extends it with that
	 *      <li>finalizes it with the dispatch data
	 * </ol>
	 *
	 * @param data {linkData} Link data from the chain
	 * @returns {Link}
	 */
	loadLink: function(data) {
		const extend = require('util')._extend;
        let thisLink,
            thatLink,
            theseLinks,
			pathNames = [],
			linkName,
            thisData,
            i;

		// get the name, load the core and overlay it
		linkName = data.link;
		// new up a Link object
		thisLink = extend({}, Anklet.Link);

		// grouped or flat?
		if (linkName.indexOf('\.') > 0) {
            theseLinks = Anklet.links;
            pathNames = linkName.split(/\./g);
            // get the link name (which is the last element in the split)
            linkName = pathNames.pop();
            // work down to the link's level in the hierarchy
            for (i = 0; i < pathNames.length; i++) {
                theseLinks = theseLinks[pathNames[i]];
            }
            // get the indicated link
            thatLink = theseLinks[linkName];
			// extend the Link object
			thisLink = extend(thisLink, thatLink);
		}
		else {
			// extend the Link object
			thisLink = extend(thisLink, Anklet.links[linkName]);
		}

		// new up the chain data
		thisData = extend({}, data);

		// load the data in
		thisLink = extend(thisLink, thisData);

		return thisLink;
    },

    /**
	 * <h4>loadLinkDirectories()<hr></h4>
	 *
	 * <p>recursively loads and instantiates the links in the supplied directory</p>
     */
    loadLinkDirectories: function(thisDir, links, fs) {
        const theseFiles = fs.readdirSync(thisDir + '/'),
            coreLink = require(Anklet.Config.constants.linkFolder + '/core.js').core;
        let thisFile,
            thisStat,
            thisLink,
            thisName;
        
        for (thisFile of theseFiles) {
            thisStat = fs.statSync(thisDir + '/' + thisFile);
            thisName = thisFile.replace(/\.js/, '');
            if (thisStat.isDirectory()) {
                console.log('group "' + thisFile + '" found.');
                links[thisName] = {};
                links[thisName] = this.loadLinkDirectories(thisDir + '/' + thisFile, links[thisName], fs);
            }
            else {
                thisLink = require(thisDir + '/' + thisFile)[thisName];
                thisLink = this.overlayLink(coreLink, thisLink);
                links[thisName] = thisLink;
                console.log('link "' + thisDir + '/' + thisName + '" loaded.');
            }
        }
        return links;
    },

    /**
	 * <h4>loadLinkDirectories()<hr></h4>
	 *
	 * <p>recursively extracts the hierarchical list of name from the links object</p>
     */
    getLinkListing: function(links, list) {
        let thisLink;

        for (thisLink of Object.keys(links)) {
            if (links[thisLink].hasOwnProperty('name')) {
                // if it has a .name property, then it's a link
                list[thisLink] = thisLink;
            }
            else {
                // otherwise, it's a directory node
                list[thisLink] = {};
                list[thisLink] = this.getLinkListing(links[thisLink], list[thisLink]);
            }
        }
        return list;
    },
    
	/**
	 * <h4>loadAllLinks()<hr></h4>
	 *
	 * <p>loads and instantiates all available links, and builds the hierarchical list of link names for the Bracelet picker</p>
	 */
	loadAllLinks: function(thisRootDir) {
		let fs = require('fs');

        console.log('\nLoading available links...\n');
        Anklet.links = this.loadLinkDirectories(thisRootDir, Anklet.links, fs);
        Anklet.linkList = this.getLinkListing(Anklet.links, Anklet.linkList);
	},

	/**
	 * <h4>listOfLinkTypes()<hr></h4>
	 *
	 * <p>returns an HTML Select Option list of the names of available Link modules</p>
	 *
	 * @returns {string}
	 */
	listOfLinkTypes: function() {
		let theseGroups,
            thisGroup,
            theseLinks,
            thisLink,
            thisOption,
			options = [];

		theseGroups = Object.keys(Anklet.linkList);
		for (thisGroup of theseGroups) {
		    theseLinks = Object.keys(Anklet.linkList[thisGroup]);
		    for (thisLink of theseLinks) {
		        thisOption = '<option value="' + thisGroup + '.' + thisLink + '">' + thisGroup + '.' + thisLink + '</option>';
                options.push(thisOption);
            }
		}
        options = options.reverse();
		return options.join('\n');
    },
    
    /**
     * <h4>listOfLinkNodes()<br></h4>
     * 
     * <p>returns the list of link nodes as JSON
     */
    listOfLinkNodes: function() {
		const extend = require('util')._extend;
        let linkList = extend({}, Anklet.linkList);
        return JSON.stringify(linkList);
    },

	/**
	 * <h4>getLink()<hr></h4>
	 *
	 * <p>returns the Link module that matches the name param, or Core if not found</p>
	 *
	 * @param name string Link Name
	 * @returns {*}
	 */
	getLink: function(name) {
		let foundLink = Anklet.links[name];

		if (!foundLink) {
			foundLink = Anklet.links['core'];
		}

		return foundLink;
	},

	/**
	 * <h4>loadCore()<hr></h4>
	 *
	 * <p>gets the Core link module and returns it</p>
	 *
	 * @returns {*}
	 */
	loadCore: function() {
		let coreLink = this.getLink(Anklet.Config.constants.coreLinkName);
		return coreLink;
	},


	/**
	 * <h4>overlayLink()<hr></h4>
	 *
	 * <p>takes a Link object, and overlays another onto it</p>
	 *
	 * @param thisLink {Link} the original Link
	 * @param thatLinks {Link} the Link to overlay
	 * @returns {*}
	 */
	overlayLink: function(thisLink, thatLinks) {
		let extend = require('util')._extend,
			newLink;

		newLink = extend({}, thisLink);
		newLink = extend(newLink, thatLinks);

		return newLink;
	},

    /**
     * <h4>pseudoEncodeSafeText()<hr></h4>
     * <p>because seriously, all we care about are the ampersands and percent marks</p>
     *
     * @param thisText
     * @returns {string | * | XML | void}
     */
    pseudoEncodeSafeText: function(thisText) {
        let thatText = thisText.replace(/&/g, '%26');
        thatText = thatText.replace(/%/g, '%25');
        return thatText;
    },


    /**
     * <h4>pseudoDecodeSafeText()<hr></h4>
     * <p>because seriously, all we care about are the ampersands and percent marks</p>
     *
     * @param thisText
     * @returns {string | * | XML | void}
     */
    pseudoDecodeSafeText: function(thisText) {
        let thatText = thisText.replace(/%26/g, '&');
        thatText = thatText.replace(/%25/g, '%');
        thatText = thatText.replace(/&lt;/g, '<');
        thatText = thatText.replace(/&gt;/g, '>');
        return thatText;
    },



    /**
     * <h4>stripLink()<hr></h4>
     * <p>Remove extraneous fields for edit display</p>
     * <p>also, because values are stored in URI-encoded form, decode them for display</p>
     *
     * @param thisLink
     * @returns {{}}
     */
    stripLink: function(thisLink) {
	    let newLink = {},
            keysToStrip = ['sideRender', 'render', 'fetch', 'init', 'output', 'workspace', 'fullname', 'name', 'template', 'customList', 'object-tree'],
            theseKeys = Object.keys(thisLink),
            thisKey,
            newArray,
            thisRow;

        for (thisKey of theseKeys) {
            if ((thisLink.customList && thisLink.customList.includes(thisKey)) || (!keysToStrip.includes(thisKey) && typeof(thisLink[thisKey]) !== 'object' && typeof(thisLink[thisKey]) !== 'function')) {
                if (Array.isArray(thisLink[thisKey])) {
                    newArray = [];
                    for (thisRow of thisLink[thisKey]) {
                        newArray.push(decodeURIComponent(thisRow));
                    }
                    newLink[thisKey] = newArray;
                }
                else {
                    newLink[thisKey] = decodeURIComponent(thisLink[thisKey]);
                }
            }
        }

        return newLink;
    },

    /**
     * <h4>resolveValue()<hr></h4>
     * <p>handler for possibly redirected value expressions</p>
     * <ul>
     *      <li>if the name begins with '@', it is a reference to a value in the workspace object. In this case, the naming path starts at the first level within 'workspace'
     *      <li>if the name begins with '#' it is a reference to a file in the folder ~/hula/assets.
     *      <li>otherwise, just pass the value through.
     *      <li>note: if not a workspace reference, workspace can be omitted.
     * </ul>
     *
     * @param thisValue
     * @param workspace
     * @returns {*}
     */
    resolveValue: function(thisValue, workspace) {

        // catch boolean passthroughs
        if (thisValue === true || thisValue === 'true') {
            return true;
        }
        if (thisValue == false || thisValue === 'false') {
            return false;
        }

        // catch any other issue
        if (!thisValue) {
            return thisValue;
        }

        const extend = require('util')._extend;
        let thatValue;

        if (thisValue.charAt(0) == '@') {
            // get the value from workspace
            try {
                let path = thisValue.replace(/@/, ''),
                    thisWork = extend({}, workspace),
                    partSnip = path.split(/\./g),
                    thisPart;
                for (thisPart of partSnip) {
                    thisWork = thisWork[thisPart];
                }
                thatValue = thisWork;
            }
            catch (error) {
                console.error(error);
                thatValue = 'error';
            }

        }
        else if (thisValue.charAt(0) == '#') {
            // get value from a file in ~/hula/assets
            try {
                // figure out its path and get the file
                const fs = require('fs'),
                    path = require('path'),
                    filename = thisValue.replace(/#/, ''),
                    filePath = path.join(Anklet.Config.constants.assetsFolder, filename);

                thatValue = fs.readFileSync(filePath, 'utf8');
            }
            catch (error) {
                console.error(error);
                thatValue = 'error';
            }
        }
        else {
            // not a reference, just pass it through
            thatValue = thisValue;
        }

        return thatValue;
    },

    /**
     * <h4>interpolateValue()<hr></h4>
     * <p>replaces any insert references with the referenced values</p>
     *
     * @param thisValue
     * @param workspace
     * @returns {*}
     */
    interpolateValue: function(thisValue, workspace) {
        const Handlebars = require('handlebars');
        let thatValue = thisValue,
            thisTemplate;

        // if there are no @- or {{- refs, just return
        if (thisValue.indexOf('@') < 0 && thisValue.indexOf('{{') < 0) {
            return thisValue;
        }

        // otherwise, resolve all the @-refs
        while (thatValue.indexOf('@') > -1) {
            thatValue = thatValue.replace(/(@\S+)/, function() {
                const matchValue = arguments[1];
                let newValue = Anklet.LinkManager.resolveValue(matchValue, workspace);
                // if it's an array, protect against flattening
                if (Array.isArray(newValue)) {
                    newValue = newValue.join('^^');
                }
                return newValue;
            })
        }

        // then, resolve any handlebar-refs
        if (thatValue.indexOf('{{') > -1) {
            thisTemplate = Handlebars.compile(thatValue);
            thatValue = thisTemplate(workspace);
        }

        // if a flattened array, split it apart
        if (thatValue.indexOf('^^') > -1) {
            return thatValue.split(/\^\^/g);
        }
        else {
            return thatValue;
        }
    },

    /**
     * <h4>zombifyThis()<hr></h4>
     * <p>a basic handler for zombification of link function calls</p>
     *
     * @param linkData
     * @param functionToCall
     * @returns {function(*): *}
     */
    zombifyThis: function(linkData, functionToCall) {

        // sometimes it's just the link name in a string
        if (typeof linkData === 'string') {
            // if so, make it an object
            linkData = {"link": linkData};
        }
        const thisZombieLink = this.loadLink(linkData),
            thisFunction = thisZombieLink[functionToCall];

        return thisFunction;
    },

    /**
     * <h4>listOfAdditionalFonts()<hr></h4>
     * <p>builds a select option list of additional font names</p>
     *
     * @returns {string}
     */
    listOfAdditionalFonts: function() {
        let thisFontList = Anklet.Config.fonts,
            thisFont,
            thisFontName,
            thisListOutput = [];

        for (thisFont of thisFontList) {
            thisFontName = Object.getOwnPropertyNames(thisFont)[0];
            thisListOutput.push('<option value="' + thisFontName + '">' + thisFontName + '</option>');
        }

        return thisListOutput.join('\n');
    },

    /**
     * <h4>isTruthy()<hr></h4>
     * <p>just a clean check of set link values</p>
     *
     * @param thisValue
     * @returns {boolean}
     */
    isTruthy: function(thisValue) {
        if (thisValue === '1' || thisValue === 1 || thisValue === true || thisValue === 'true' || (thisValue.length > 0 && thisValue !== 'false' && thisValue !== '0')) {
            return true;
        }
        else {
            return false;
        }
    }



};

module.exports.LinkManager = LinkManager;

