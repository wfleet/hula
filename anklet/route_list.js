'use strict';
/**
 * <h4>RouteList<hr></h4>
 *
 * <p>set up a list of Routes for the Route object</p>
 * @type {[*]} Array of base routes for the path table
 */

const RouteList = [

	// basic routes -- don't edit these
	{
		basePath: '/core',
		type: 'chain',
		target: 'core.json'
	},
	{
		basePath: '/json',
		type: 'send',
		target: 'post.json'
	},


	// user routes -- change / add these to reference permanent chains
	{
		basePath: '/initial',
		type: 'chain',
		target: '../chains/initialtest.json'
	},
	{
		basePath: '/ps_test',
		type: 'chain',
		target: '../chains/ps_2.json'
	}
];

module.exports.RouteList = RouteList;
