'use strict';
/**
 * <h4>Anklet<hr></h4>
 *
 * <p>A Unified Resource Broker (URB)</p>
 * <img src="../bracelet/assets/hula.png" height="176" width="468" style="float: right">
 * <p>the Anything Framework</p>
 *
 * <ul>
 *     <li>Anklet is the service component of the Hula framework
 *     <li>Anklet is the core object that anchors the Anklet service
 *     <li>It sits in the global scope, so it is easily referenced from any function, sync or async
 *     <li>Globally, all other modules/classes are attached to this one
 * </ul>
 *
 * @example
 * // to invoke from the command line:
 * node hula.js
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Anklet
 * @version 0.1
 *
 * @type {{init: Anklet.init, workspace: {}, loadModules: Anklet.loadModules, dispatch: Anklet.dispatch, runService: function(), pathTable: Array, output: string}}
 */

const Anklet = {

	/**
	 * <h4>init()<hr></h4>
	 *
	 * <p>automagically called at program load to start the Anklet service</p>
	 * <ul>
	 *     <li>loads all major Classes
	 *     <li>loads all available Links (through LinkManager)
	 *     <li>calls runService() to start the Anklet server
	 * </ul>
	 *
	 * @returns {Anklet}
	 */
	init: function() {
		let anklet = this,
            thisLinkRoot;

		console.info('loading Anklet.Config...');
		anklet.Config = require('./config.js').Config;

		console.info('loading Anklet.Route...');
		anklet.Route = require('./route.js').Route;

		console.info('loading Anklet.RouteList...');
		anklet.RouteList = require('./route_list.js').RouteList;

		console.info('loading Anklet.Link...');
		anklet.Link = require('./link.js').Link;

		console.info('loading Anklet.LinkManager...');
		anklet.LinkManager = require('./link_manager.js').LinkManager;

		console.info('loading Anklet.Chain...');
		anklet.Chain = require('./chain.js').Chain;

		// load the additional modules, if enabled
        if (Anklet.Config.loadModules) {
            console.info('loading additional modules...');
            this.loadModules();
        }

        // load any additional fonts, if defined
        if (anklet.Config.fonts.length > 0) {
            anklet.fontList = anklet.LinkManager.listOfAdditionalFonts();
        }

		// load all the Link modules
		console.info('loading the Link modules...');
		anklet.links = {};
		anklet.linkList = {};
		thisLinkRoot = Anklet.Config.constants.linkFolder;
		anklet.LinkManager.loadAllLinks(thisLinkRoot);

		// run the service
		console.info('\nstarting the Anklet service...');
		anklet.runService();

		return anklet;
	},

    /**
     * <h4>workspace<hr></h4>
     * <p>The catchall holder for chain fetches and workspaces</p>
     *
     * @type {{}}
     */
	workspace: {},

    fontList: '',

    /**
     * <h4>loadModules()<hr></h4>
     * <p>looks into the modules folder and loads the modules it finds there</p>
     *
     */
    loadModules: function() {
	    let anklet = this,
            fs = require('fs'),
            modPath = anklet.Config.constants.modulesFolder,
            modList,
            thisStat,
            thisFileName,
            thisModName,
            thisModule,
            thisKey;

        modList = fs.readdirSync(modPath + '/');

        for (thisFileName of modList) {
            thisStat = fs.statSync(modPath + '/' + thisFileName);

            if (thisStat.isDirectory()) {
                console.log('group "' + thisFileName + '" found.');
                theseDirs.push(modPath + '/' + thisFileName);
            }
            else {
                thisModName = thisFileName.replace(/\.js/, '');
                if (thisModName.indexOf('\.') < 0 && thisModName !== 'core') {
                    thisModule = require(modPath + '/' + thisFileName);
                    for (thisKey of Object.keys(thisModule)) {
                        Anklet[thisKey] = thisModule[thisKey];
                        console.log('    module Anklet.' + thisKey + ' loaded.');
                        if (Anklet[thisKey].hasOwnProperty('init')) {
                            console.log('    module Anklet.' + thisKey + '.init() called.');
                            Anklet[thisKey].init();
                        }
                        else {
                            console.log('    module Anklet.' + thisKey + ' has no init function.');
                        }
                    }
                }
            }
        }

    },

    /**
     * <h4>dispatch()<hr></h4>
     * <p>called by Anklet.Routes.getRoutes() at run time with the chain, query data, and post data</p>
     *
     * <ul>
     *      <li>inits the Chain, then calls its render phase as a Promise
     *      <li>sets a timer to wait until all Promises are fulfilled
     *      <li>once the fetch phase is fulfilled, call the render phase and then finish the response by sending it back to the client
     *      <li>Here is where we invoke a number of nested Promises</li>
     * </ul>
     *
     * @param {Object} chain - the Chain data as a JSON object
     * @param {Object} query - the query object from the query line
     * @param {Object} post - the post body object
     * @param {boolean} repeat - optional - if supplied, repeat the chain with the same workspace
     * @param {Object} oldWorkspace - the previous workspace, for repeat runs
     * @return {*}
     */
    dispatch: function(chain, query, post, repeat, oldWorkspace) {

        process.on('unhandledRejection', function(reason, promise) {
            Anklet.service.log('error', 'Unhandled Rejecion at ', promise);
            Anklet.service.log('error', 'reason ', reason);
        });

        // init the chain
        let thisChain = Anklet.Chain.init(chain),
            workspace,
            segmentFunctions = [],
            thisSegment,
            fetchFunctions,
            renderFunctions,
            thisPromiseSequence,
            gatherCall;

        try {
            if (repeat) {
                // if rerun is supplied, then this is a re-run of the chain
                workspace = oldWorkspace;
                thisChain.workspace = workspace;
            }
            else {
                // otherwise, it's a new run init the workspace
                workspace = thisChain.workspace;

                // store the query and post
                workspace.queryData = query;
                workspace.postData = post;

                // set the exec segments
                workspace.segments = thisChain.getSegments(workspace);
                workspace.segmentIndex = -1;
            }

            // build the segment function lists
            for (thisSegment of workspace.segments) {
                fetchFunctions = thisChain.fetchAll(thisSegment, workspace);
                segmentFunctions.push([].concat(fetchFunctions));

                renderFunctions = thisChain.renderAll(thisSegment, workspace);
                segmentFunctions.push([].concat(renderFunctions));
            }

            // add the gatherall phase at the end
            gatherCall = {
                'module': Anklet.Chain,
                'exec': 'gatherAll'
            }
            segmentFunctions.push([].concat(gatherCall));

            // log the beginning of the promise assembly
            Anklet.service.log('promise', '*');
            Anklet.service.log('promise', 'begin the assembly and execution of the promise segments');

            // build the promise structure
            return thisPromiseSequence = segmentFunctions.reduce(
                (thisPromise, thisSegment) => thisPromise.then(() => {
                    let theseProms = [],
                        thisProm,
                        thisCall;
                    // thisSegment is an array of link function calls
                    // each function is a 'link' reference, plus the 'exec' indicating which function to call within it
                    // each function gets passed the workspace object when called
                    Anklet.service.log('segment', 'segment call');

                    // start an array of Promise, then all() them to finish
                    for (thisCall of thisSegment) {
                        theseProms.push(
                            new Promise((resolve, reject) => {
                                const thisExecCall = thisCall.exec,
                                    thisExecLink = thisCall.module,
                                    thisResult = thisExecLink[thisExecCall](workspace);

                                if (thisExecLink.id) {
                                    Anklet.service.log('promise', '    new promise: ' + thisExecLink.id + ':' + thisExecCall);
                                }
                                else {
                                    Anklet.service.log('promise', '    new promise: ' + thisExecCall);
                                }
                                resolve(thisResult);
                            })
                        );
                    }

                    return Promise.all(theseProms);

                }), Promise.resolve()
            ).then(
                () => {
                    // hopefully, workspace is global enough
                    return workspace;
                }
            );

        }
		catch(error) {
			// maybe a better handler here eventually
			console.error(error);
			return null;
		}
	},

	/**
	 * <h4>runService()<hr></h4>
	 *
	 * <p>called from Anklet.init()</p>
	 * <ul>
	 *     <li>sets up and runs the anklet server
     *     <li>installs routes and handlers
     *     <li>runs continuously until stopped
     *     <LI>moved to hapi server, to see if any simpler
     * </ul>
     *
	 */
    runService: () => {
        
        const Hapi = require('hapi'),
            Good = require('good'),
            Handlebars = require('handlebars'),
            Inert = require('inert'),
            Vision = require('vision');
            
        let anklet = this,
            server;
        if (Anklet.Config.debugLogs) {
            server = new Hapi.Server(
                {
                    debug: {
                        log: Anklet.Config.debugLevels,
                        request: Anklet.Config.debugLevels
                    }
                }
            );
        }
        else {
			server = new Hapi.Server();
        }

		server.register([
			{
				register: Good,
				options: {
					reporters: {
						console: [
							{
								module: 'good-squeeze', name: 'Squeeze', args: [
								{response: '*', log: '*'}
							]
							},
							{module: 'good-console'},
							'stdout'
						]
					}
				}
			},
			{
				register: Inert
			},
			{
				register: Vision
			}
		],

		(err) => {

			if (err) {
				throw err;
			}

			server.views({
				engines: {
					html: Handlebars.create()
				}
			});
		});

        server.connection({ port: 8080 });

		anklet.Anklet.Route.getRoutes(server, Anklet.RouteList);
		if (anklet.Anklet.Config.applyChainPaths) {
		    let chainRoutes = Anklet.Route.loadChainPaths();
		    anklet.Anklet.Route.getRoutes(server, chainRoutes);
        }

		server.start((err) => {
			if (err) {
				throw err;
			}
			server.log('start', 'Anklet service started and listening at ' + server.info.uri);
		});

		server.on('response', function (request) {
			server.log(request.info.remoteAddress + ': ' + request.method.toUpperCase() + ' ' + request.url.path + ' --> ' + request.response.statusCode);
		});

		Anklet.service = server;

	},

	/**
	 * array of path and target mappings
	 * @type {Array}
	 */
	pathTable: [],

	/**
	 * gathering place for rendered output - probably not used
	 * @type {String}
	 */
	output: ''

};

// start this puppy up
module.exports.Anklet = Anklet;
