'use strict';
/**
 * <h4>Route<hr></h4>
 *
 * <p>set up a list of Routes to available Chains and other resources</p>
 *
 * <p>using the list of routes in <tt>RouteList</tt>, set up routes and route handlers for all approach types
 * and hand them off to <tt>Anklet.dispatch()</tt> for processing.</p>
 *
 * <p>There are several types of approach: </p>
 * <ul>
 *     <li>get chain - load a Chain document from the server for processing
 *     <li>post chain - the Chain data will be in the JSON body, run that and return the output
 *     <li>get resource - run a service call for resources, usually for bracelet
 *     <li>get static - a simple serve of static files from the server, usually for bracelet
 * </ul>
 *
 * <p>For Production environments, it is recommended that the latter two approaches be disabled. Simply remove thr routes
 * from the RouteList object if you do not wish to change code.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Routes
 * @version 0.1
 * @type {{runChain: Routes.runChain, runPostChain: Routes.runPostChain, runResourceCall: Routes.runResourceCall, getActualRoute: Routes.getActualRoute, getRoutes: Routes.getRoutes}}
 */

const Route = {

	/**
	 * <h4>runChain()<hr></h4>
	 * <p>retrieves the name chain file from disk, inits the Chain from it and runs it</p>
	 * <p>many Promises here</p>
	 *
	 * @param request
	 * @param reply
	 */
	runChain: function (request, reply) {

	    // the chain workspace object
        let chainWorkspace = {};

		// read the chain from the server path and dispatch it
		new Promise(function(resolve, reject) {

			const path = require('path'),
				fs = require('fs');
			let chainFile,
				chainPath;

			// retrieve the target from the pathTable
			chainFile = Anklet.pathTable[request.path];
			// figure out its path
            if (chainFile.charAt(0) === '/') {
                // it's absolute
                chainPath = chainFile;
            }
            else {
                // it's relative
                chainPath = path.join(Anklet.Config.constants.chainFolder, chainFile);
            }
			// get the query data
			Anklet.queryData = request.query;
			// get the post data
			Anklet.postData = request.payload;

            if (request.payload && request.payload.json) {
                // okay, it's a POSTed chain object
                // this is allowed by Anklet.Config.runPostSends
                let thisChainData = request.payload.json,
                    thisChain;

                    //correct any ampersands
                    thisChainData = thisChainData.replace(/&/g, '%26');
                    thisChain = JSON.parse(thisChainData);

                // was there a parse error?
                if (typeof(thisChain) === 'error') {
                    console.error(thisChain);
                    throw (thisChain);
                }
                resolve(thisChain);
            }
            else {
                // it's a normal chain request
                // get the chain json file
                fs.readFile(chainPath, 'utf8', function (error, data) {

                    // was there a read error?
                    if (error) {
                        console.error(error);
                        throw (error);
                    }

                    // otherwise, parse the chain
                    const thisChain = JSON.parse(data);

                    // was there a parse error?
                    if (typeof(thisChain) === 'error') {
                        console.error(thisChain);
                        throw (thisChain);
                    }
                    resolve(thisChain);
                })
            }
		})
		.then(
			(chainData) => {
			    // dispatch the chain for processing
                let thisPatch = Anklet.dispatch(chainData, Anklet.queryData, Anklet.postData);
                chainWorkspace = Promise.resolve(thisPatch);
                return chainWorkspace;
		})
		.then(
			(chainWorkspace) => {
			    if (Array.isArray(chainWorkspace)) {
			        chainWorkspace = chainWorkspace[0];
                }
			    // was there a render error?
                if (request.method === 'get' && !chainWorkspace.output) {
                    request.server.log(chainWorkspace.output);
                    throw ('no final output');
                }
                else if (chainWorkspace.output && typeof(chainWorkspace.output) === 'error') {
                    request.server.log(chainWorkspace.output);
                    throw (chainWorkspace.output);
                }
                return chainWorkspace;
		})
		.then(
			(chainWorkspace) => {
				// once complete, return the rendered data to the client
				try {
				    let thisHeader,
                        splitHeader,
                        response;
					// console.info(chainWorkspace.output);

					// set any headers - default to text/html if none
					if (!chainWorkspace.outputHeaders) {
					    // best guess
					    chainWorkspace.outputHeaders = [
                            'Content-Type: text/html'
                        ]
                    }
                    // set the response
                    // if (chainWorkspace.outputName) {
					 //    response = reply.file(chainWorkspace.outputName);
                    // }
                    // else {
                        response = reply(chainWorkspace.outputStream);
                    // }
                    for (thisHeader of chainWorkspace.outputHeaders) {
					    splitHeader = thisHeader.split(/\:\s*/);
                        response.header(splitHeader[0], splitHeader[1]);
                    }
                    return chainWorkspace;
				}
				catch(error) {
					console.error(error);
					return false;
				}
			}
		)
        // .then(
        //     (chainWorkspace) => {
        //         const fs = require('fs');
        //         // if it was a file send, delete the file
        //         if (chainWorkspace.outputName) {
        //             fs.unlinkSync(chainWorkspace.outputName);
        //         }
        //     }
        // )
		.catch(
			(chainData) => {
			// handle error here
			console.error(chainData);
			return false;
		});
	},

	/**
	 * <h4>runResourceCall()<hr></h4>
	 * <p>retrieves and sends the resource called from Bracelet front-end</p>
	 *
	 * @param request
	 * @param response
	 */
	runResourceCall: function(request, response) {
		let thisInfo,
			thisActualRoute = Anklet.Routes.getActualRoute(request.url),
			thisFunction = thisActualRoute.getFunction;

		if (thisFunction == 'getResource') {
			server.log('calling resource function...');
			thisInfo = Anklet.Bracelet.getResource();
			response.set('Content-Type', 'text/html');
			response.send(thisInfo);
		}
	},

    /**
     *
     * @returns {Array}
     */
    loadChainPaths: function() {
        const fs = require('fs'),
            absChainFolder = Anklet.Config.constants.chainFolder + '/',
            relChainFolder = Anklet.Config.constants.relChainFolder + '/',
            theseChains = fs.readdirSync(absChainFolder);
        let thisChain,
            thisHeader,
            thisChainRoute,
            thisRouteList = [];

        for (thisChain of theseChains) {
            thisHeader = require(absChainFolder + thisChain).header;
            if (thisHeader.path && thisHeader.method) {
                thisChainRoute = {};
                thisChainRoute.basePath = thisHeader.path.replace(/^\/anklet/, '');
                if (thisHeader.method === 'POST') {
                    thisChainRoute.type = 'post';
                }
                else {
                    thisChainRoute.type = 'chain';
                }
                thisChainRoute.target = relChainFolder + thisChain;
                console.log('added chain path: ' + thisChainRoute.target + ' at ' + thisChainRoute.basePath);
                thisRouteList.push(thisChainRoute);
            }
        }

        return thisRouteList;
    },

	/**
	 * <h4>getActualRoute()<hr></h4>
	 * <p>reverse lookup for resource calls --  called by runResourceCall() above</p>
	 *
	 * @param thisPath
	 * @returns {*}
	 */
	getActualRoute: function(thisPath) {
		let thisBaseURL = Anklet.Config.constants.baseURL,
			thisRoute;

		for (thisRoute of Anklet.RouteList) {
			if (thisPath == thisBaseURL + thisRoute.basePath) {
				return thisRoute;
			}
		}
		return {};
	},

    /**
     * <h4>getRoutes()<hr></h4>
     * <p>reads in the list of routes from the Anklet.RouteList obkect, then registers them with the node server.</p>
     * <ul>
     *     <li>moving across from express.js to hapi server
     * </ul>
     *
     * @param ankletServer {Object} the node.js app instance we're starting up
     * @param thisRouteList {Array} list of routes to add to the path table
     */
	getRoutes: function(ankletServer, thisRouteList) {
		let baseURL = Anklet.Config.constants.baseURL,
			path = require('path'),
			inert = require('inert'),
			thisRoute,
			thisPath,
			thisDir;

		for (thisRoute of thisRouteList) {
			// TODO: clean this mess up

			// set a base path and notify log
			thisPath = path.join(baseURL + thisRoute.basePath);
			thisDir = path.join(__dirname + thisRoute.target);
			ankletServer.log(['start', 'route'], 'Registered ' + thisRoute.type + ' call ' + (thisRoute.target) + ' at ' + thisPath + ' ...');

			// save the giblets --  or rather, a map of paths to targets
			Anklet.pathTable[thisPath] = thisRoute.target;

			/**
			 * type 1 - CHAIN
			 */
			if (thisRoute.type === 'chain') {
				// we're dealing with a chain request
				// add it to the path table for retrieval inside the callback and dispatch the data
				ankletServer.route({
					method: 'GET',
					path: thisPath,
					handler: function(req, reply) {
						Anklet.Route.runChain(req, reply);
					},
					config: {
						bind: {
							'target': thisRoute.target
						}
					}
				});
			}
			/**
			 * type 2 - POSTed CHAIN
			 */
			else if (thisRoute.type === 'send') {
				// it's a post request - the chain will be in the json body
				ankletServer.route({
					method: 'POST',
					path: thisPath,
					handler: function(req, reply) {
					    // check to see if we're accepting these
					    if (Anklet.Config.runPostSends) {
                            Anklet.Route.runChain(req, reply);
                        }
                        else {
					        // otherwise, disappoint them
					        return false;
                        }
					},
					config: {
						bind: {
							'target': thisRoute.target
						}
					}
				});
			}
			/**
			 * type 3 - CHAIN with POST data
			 */
			else if (thisRoute.type === 'post') {
				// it's a post request - handle the request
				ankletServer.route({
					method: 'POST',
					path: thisPath,
					handler: function(req, reply) {
					    // console.log(req.payload);
						Anklet.Route.runChain(req, reply);
					},
					config: {
						bind: {
							'target': thisRoute.target
						},
                        payload: {
                            output: 'data'
                        }
                    }
				});
			}
			/**
			 * type 5 - STATIC FILE
			 */
			else {
                ankletServer.route({
                    method: 'GET',
                    path: thisPath,
                    handler: {
                        directory: {
                            path:    thisRoute.target,
                            listing: true,
                            index:   false
                        }
                    }
                });
			}
		}
	}
};

module.exports.Route = Route;