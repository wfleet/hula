'use strict';
/**
 * <h4>Config<hr></h4>
 *
 * <p>the safe place to put all your common values, constants, and message strings</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Config
 * @version 0.1
 * @type {{constants: {hula_root: string | void | *, baseURL: string, chainFolder: string, linkFolder: string, outputFolder: string, assetsFolder: string, coreLinkName: string, bracelet_chainFolder: string, bracelet_linkFolder: string, bracelet_assetsFolder: string, bracelet_templateFolder: string, bracelet_routeList: string, ankletPort: number, braceletPort: number}, loadBracelet: boolean, runPostSends: boolean, messages: {}}}
 */

const Config = {

	/**
	 * <h4>constants<hr></h4>
     * <p>holder for startup values, locations and base params</p>
     *
	 * @type {{hula_root: string | void | *, baseURL: string, relChainFolder: string, chainFolder: string, linkFolder: string, assetsFolder: string, modulesFolder: string, outputFolder: string, coreLinkName: string, bracelet_chainFolder: string, bracelet_linkFolder: string, bracelet_assetsFolder: string, bracelet_templateFolder: string, bracelet_routeList: string, log_responses: boolean, ankletPort: number, braceletPort: number}}
	 */
	constants: {
        hula_root: __dirname.replace(/hula\/.*/, 'hula'),
		baseURL: '/anklet',
		relChainFolder: '../chains',
		chainFolder:    __dirname.replace(/\/anklet/, '') + '/chains',
		linkFolder:     __dirname.replace(/\/anklet/, '') + '/links',
        assetsFolder:   __dirname.replace(/\/anklet/, '') + '/assets',
        modulesFolder:  __dirname.replace(/\/anklet/, '') + '/modules',
        outputFolder: '/output',
		coreLinkName: 'core.js',

		bracelet_chainFolder:   __dirname.replace(/\/anklet/, '') + '/bracelet/chains',
		bracelet_linkFolder:    __dirname.replace(/\/anklet/, '') + '/bracelet/links',
		bracelet_assetsFolder:  __dirname.replace(/\/anklet/, '') + '/bracelet/assets',
		bracelet_templateFolder: './bracelet/templates',
		bracelet_routeList:     __dirname.replace(/\/anklet/, '') + '/bracelet/route_list.js',

        log_responses: false,

		ankletPort: 3002,
		braceletPort: 3004
	},

	/**
	 * <h4>loadBracelet<hr></h4>
	 * <p>switch for whether to load Bracelet or not</p>
	 * @type {boolean}
	 */
	loadBracelet: true,

	/**
	 * <h4>loadModules<hr></h4>
	 * <p>switch for whether to load Bracelet or not</p>
	 * @type {boolean}
	 */
    loadModules: true,

	/**
	 * <h4>runPostSends<hr></h4>
	 * <p>allow the instant run of posted JSON chains</p>
	 * @type {boolean}
	 */
    runPostSends: true,

    /**
     * <h4>applyChainPaths<hr></h4>
     * <p>if enabled, add each saved chain's path to the routes table</p>
     * @type {boolean}
     */
	applyChainPaths: true,
	
	/**
	 * <h4>debugLogs<hr></h4>
	 * <p>if enabled, set debug logging</p>
	 * @type {boolean}
	 */
	debugLogs: true,
	
	/**
	 * <h4>debugLevels<hr></h4>
	 * <p>an array of log levels to send to stdout</p>
	 * @type {boolean}
	 */
	debugLevels: [
		'error',
		'info',
		'promise',
		'segment',
		'call'
	],

    /**
     * <h4>fonts<hr></h4>
     * <p>list of additional fonts for system use beyond the base 13.</p>
     * <p>these are best located in the ~/hula/fonts directory, but could be anywhere.</p>
     * <p>paths are relative to the ~/hula/ directory</p>
     * <p>The base 13 fonts are:</p>
     * <ul>
     *      <li>Helvetica
     *      <li>Helvetica-Oblique
     *      <li>Helvetica-Bold
     *      <li>Helvetica-BoldOblique
     *      <li>Times-Roman
     *      <li>Times-Italic
     *      <li>Times-Bold
     *      <li>Times-BoldItalic
     *      <li>Courier
     *      <li>Courier-Oblique
     *      <li>Courier-Bold
     *      <li>Courier-BoldOblique
     *      <li>Symbol
     *      <li>ZapfDingbats
     * </ul>
     * <p>contains an array of {'name': 'filename'} objects}. No spaces in the names, please.</p>
     *
     */
    fonts: [
        {'DIN-Next-W01-Black'       : 'fonts/DIN-Next-W01-Black.ttf'},
        {'DIN-Next-W01-Black-Italic': 'fonts/DIN-Next-W01-Black-Italic.ttf'},
        {'DIN-Next-W01-Bold'        : 'fonts/DIN-Next-W01-Bold.ttf'},
        {'DIN-Next-W01-Light'       : 'fonts/DIN-Next-W01-Light.ttf'},
        {'DIN-Next-W01-Light-Italic': 'fonts/DIN-Next-W01-Light-Italic.ttf'},
        {'DIN-Next-W01-Medium'      : 'fonts/DIN-Next-W01-Medium.ttf'},
        {'DIN-Next-W01-Regular'     : 'fonts/DIN-Next-W01-Regular.ttf'},
        {'DINEngschriftStd'         : 'fonts/DINEngschriftStd.otf'}
    ],

	/**
	 * <h4>messages<hr></h4>
	 * <p>holder for message strings</p>
	 * <p>haven't really implemented it yet, will do in time</p>
	 * @type {{}}
	 */
	messages: {}

};

module.exports.Config = Config;
