# in base alpine, install other frameworks pre-node
FROM alpine

#
# run install of imagemagick and ghostscript
#RUN apk update && apk add --no-cache file && apk --update add imagemagick ghostscript-fonts ghostscript-dev
RUN apk update && apk add --no-cache file && apk --update add imagemagick

## make a work dir and move to it
#RUN mkdir /app
#WORKDIR /app

# switch to running node 8
FROM node:8

# create hula directory
WORKDIR /usr/src/hula

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# run the npm installer from the package.json file
RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

# set the exposed ports
EXPOSE 8080

# set the name
ENV NAME hula

# run the app
CMD [ "npm", "start" ]
