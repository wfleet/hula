/**
 * called by most send functions
 * needs to be on the global level so it can be found by link editors
 * @param options
 */

function sendPost(options) {

    var theseOptions = {
        url: '',
        type: 'post',
        data: '',
        dataType: 'text',
        async: false,
        success: function(data) {
            $('.ui-dialog').remove();
            $('#chain_info').load('/anklet/bracelet/info');
            $('#chain_list').load('/anklet/bracelet/list', function() {
                set_chain_list();
            });
            $('#chain_code').load('/anklet/bracelet/code', function() {
                set_chain_code();
            });
            $('#page_modals').load('/anklet/bracelet/modal', function() {
                set_page_modals();
            });
        },
        error: function(data, error) {
            console.log('error: ' + JSON.stringify(data, null, 4));
            console.log('error: ' + error);
        }
    };

    // overlay the passed-in options on the main theseOptions
    Object.keys(options).forEach(function(key, index) {
        theseOptions[key] = options[key];
    })

    $.ajax(theseOptions);
}

/**
 * called by API testors
 * needs to be on the global level so it can be found by link editors
 */

function viewAPI() {
    // set values for an api test/view

    var $fieldInputs = $('#thisEditDialog').find('input'),
        $fieldSelects = $('#thisEditDialog').find('select'),
        $fieldAreas = $('#thisEditDialog').find('textarea'),
        thisQuery = $('#input-get-data').val(),
        theseFields = [],
        $thisField,
        thisKey,
        thisValue,
        options = {},
        i,
        getValues = function($theseItems) {

            for (i = 0; i < $theseItems.length; i++) {
                $thisField = $($theseItems[i]);
                thisKey = $thisField.attr('name');
                if (thisKey) {
                    thisValue = $thisField.val();
                    theseFields.push(encodeURIComponent(thisKey) + '=' + encodeURIComponent(thisValue));
                }
            }
        };

    getValues($fieldInputs);
    getValues($fieldAreas);
    getValues($fieldSelects);
    options = {
        data: theseFields.join('&'),
        url: '/anklet/bracelet/object-tree?' + thisQuery,
        success: function(data) {
            var apiWindow = window.open(window.location.origin, 'apiWindow'),
                thisDocument = apiWindow.document,
                thisScript = document.createElement('script');

            thisDocument.write(data);

            function injectjQuery() {
                var thisHead = document.getElementsByTagName("head")[0];
                var thisScript = document.createElement("script");
                thisScript.src = "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
                thisScript.onload = function() {
                    clipThis();
                };
                thisHead.appendChild(thisScript);
            }

            function clipThis() {
                $('.clipLink').on('click', function(event) {

                    event.preventDefault();
                    const $thisItem = $(event.target),
                        thisValue = $thisItem.data('path');
                    let $thisInput = $("<input>");
                    $("body").append($thisInput);
                    $thisInput.val(thisValue).select();
                    document.execCommand("copy");
                    $thisInput.remove();

                    alert('The path "' + thisValue + '" has been copied to the clipboard.');
                });
            }

            thisScript.innerHTML = injectjQuery.toString() + '\n\n' + clipThis.toString() + '\n\injectjQuery();';
            thisDocument.body.appendChild(thisScript);
        }
    };
    sendPost(options);
};

/**
 * called by main_page.json
 * wrapped in a closure for your code safety
 */

(function($) {

    // set the panel components
    $('#chain_info').load('/anklet/bracelet/info');
    $('#chain_list').load('/anklet/bracelet/list', function() {
        set_chain_list();
    });
    $('#chain_code').load('/anklet/bracelet/code', function() {
        set_chain_code();
    });
    $('#page_modals').load('/anklet/bracelet/modal', function() {
        set_page_modals();
    });

    // })(jQuery)

    /**
     called by page_modals.json
     **/

    window.set_page_modals = function() {
        // set up action buttons
        $('.save-btn').on('click', function (event) {
            event.preventDefault();

            $('#thisSaveDialog').dialog('open');
            $('#saveChain-input').val($('#chain_info h2').text());
        });

        $('.view-btn').on('click', function (event) {
            event.preventDefault();
            viewChain();
        });

        $('.load-btn').on('click', function (event) {
            event.preventDefault();
            $('#thisLoadDialog').dialog('open');
        });

        $('.edit-btn').on('click', function (event) {
            event.preventDefault();
            editHeader();
            $('#thisHeaderDialog').dialog('open');
        });


        // init the load dialog
        var loadDialog = $('#thisLoadDialog').dialog({
            autoOpen: false,
            width: 450,
            modal: true,
            title: 'Load Chain',
            buttons: {
                'Load Chain': loadChain,
                Cancel: function () {
                    loadDialog.dialog('close');
                }
            }
        });

        // init the save dialog
        var saveDialog = $('#thisSaveDialog').dialog({
            autoOpen: false,
            width: 450,
            modal: true,
            title: 'Save Chain',
            buttons: {
                'Save Chain': saveChain,
                Cancel: function () {
                    saveDialog.dialog('close');
                }
            }
        });

        // init the edit dialog
        var editDialog = $('#thisEditDialog').dialog({
            autoOpen: false,
            width: 600,
            modal: false,
            title: 'Edit Link',
            buttons: {
                'Save': saveEdit,
                Cancel: function () {
                    editDialog.dialog('close');
                }
            }
        });

        // init the header dialog
        var headerDialog = $('#thisHeaderDialog').dialog({
            autoOpen: false,
            width: 600,
            modal: false,
            title: 'Edit Chain Hesder',
            buttons: {
                'Save': saveHeader,
                Cancel: function () {
                    headerDialog.dialog('close');
                }
            }
        });

        function setCookie(cookieName, cookieValue, expirationDays) {
            var thisDate = new Date();
            cookieValue = encodeURIComponent(cookieValue);
            thisDate.setTime(thisDate.getTime() + (expirationDays*24*60*60*1000));
            var expires = "expires="+ thisDate.toUTCString();
            document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
        }

        function getCookie(cookieName) {
            var thisName = cookieName + "=",
                thisCookie = decodeURIComponent(document.cookie),
                splitCookie = thisCookie.split(';'),
                i, thatCookie;

            for (i = 0; i < splitCookie.length; i++) {
                thatCookie = splitCookie[i];
                while (thatCookie.charAt(0) == ' ') {
                    thatCookie = thatCookie.substring(1);
                }
                if (thatCookie.indexOf(thisName) == 0) {
                    return thatCookie.substring(thisName.length, thatCookie.length);
                }
            }
            return "";
        }

        function viewChain() {

            var getData = $('#test-query').html(),
                postData = $('#test-post').html(),
                thisPath = $('#route-path').html();

            if (getData) {
                window.open(thisPath + '?' + getData, 'test_window');
            }
            else {
                window.open(thisPath, 'test_window');
            }

        }

        function loadChain(event) {
            event.preventDefault();

            var thisFile = $('#loadChain-input').val(),
                options = {
                    data: 'filename=' + thisFile,
                    url: '/anklet/bracelet/load',
                    dataType: 'json'
                };

            sendPost(options);
        }

        function saveChain(event) {
            event.preventDefault();

            var thisFile = $('#saveChain-input').val(),
                thisContent = $('#link_code').val(),
                options = {
                    data: 'filename=' + thisFile + '&content=' + thisContent,
                    url: '/anklet/bracelet/save'
                };

            sendPost(options);
        }

        function getValues($theseItems) {
            var theseFields = [],
                thisKey,
                thisType,
                thisChecked,
                thisValue,
                $thisField;
            for (i = 0; i < $theseItems.length; i++) {
                $thisField = $($theseItems[i]);
                thisKey = $thisField.attr('name');
                thisType = $thisField.attr('type');
                thisChecked = $thisField.prop('checked');

                if (thisType === 'checkbox') {
                    if (thisChecked) {
                        if ($thisField.val() == 'true' || $thisField.val() == 'false') {
                            theseFields.push(encodeURIComponent(thisKey) + '=true');
                        }
                        else if ($thisField.val() == 0 || $thisField.val() == 1) {
                            theseFields.push(encodeURIComponent(thisKey) + '=1');
                        }
                        else {
                            theseFields.push(encodeURIComponent(thisKey) + '=true');
                        }
                    }
                    else {
                        if ($thisField.val() == 'true' || $thisField.val() == 'false') {
                            theseFields.push(encodeURIComponent(thisKey) + '=false');
                        }
                        else if ($thisField.val() == 0 || $thisField.val() == 1) {
                            theseFields.push(encodeURIComponent(thisKey) + '=0');
                        }
                        else {
                            theseFields.push(encodeURIComponent(thisKey) + '=false');
                        }
                    }
                }
                else if ((thisType === 'radio' && thisChecked) || (thisType !== 'radio' && thisKey)) {
                    thisValue = $thisField.val();
                    if (thisValue.indexOf('+') > -1 || thisValue.indexOf('%2B') > -1) {
                        thisValue.replace(/%2B/g, '%252B');
                        thisValue.replace(/\+/g, '%2B');
                    }
                    theseFields.push(encodeURIComponent(thisKey) + '=' + encodeURIComponent(thisValue));
                }
            }
            return theseFields;
        }

        function editHeader() {

            var options = {
                url: '/anklet/bracelet/get-header',
                success: function(data) {
                    $('#editHead').html(data);
                }
            };

            sendPost(options);
            $('#thisHeaderDialog').dialog('option', 'title', 'Edit Chain Header ');
            $('#thisHeaderDialog').dialog('open');
        }

        function saveHeader(event) {
            event.preventDefault();

            var thisFile = $('#saveChain-input').val(),
                thisContent = $('#link_code').val(),
                theseFields = [],
                $theseInputs = $('#thisHeaderDialog').find('input'),
                $theseAreas = $('#thisHeaderDialog').find('textarea'),
                $theseSelects = $('#thisHeaderDialog').find('select'),
                options;

            theseFields = theseFields.concat(getValues($theseInputs));
            theseFields = theseFields.concat(getValues($theseAreas));
            theseFields = theseFields.concat(getValues($theseSelects));

            options = {
                data: theseFields.join('&'),
                url: '/anklet/bracelet/edit-header'
            };

            sendPost(options);
        }

        function saveEdit(event) {
            event.preventDefault();
            var thisType = $('#edit').val();
            if (thisType === 'linkType') {
                // set values for a link type edit
                var thisLinkType = $('#linkType').val(),
                    thisId = $('#link-id').val(),
                    options = {
                        data: 'id=' + thisId + '&link=' + thisLinkType,
                        url: '/anklet/bracelet/edit',
                        success: function(data) {
                            // close dialog
                            $('#thisEditDialog').dialog('close');
                            // call for a link edit
                            var thisId = $('#link-id').val(),
                                options = {
                                    url: '/anklet/bracelet/link',
                                    data: 'id=' + thisId,
                                    success: function(data) {
                                        $('#editLink').html(data);
                                    }
                                };

                            sendPost(options);
                            $('#thisEditDialog').dialog('open');

                        }
                    };
            }
            else {
                // set values for a link edit
                var $fieldInputs = $('#thisEditDialog').find('input'),
                    $fieldSelects = $('#thisEditDialog').find('select'),
                    $fieldAreas = $('#thisEditDialog').find('textarea'),
                    theseFields = [],
                    $thisField,
                    thisKey,
                    thisValue,
                    options = {},
                    i;

                theseFields = theseFields.concat(getValues($fieldInputs));
                theseFields = theseFields.concat(getValues($fieldAreas));
                theseFields = theseFields.concat(getValues($fieldSelects));

                options = {
                    data: theseFields.join('&'),
                    url: '/anklet/bracelet/edit'
                };
            }
            sendPost(options);
        }

        // set the view query value
        $('#input-get-data').val(getCookie('braceletQuery'));

    };

    /**
     called by chain_code.json
     **/

    window.set_chain_code = function() {
        // set up apply button
        $('.apply-btn').on('click', function(event) {
            event.preventDefault();

            var thisData = $('#link_code').val();
            thisData = 'script=' + encodeURIComponent(thisData)
            $.ajax({
                url: '/anklet/bracelet/apply',
                type: 'post',
                data: thisData,
                dataType: 'text',
                async: false,
                success: function(data) {
                    window.location.reload();
                },
                error: function(data, error) {
                    console.log('error: ' + JSON.stringify(data, null, 4));
                    console.log('error: ' + error);
                }
            })
        });

        // code refresh button
        $('.refresh-btn').on('click', function(event) {
            event.preventDefault();
            window.location.reload();
        });

        // tallify the code panel
        var thisPanel = document.getElementById('link_code'),
            thisTall = window.innerHeight - thisPanel.offsetTop - 75;
        // console.log('thisTall: ' + thisTall)
        $('#link_code').height(thisTall);

        $(window).on('resize', function($) {
            window.setTimeout(function() {
                var thisPanel = document.getElementById('link_code'),
                    thisTall = window.innerHeight - thisPanel.offsetTop - 75;
                // console.log('thisTall: ' + thisTall)
                thisPanel.style.height = thisTall + 'px';
            }, 500);
        });

    };

    /**
     called by chain_list.json
     **/

    window.set_chain_list = function() {

        // link list click handlers
        $('.link').on('click', selectLinkByEvent);
        $('.link-up-btn').on('click', moveLinkUp);
        $('.link-down-btn').on('click', moveLinkDown);
        $('.trash-link-btn').on('click', trashLink);

        // set up add-link button
        $('.add-link-btn').on('click', function(event) {
            event.preventDefault();
            var thisId = $('.link-selected').find('.link-id').text(),
                options = {
                    url: '/anklet/bracelet/add',
                    data: 'action=addLink'
                };

            sendPost(options);
        });

        // utility methods for the handlers
        function selectLink($thisItem) {

            if ($thisItem.hasOwnProperty('originalEvent')) {
                // it's an event, get the target
                event.preventDefault();
                openLink($thisItem);
                return;
            }

            var $lastItem = $('.link-selected');

            $thisItem.off('click');
            $thisItem.on('click', this, openLink);

            $lastItem.off('click');
            $lastItem.on('click', this, selectLink);

            $lastItem.removeClass('link-selected').addClass('link');
            $thisItem.removeClass('link').addClass('link-selected');
        }

        function selectLinkById(thisId) {
            var $thisItem = $('#link-' + thisId);
            selectLink($thisItem);
        }

        function selectLinkByEvent(event) {
            event.preventDefault();

            var $thisItem = $(event.target).closest('.link');
            selectLink($thisItem);
        }

        function openLink(event) {
            event.preventDefault();
            var $thisItem = $(event.target).closest('a')
            thisId = $('.link-selected').find('.link-id').text(),
                thisName = $('.link-selected').find('.link-name').text(),
                options = {
                    url: '/anklet/bracelet/link',
                    data: 'id=' + thisId,
                    success: function(data) {

                        $('#editLink').html(data);
                    }
                };

            sendPost(options);
            $('#thisEditDialog').dialog('option', 'title', 'Edit Link ' + thisId + ': ' + thisName);
            $('#thisEditDialog').dialog('open');
        }

        function moveLinkUp(event) {
            event.preventDefault();
            var thisId = $('.link-selected').find('.link-id').text(),
                options = {
                    url: '/anklet/bracelet/move',
                    data: 'id=' + thisId + '&move=up',
                    success: function() {

                        $('.ui-dialog').remove();
                        $('#chain_info').load('/anklet/bracelet/info');
                        $('#chain_list').load('/anklet/bracelet/list', function() {
                            set_chain_list();
                        });
                        $('#chain_code').load('/anklet/bracelet/code', function() {
                            set_chain_code();
                        });
                        $('#page_modals').load('/anklet/bracelet/modal', function() {
                            set_page_modals();
                        });
                    }
                };

            sendPost(options);
        }

        function moveLinkDown(event) {
            event.preventDefault();
            var thisId = $('.link-selected').find('.link-id').text(),
                options = {
                    url: '/anklet/bracelet/move',
                    data: 'id=' + thisId + '&move=down',
                    success: function() {

                        $('.ui-dialog').remove();
                        $('#chain_info').load('/anklet/bracelet/info');
                        $('#chain_list').load('/anklet/bracelet/list', function() {
                            set_chain_list();
                        });
                        $('#chain_code').load('/anklet/bracelet/code', function() {
                            set_chain_code();
                        });
                        $('#page_modals').load('/anklet/bracelet/modal', function() {
                            set_page_modals();
                        });
                    }
                };

            sendPost(options);
        }

        function trashLink(event) {
            event.preventDefault();
            var thisId = $('.link-selected').find('.link-id').text(),
                options = {
                    url: '/anklet/bracelet/trash-link',
                    data: 'id=' + thisId
                };

            sendPost(options);
        }

    };

})(jQuery);
