/**
 * <h4>Link.web.object_tree<hr></h4>
 *
 * <p>Definition for the object_tree link module</p>
 *
 * <p>Emits an HTML page section.</p>
 * <p>this is a listing of an object tree, where the key names are clickable links that will save its path value to the
 * clipboard, to be pasted into the bracelet editor.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.web.object_tree
 * @version 0.1
 * @type {{name: string, fullname: string, context: string, indent: number, makeButton: Link.web.object_tree.makeButton, buildList: Link.web.object_tree.buildList, render: Link.web.object_tree.render}}
 */

let object_tree = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'object_tree',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.bracelet.object_tree',

    context: '',
    indent: 2,

    /**
     * <h4>makeButton()<hr></h4>
     * <p>makes the common button to push the key path onto the clipboard</p>
     *
     * @param label
     * @param path
     * @returns {string}
     */
    makeButton: function(label, path) {
	    return '<input type="button" value="' + label + '" data-path="' + path + '" class="clipLink tree-button" title="Add \'' + path + '\' to the clipboard">';
    },

    /**
     * <h4>buildList()<hr></h4>
     * <p>builds the active list of the object passed in</p>
     *
     * @param listObject
     * @param thisOutput
     * @param indent
     * @param path
     * @returns {*}
     */
    buildList: function(listObject, thisOutput, indent, path) {

        try {
            const theseKeys = Object.keys(listObject);
            let thisKey,
                thisObject,
                thisSubIndex,
                thisSubObject,
                thisIndent;

            for (thisKey of theseKeys) {
                thisObject = listObject[thisKey];

                // thisObject will be either Array, Object or String
                if (Array.isArray(thisObject)) {
                    thisIndent = indent + this.indent;
                    if (thisObject.length === 0) {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': [ ]');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': [ ]</div>');
                    }
                    else {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': [');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': [</div>');
                        thisIndent += this.indent;
                        for (thisSubIndex in thisObject) {
                            thisSubObject = thisObject[thisSubIndex];
                            if (typeof(thisSubObject) === 'string') {
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' "' + thisSubObject + '"</div>');
                            }
                            else {
                                // thisOutput.push(indent + '        ' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' {');
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' {</div>');
                                thisOutput = this.buildList(thisSubObject, thisOutput, thisIndent, path + '.' + thisKey + '.' + thisSubIndex);
                                // thisOutput.push(indent + '        }');
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">}</div>');
                            }
                        }
                        thisOutput.push('<div style="padding-left:' + (thisIndent - this.indent) + 'em">]</div>');
                    }
                }
                else if (typeof(thisObject) === 'object') {
                    thisIndent = indent + this.indent;
                    if (thisObject === null) {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': null');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': null</div>');
                    }
                    else {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': {');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': {</div>');
                        thisOutput = this.buildList(thisObject, thisOutput, thisIndent, path + '.' + thisKey);
                        // thisOutput.push(indent + '    }');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">}</div>');
                    }
                }
                else if (typeof(thisObject) === 'string') {
                    thisIndent = indent + this.indent;
                    // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': "' + thisObject + '"');
                    thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': "' + thisObject + '"</div>');
                }
                else {
                    thisIndent = indent + this.indent;
                    // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': ' + thisObject);
                    thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': ' + thisObject + '</div>');
                }
            }
        }
        catch(error) {
            console.log(JSON.stringify(thisOutput, null, 4));
            console.error(error);
        }

        return thisOutput;
    },

    /**
     * <h4>render()<hr></h4>
     *
     * @param workspace
     */
    render: function(workspace) {
	    // TODO: eliminate possibly evil eval
	    let thisTarget = workspace.objTarget;
            thisContext = eval(workspace[thisTarget]),
            thisPath = '@' + thisTarget,
            thisOutput = [
                '<div class="grid-container">',
                '<div>{\n' + this.buildList(thisContext, [], 0, thisPath).join('\n') + '\n}</div>',
                '</div>'
            ];

        if (!workspace.output) {
            workspace.output = {};
        }
        // workspace.output[this.id] = '<pre>' + JSON.stringify(thisContext, null, 4) + '</pre>';
        workspace.output[this.id] = thisOutput.join('\n');

    }


};

module.exports.object_tree = object_tree;

