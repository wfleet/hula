/**
 * <h4>Link.bracelet.bracelet_call<hr></h4>
 *
 * <p>Definition for the bracelet_call link module</p>
 *
 * <p>Allows meta calls to Bracelet functions.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.bracelet.bracelet_call
 * @version 0.1
 * @type {{name: string, fullname: string, command: string, fetch: Link.bracelet.bracelet_call.fetch, render: Link.bracelet.bracelet_call.render}}
 */

let bracelet_call = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'bracelet_call',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.bracelet.bracelet_call',

    /**
     * <h4>command<hr></h4>
     * <p>The bracelet function to invoke with this call</p>
     * @type {string}
     */
    command: '',

    /**
     * <h4>fetch()<hr></h4>
     * <p>overrides the core method</p>
     * <p>here, we find the command for Bracelet and call the appropriate method in bracelet.js</p>
     * <p>A distinction: the passed-in 'workspace' is the chain being currently run,
     * the 'Bracelet.workspace' object is the chain being edited in Bracelet</p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const thisCommand = this.command;
            Bracelet.workspace.queryData = workspace.queryData;
            Bracelet.workspace.postData = workspace.postData;
            workspace.output[this.id] = Bracelet[thisCommand](Bracelet.workspace);
        }
        catch(error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p>overrides the core method a little bit</p>
     *
     * @param workspace
     */
    render: function(workspace) {
        try {
            const thisOutput = workspace.output[this.id];
            if (thisOutput && thisOutput.trim().length > 0) {
                workspace.output[this.id] = thisOutput;
            }
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

};

module.exports.bracelet_call = bracelet_call;

