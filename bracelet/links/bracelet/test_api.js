/**
 * <h4>Link.bracelet.test_api<hr></h4>
 *
 * <p>Definition for the test_api link module</p>
 *
 * <p>Gets a response from a net api call, and stores it.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.bracelet.test_api
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let test_api = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'test_api',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.bracelet.test_api',

    /**
     * hostname, path, port, method, headers
     */
    hostname: '',
    path: '',
    port: '80',
    method: 'GET',
    dataValues: [],
    headers: [],
    protocol: 'HTTP',
    target: 'body',

    /**
     * <h4>fetch()<hr></h4>
     * <p>calls the web resource and stores it in the location in thisTarget. Returns a Promise.</p>
     *
     * @param workspace
     * @returns {Promise.<*>}
     */
    fetch: function(workspace) {

	    const reqProm = require('request-promise-native'),
            extend = require('util')._extend,
            isTruthy = Anklet.LinkManager.isTruthy,
            thisPostData = workspace.postData;

	    // set the test data query and post
        let thisDataSet = {},
            theseValues = workspace.chainObject.header.testQuery.split(/&/g),
            thisPath,
            thisValue;
        for (thisValue of theseValues) {
            thisValue = thisValue.split(/=/);
            thisDataSet[thisValue[0]] = thisValue[1];
        }
        workspace.queryData = extend(workspace.queryData, thisDataSet);
        thisDataSet = {};
        theseValues = workspace.chainObject.header.testQuery.split(/&/g);
        for (thisValue of theseValues) {
            thisValue = thisValue.split(/=/);
            thisDataSet[thisValue[0]] = thisValue[1];
        }
        workspace.postData = extend(workspace.queryData, thisDataSet);


        // for bracelet post input, start by pulling all the fields from the post data
        if (!this.protocol) {
            this.protocol = thisPostData.protocol;
        }
        if (!this.hostname) {
            this.hostname = thisPostData.hostname;
        }
        if (!this.path) {
            this.path = decodeURIComponent(thisPostData.path);
        }
        if (!this.port) {
            this.port = thisPostData.port;
        }
        if (!this.method) {
            this.method = thisPostData.method;
        }
        if (thisPostData.dataValues) {
            this.dataValues = thisPostData.dataValues.split(/\n/g);
        }
        else {
            this.dataValues = [];
            if (isTruthy(thisPostData.inStock)) {
                this.dataValues.push('inStock=true');
            }
            if (isTruthy(thisPostData.showMarketingBullets)) {
                this.dataValues.push('showMarketingBullets=true');
            }
            if (isTruthy(thisPostData.rollUpVariants)) {
                this.dataValues.push('rollUpVariants=1');
            }
            if (isTruthy(thisPostData.showCustomerAlsoViewed)) {
                this.dataValues.push('showCustomerAlsoViewed=1');
            }
            if (isTruthy(thisPostData.showEpc)) {
                this.dataValues.push('showEpc=1');
            }
            if (isTruthy(thisPostData.showEpp)) {
                this.dataValues.push('showEpp=1');
            }
            if (isTruthy(thisPostData.showExpandedFields)) {
                this.dataValues.push('showExpandedFields=1');
            }
            if (isTruthy(thisPostData.showGuides)) {
                this.dataValues.push('showGuides=1');
            }
            if (isTruthy(thisPostData.showJumpNav)) {
                this.dataValues.push('showJumpNav=1');
            }
            if (isTruthy(thisPostData.showProdLocations)) {
                this.dataValues.push('showProdLocation=1');
            }
            if (isTruthy(thisPostData.showQA)) {
                this.dataValues.push('showQA=1');
            }
            if (isTruthy(thisPostData.showRefinements)) {
                this.dataValues.push('showRefinements=1');
            }
            if (isTruthy(thisPostData.showRelatedCats)) {
                this.dataValues.push('showRelatedCats=1');
            }
            if (isTruthy(thisPostData.showRelatedItems)) {
                this.dataValues.push('showRelatedItems=1');
            }
            if (isTruthy(thisPostData.showRequiredItems)) {
                this.dataValues.push('showRequiredItems=1');
            }
            if (isTruthy(thisPostData.showRestrictions)) {
                this.dataValues.push('showRestrictions=1');
            }
            if (isTruthy(thisPostData.showReviews)) {
                this.dataValues.push('showReviews=1');
            }
            if (isTruthy(thisPostData.showRomanceCopy)) {
                this.dataValues.push('showRomanceCopy=1');
            }
            if (isTruthy(thisPostData.showSpecs)) {
                this.dataValues.push('showSpecs=1');
            }
            if (isTruthy(thisPostData.showURL)) {
                this.dataValues.push('showURL=1');
            }
            if (isTruthy(thisPostData.showVariants)) {
                this.dataValues.push('showVariants=1');
            }
            if (isTruthy(thisPostData.maxResults)) {
                this.dataValues.push('maxResults=' + thisPostData.maxResults);
            }
            if (isTruthy(thisPostData.offset)) {
                this.dataValues.push('offset=' + thisPostData.offset);
            }
            if (isTruthy(thisPostData.priceFlag)) {
                this.dataValues.push('priceFlag=' + thisPostData.priceFlag);
            }
            if (isTruthy(thisPostData.refinement)) {
                this.dataValues.push('refinement=' + thisPostData.refinement);
            }
            if (isTruthy(thisPostData.sortMethod)) {
                this.dataValues.push('sortMethod=' + thisPostData.sortMethod);
            }
            if (isTruthy(thisPostData.storeNumber)) {
                this.dataValues.push('storeNumber=' + thisPostData.storeNumber);
            }
        }
        if (thisPostData.headers) {
            this.headers = thisPostData.headers.split(/\n/g);
        }
        this.target = thisPostData.target;

	    let thisTarget = this.target,
            thisData = '',
            theseOptions,
            thisHeader,
            splitHeader,
            headerKey,
            headerValue,
            thisProtocol;

	    try {

            // complete the path
            thisPath = Anklet.LinkManager.interpolateValue(this.path, workspace);
            thisPath = thisPath.replace(/%26/g, '&');
            workspace.apiPath = this.protocol.toLowerCase() + '://' + this.hostname + thisPath;

            // compile the data
            if (Array.isArray(this.dataValues)) {
                thisData = this.dataValues.join('&');
            }
            if (thisData) {
                thisData = '?' + thisData;
            }

            // set the protocol
            if (this.protocol === 'HTTPS') {
                this.hostname = 'https://' + this.hostname;
            }
            else {
                this.hostname = 'http://' + this.hostname;
            }

            // save the target
            workspace.objTarget = this.target;

            // set up the options object
            theseOptions = {
                uri: this.hostname + thisPath + thisData,
                method: this.method,
                headers: {
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                    'Upgrade-Insecure-Requests': '1',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate',
                    'Accept-Language': 'en-US,en;q=0.9'
                },
                json: true
            };

            if (this.port) {
                theseOptions.port = this.port;
            }

            // push in the additional headers from input, if any
            for (thisHeader of this.headers) {
                if (thisHeader.indexOf(':') > -1) {
                    splitHeader = thisHeader.split(':');
                    headerKey = splitHeader[0].trim();
                    headerValue = splitHeader[1].trim();
                    theseOptions.headers[headerKey] = headerValue;
                }
            }

            return reqProm(theseOptions)
                .then((response) => {
                    if (Anklet.Config.constants.log_responses) {
                        console.log(JSON.stringify(response, null, 4));
                    }
                    workspace.origin = Anklet.service.info.uri;
                    workspace[thisTarget] = response;
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        catch(error) {
	        console.error(error);
        }

    }

};

module.exports.test_api = test_api;

