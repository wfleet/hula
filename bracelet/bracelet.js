/**
 * <h4>Bracelet<hr></h4>
 *
 * <p>The core component for the Bracelet editor for Anklet chains.</p>
 * <ul>
 *     <li>Bracelet allows Developers to edit Chains more conveniently
 *     <li>Bracelet provides a GUI (web) front end to visualize chains
 *     <li>Bracelet is optional; if it is not needed, the /bracelet folder can be omitted
 * </ul>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Bracelet
 * @version 0.1
 * @type {{workspace: {}, init: Bracelet.init, setHandlebarsHelpers: Bracelet.setHandlebarsHelpers, loadChain: Bracelet.loadChain, saveChain: Bracelet.saveChain, addLinkToChain: Bracelet.addLinkToChain, getNewId: Bracelet.getNewId, pseudoEncodeSafeText: Bracelet.pseudoEncodeSafeText, applyChainCode: Bracelet.applyChainCode, moveLink: Bracelet.moveLink, trashLink: Bracelet.trashLink, loadChainCall: Bracelet.loadChainCall, saveChainCall: Bracelet.saveChainCall, saveLinkCall: Bracelet.saveLinkCall, getHeaderCall: Bracelet.getHeaderCall, saveHeaderCall: Bracelet.saveHeaderCall, listLinkCall: Bracelet.listLinkCall, setTemplateContext: Bracelet.setTemplateContext, linkTemplate: Bracelet.linkTemplate, chainHeaderTemplate: string[], headTemplate: string[], bodyTemplate: string[]}}
 */
var Bracelet = {

    /**
     * where all the Bracelet work variables go
     */
    workspace: {},

    /**
	 * <h4>init()<hr></h4>
	 * <p>inits the basic Bracelet object</p>
	 *
	 */
	init: function() {

        braceletLinkRoot = Anklet.Config.constants.bracelet_linkFolder;
		// include the bracelet routes
		let braceletList = require('./route_list.js').BraceletRouteList;

		// add on the bracelet-specific routes and link(s)
		Anklet.Route.getRoutes(Anklet.service, braceletList);
		Anklet.LinkManager.loadAllLinks(braceletLinkRoot);
		this.loadChain('core');

		// insert any Handlebars helpers
        this.setHandlebarsHelpers();

    },

    /**
     * <h4>setHandlebarsHelpers()<hr></h4>
     * <p>add any helpers for Handlebars</p>
     *
     */
    setHandlebarsHelpers: function () {

	    // load up Handlebars
        const Handlebars = require('handlebars');

        // a quick handler to put text arrays into multiline text for textareas
        Handlebars.registerHelper('arrayText', function (textArray) {
            if (textArray && Array.isArray(textArray)) {
                let thisContent = textArray.join('\n');
                return thisContent;
            }
            else if (textArray) {
                return textArray.toString();
            }
            else {
                return '';
            }
        });

        // an equality checker
        Handlebars.registerHelper('isEqual', function (cond1, cond2, options) {
            if (cond1 == cond2)
                return options.fn(this);
            else
                return options.inverse(this);
        });

        // an empty checker
        Handlebars.registerHelper('isNotEmpty', function (inputValue, options) {
            if (inputValue && inputValue != undefined && inputValue != '')
                return options.fn(this);
            else
                return options.inverse(this);
        });
    },

    /**
     * <h4>loadChain()<hr></h4>
     * <p>loads the chain object from fs and returns its JSON</p>
     *
     * @param chainName
     * @returns string
     */
    loadChain: function(chainName) {
        let path = require('path'),
            fs = require('fs'),
            filePath,
            fileData,
            fileObj;

        // figure out its path
        filePath = path.join(Anklet.Config.constants.chainFolder, chainName + '.json');

        // get the chain json file
        fileData = fs.readFileSync(filePath, 'utf8');

        // was there a read error?
        if (typeof(fileData) === 'error') {
            console.error(fileData);
            throw (fileData);
        }

        // otherwise, parse the chain
        fileObj = JSON.parse(fileData);

        // was there a parse error?
        if (typeof(fileObj) === 'error') {
            console.error.log(fileObj);
            throw (fileObj);
        }

        // save the return data
        this.workspace.chain = fileObj;
        this.workspace.code = JSON.stringify(fileObj, null, 4);
        return JSON.stringify(fileObj);

    },

    /**
     * <h4>saveChain()<hr></h4>
     * <p>write current chain to disk</p>
     *
     * @param filename
     * @param chainData
     */
    saveChain: function(filename, chainData) {
        let path = require('path'),
            fs = require('fs'),
            filePath;

        // figure out its path
        filePath = path.join(Anklet.Config.constants.chainFolder, filename + '.json');

        // fix any ampersands that have popped up
        chainData = chainData.replace(/&/g, '%26');

        // get the chain json file
        fs.writeFileSync(filePath, chainData, 'utf8');

    },

    /**
     * <h4>addLinkToChain()<hr></h4>
     * <p>adds a generic core link to the end of the current chain</p>
     *
     * @param workspace
     */
    addLinkToChain: function(workspace) {
        const newId = this.getNewId(workspace.chain),
            newLinkData = {
                "id": newId,
                "title": "New link",
                "link": "core"
            };

        let thisChain = workspace.chain,
            newLink = Anklet.LinkManager.loadLink(newLinkData);

        thisChain.links.push(newLink);
        workspace.code = JSON.stringify(workspace.chain, null, 4);
    },

    /**
     * <h4>getNewId()<hr></h4>
     * <p>finds the highest id in the chain and tops it by one</p>
     *
     * @param chain
     * @returns {number}
     */
    getNewId: function(chain) {
        let thisId = 0,
            thisLink;
        for (thisLink of chain.links) {
            thisId = Math.max(thisId, parseInt(thisLink.id));
        }
        return thisId + 1;
    },

    /**
     * <h4>pseudoEncodeSafeText()<hr></h4>
     * <p>because seriously, all we care about are the ampersands. and the percents.</p>
     *
     * @param thisText
     * @returns {string | * | XML | void}
     */
    pseudoEncodeSafeText: function(thisText) {
        let thatText = thisText.replace(/&/g, '%26');
            thatText = thatText.replace(/%/g, '%25');
        return thatText;
    },

    /**
     * <h4>applyChainCode()<hr></h4>
     * <p>takes the chain listing from send and applys it to the workspace</p>
     *
     * @param workspace
     */
    applyChainCode: function(workspace) {

        const thisChain = JSON.parse(Anklet.postData.script);
        this.workspace.chain = thisChain;
        // yes, we could just use the workspace.chain, but i want to regularize it just in case
        this.workspace.code = JSON.stringify(thisChain, null, 4);

    },

    /**
     * <h4>moveLink()<hr></h4>
     * <p>moves a link one position up or down in the chain</p>
     *
     * @param workspace
     */
    moveLink: function(workspace) {

        const thisId = Anklet.postData.id,
            thisMove = Anklet.postData.move;
        let thisChain = workspace.chain,
            theseLinks = thisChain.links,
            thisLink,
            thisIndex,
            i;

        for (i in theseLinks) {
            thisLink = theseLinks[i];
            if (thisLink.id == thisId) {
                thisIndex = i;
                break;
            }
        }

        if (thisMove == 'up') {
            if (thisIndex > 0) {
                thisLink = theseLinks.splice(thisIndex, 1)[0];
                theseLinks.splice(--thisIndex, 0, thisLink);
            }
        }
        else {
            if (thisIndex < theseLinks.length-1) {
                thisLink = theseLinks.splice(thisIndex, 1)[0];
                theseLinks.splice(++thisIndex, 0, thisLink);
            }
        }
        this.workspace.chain.links = theseLinks;
        this.workspace.selectedLink = thisLink.id;
        this.workspace.code = JSON.stringify(this.workspace.chain, null, 4);

    },

    /**
     * <h4>trashLink()<hr></h4>
     * <p>remove a link from the chain</p>
     *
     * @param workspace
     */
    trashLink: function(workspace) {

        const thisId = JSON.parse(Anklet.postData.id);
        let thisChain = workspace.chain,
            theseLinks = thisChain.links,
            thisLink,
            thisIndex,
            i;

        for (i in theseLinks) {
            thisLink = theseLinks[i];
            if (thisLink.id == thisId) {
                thisIndex = i;
                break;
            }
        }

        theseLinks.splice(thisIndex, 1);
        this.workspace.chain.links = theseLinks;
        this.workspace.selectedLink = null;
        this.workspace.code = JSON.stringify(this.workspace.chain, null, 4);
    },

    /**
     * <h4>loadChainCall()<hr></h4>
     * <p>called by bracelet, loads a chain into bracelet editor</p>
     *
     * @param workspace
     * @returns {*}
     */
    loadChainCall: function(workspace) {
        const thisFileName = Anklet.postData.filename;

        let thisChain = this.loadChain(thisFileName);
        return thisChain;

    },

    /**
     * <h4>saveChainCall()<hr></h4>
     * <p>called by bracelet, saves the currently edited chain to disk</p>
     *
     * @param workspace
     */
    saveChainCall: function(workspace) {
        const thisFileName = workspace.postData.filename,
            chainData = workspace.postData.content;

        this.saveChain(thisFileName, chainData);

    },

    /**
     * <h4>saveLinkCall()<hr></h4>
     * <p>called by bracelet, saves the edited link back into the current chain</p>
     *
     * @param workspace
     */
    saveLinkCall: function (workspace) {
        const extend = require('util')._extend;
        let theseFields = workspace.postData,
            thisId = theseFields.id,
            theseKeys,
            thisKey,
            thisLink,
            thisIndex,
            thisValue,
            i;

        // get the link in question and its index
        for (i = 0; i < workspace.chain.links.length; i++) {
            thisLink = workspace.chain.links[i];
            if (thisLink.id == thisId) {
                thisIndex = i;
                break;
            }
        }

        // clear out spurious choose buttons and core fields
        if (theseFields['link-chooser']) {
            delete theseFields['link-chooser'];
        }
        if (theseFields['object-tree']) {
            delete theseFields['object-tree'];
        }
        if (thisLink['name']) {
            delete thisLink['name'];
        }
        if (thisLink['fullname']) {
            delete thisLink['fullname'];
        }
        // merge the new fields, URI-encode them (if necessary) and split any arrays
        theseKeys = Object.keys(theseFields);
        for (thisKey of theseKeys) {
            thisValue = theseFields[thisKey];
            if (thisValue.indexOf('&') > -1 || thisValue.indexOf('%') > -1) {
                thisValue = this.pseudoEncodeSafeText(thisValue);
            }
            if (thisValue.indexOf('\n') > -1) {
                thisValue = thisValue.split(/\n/g);
            }
            if (thisValue.indexOf('%0A') > -1) {
                thisValue = thisValue.split(/%0A/g);
            }
            thisLink[thisKey] = thisValue;
        }

        this.workspace.chain.links[thisIndex] = thisLink;
        this.workspace.code = JSON.stringify(this.workspace.chain, null, 4);
    },

    /**
     * <h4>getHeaderCall()<hr></h4>
     * <p>called by bracelet, gets the header edit panel</p>
     *
     * @param workspace
     * @returns {*}
     */
    getHeaderCall: function(workspace) {
        const Handlebars = require("handlebars"),
            thisHeader = workspace.chain.header,
            thisTemplate = Handlebars.compile(this.chainHeaderTemplate.join('\n'));

        return thisTemplate(thisHeader);
    },

    /**
     * <h4>saveHeaderCall()<hr></h4>
     * <p>called by bracelet, save the edited header back into the chain</p>
     *
     * @param workspace
     */
    saveHeaderCall: function(workspace) {
        const theseFields = workspace.postData;
        let theseKeys = Object.keys(theseFields),
            thisKey,
            thisValue,
            thisHeader = {};

        for (thisKey of theseKeys) {
            thisValue = theseFields[thisKey];
            if (thisValue.indexOf('&') > -1 || thisValue.indexOf('%') > -1) {
                thisValue = this.pseudoEncodeSafeText(thisValue);
            }
            if (thisValue.indexOf('\n') > -1) {
                thisValue = thisValue.split(/\n/g);
            }
            if (thisValue.indexOf('%0A') > -1) {
                thisValue = thisValue.split(/%0A/g);
            }
            thisHeader[thisKey] = thisValue;
        }

        this.workspace.chain.header = thisHeader;
        this.workspace.code = JSON.stringify(this.workspace.chain, null, 4);
    },

    /**
     * <h4>listLinkCall()<hr></h4>
     * <p>called by bracelet, this renders the edit panel for the requested link</p>
     *
     * @param workspace
     * @returns {*}
     */
    listLinkCall: function(workspace) {
        
        let thisId = workspace.postData.id,
            choose = workspace.postData.choose,
            thisLink,
            thisOutput,
            i;
        
        // fi choose wasn't sent, default to false
        if (!choose) {
            choose = false;
        }

        for (i = 0; i < workspace.chain.links.length; i++) {
            thisLink = workspace.chain.links[i];
            if (thisLink.id == thisId) {
                break;
            }
        }

        thisLink = Anklet.LinkManager.loadLink(thisLink),
        thisOutput = this.linkTemplate(thisLink, choose);
        return thisOutput;
    },

    /**
     * <h4>setTemplateContext()<hr></h4>
     * <p>sets a three-level context for the list of fields in a link</p>
     * <p>using the customList array, it separates the link into:</p>
     * <ul>
     *      <li>the header section (id, title, linktype, rendered by the head template)
     *      <li>the custom section (whatever is in the customList array, to be rendered by the custom template in the link definiition)
     *      <li>anything else (rendered by the body template)
     * </ul>
     * <p>this way, developer can control the appearance of a link's edit panel</p>
     *
     * @param data
     * @param link
     * @returns {{}}
     */
    setTemplateContext: function(data, link) {

        const extend = require('util')._extend,
            theseProperties = link.customList;
        let thisContext = {},
            thisKey,
            thisValue;

        // set up the required head fields
        thisContext.head = {};
        thisContext.head.id = data.id;
        thisContext.head.title = data.title;
        thisContext.head.link = data.link;
        thisContext.head.exec = data.exec;

        // remove them
        data.id = undefined;
        data.title = undefined;
        data.link = undefined;
        data.exec = undefined;

        // set up any custom properties
        thisContext.custom = {};
        if (theseProperties) {
            for (thisKey of theseProperties) {
                thisContext.custom[thisKey] = data[thisKey];
                data[thisKey] = undefined;
            }
        }

        // now, copy over any remaining properties
        thisContext.body = {};
        for (thisKey in data) {
            thisValue = data[thisKey];
            if (thisValue != undefined) {
                thisContext.body[thisKey] = thisValue;
            }
        }

        return thisContext;
    },

    /**
     * <h4>linkTemplate()<hr></h4>
     * <p>renders the edit panel for a link in bracelet</p>
     * <p>if it a core link, presents a chooser for another link type, then presents a new edit panel for the full link</p>
     *
     * @param linkData
     * @returns {*}
     */
    linkTemplate: function(linkData, choice) {

        const Handlebars = require('handlebars'),
            extend = require('util')._extend;
        let thisOutput,
            bodyLength,
            bodyOutput = '',
            customOutput = '';
            
        if (linkData.link !== 'core' && !choice) {

            try {
                // it's not a core link, so prep and run the templates we need
                const newLink = Anklet.LinkManager.loadLink(linkData),
                    headTemplate = Handlebars.compile(this.headTemplate.join('\n')),
                    bodyTemplate = Handlebars.compile(this.bodyTemplate.join('\n')),
                    newLinkData = Anklet.LinkManager.stripLink(extend({}, newLink)),
                    newContext = this.setTemplateContext(newLinkData, newLink);

                // newContext should have three subs: head, custom, and body
                const headOutput = headTemplate(newContext.head);

                // if there is a custom section:
                if (newLink.template) {
                    const customTemplate = Handlebars.compile(newLink.template.join('\n'));
                    customOutput = '<hr />\n' + customTemplate(newContext.custom);
                }

                // do we need a body section?
                bodyLength = Object.keys(newContext.body);
                if (bodyLength[0]) {
                    bodyOutput = '<hr />\n' + bodyTemplate(newContext.body);
                }

                thisOutput = headOutput + customOutput + bodyOutput;
            }
            catch(error) {
                throw error;
            }
        }
        else {
            // it's a core link, we need to choose a different type
            // here we present a simple chooser with a dropdown of all the available types
            const thisNodeList = Anklet.LinkManager.listOfLinkNodes(),
                pickerTemplate = Handlebars.compile(this.linkSelect.join('\n'));

            let thisPicker = {};
                thisPicker.id = linkData.id;
                thisPicker.linkName = linkData.link;
                thisPicker.nodeList = thisNodeList;
                
            thisOutput = pickerTemplate(thisPicker);
        }
        return thisOutput;

    },

    /**
     * parts and pieces for the link picker
     */
    linkSelect: 
    [
        "<input type=\"hidden\" id=\"edit\" value=\"linkType\">",
        "<input type=\"hidden\" id=\"linkType\" value=\"{{linkName}}\">",
        "<input type=\"hidden\" id=\"link-id\" value=\"{{id}}\">",
        "<div class=\"field-name\">Choose a link type</div>",
        "<div class=\"form-group\" id=\"link-picker\">",
        "</div>",
        "<script type=\"text/javascript\">",
        "(function($) {",
        "    function setLinkPicker(linkName, linkNodes) {",
        "        var pickTemplate = [",
        "            '<span class=\"form-group select\">',",
        "                '    <select name=\"linkType-||level||\" class=\"form-control-narrow link-type\" id=\"linkType-||level||\">',",
        "                    '||options||',",
        "                    '    </select>',",
        "                    '</span>',",
        "                    '<span id=\"next-||level||\"></span>'",
        "                ].join('\\n'),",
        "        optTemplate = '<option value=\"||value||\" ||selected||>||value||</option>\\n',",
        "        levels = linkName.split(/\\./g),",
        "        i,",
        "        setLevel = function(level, linkName) {",
        "            var theseLinkNodes = JSON.parse(JSON.stringify(linkNodes)),",
        "                theseKeys,",
        "                nameLevels = linkName.split(/\\./g),",
        "                theseOptions = '',",
        "                selected,",
        "                $thisContainer,",
        "                thisPicker,",
        "                i;",
        "            ",
        "            // get the level keys",
        "            if (level > 0 && linkName != 'core') {",
        "                for (i = 0; i < level; i++) {",
        "                    theseLinkNodes = theseLinkNodes[nameLevels[i]];",
        "                }",
        "            }",
        "            theseKeys = Object.keys(theseLinkNodes);",
        "            if (nameLevels[level] == '' || linkName == 'core') {",
        "                theseKeys.unshift('');",
        "            }",
        "",
        "            // make the options list",
        "            for (i = 0; i < theseKeys.length; i++) {",
        "                selected = '';",
        "                if (theseKeys[i] == nameLevels[level]) {",
        "                    selected = 'selected=\"selected\"';",
        "                }",
        "                theseOptions += optTemplate.replace(/\\|\\|value\\|\\|/g, theseKeys[i]).replace(/\\|\\|selected\\|\\|/g, selected);",
        "            } ",
        "            // get the container",
        "            if (level == 0) {",
        "                // it's the first one",
        "                $thisContainer = $('#link-picker');",
        "            }",
        "            else {",
        "                // it's a further one",
        "                $thisContainer = $('#next-' + (level - 1));",
        "            }",
        "            thisPicker = pickTemplate.replace(/\\|\\|level\\|\\|/g, level).replace(/\\|\\|options\\|\\|/g, theseOptions);",
        "            $thisContainer.html(thisPicker);",
        "        };",
        "        ",
        "        // set the return value field",
        "        $('#linkType').val(linkName);",
        "        ",
        "        // set the select values",
        "        for (i = 0; i < levels.length; i++) {",
        "            setLevel(i, linkName);",
        "        }",
        "        // set the select handlers",
        "        $('.link-type').on('change', function(event) {",
        "            var thisLevel = event.target.id.split(/-/)[1];",
        "            gatherLinkName(thisLevel, linkNodes);",
        "        })",
        "    };",
        "",
        "    function gatherLinkName(level, linkNodes) {",
        "        var theseLinkNodes = JSON.parse(JSON.stringify(linkNodes)),",
        "            thesePickers = $('.link-type'),",
        "            linkNames = [],",
        "            thisLinkName,",
        "            i;",
        "        ",
        "        // make sure we don't run off the end",
        "        if (thesePickers.length < (level + 1)) {",
        "            level = thesePickers.length - 1;",
        "        }",
        "",
        "        // gather the link names",
        "        for (i = 0; i <= level; i++) {",
        "            thisLinkName = $('#linkType-' + i).val();",
        "            linkNames.push(thisLinkName);",
        "            theseLinkNodes = theseLinkNodes[thisLinkName];",
        "        }",
        "        // if there are more levels to go, leave room",
        "        if (typeof(theseLinkNodes) == 'object') {",
        "            linkNames.push('');",
        "        }",
        "",
        "        // push out the gathered name",
        "        setLinkPicker(linkNames.join('.'), linkNodes);",
        "",
        "    };",
        "",
        "    var linkNodes = {{{nodeList}}};",
        "    setLinkPicker($('#linkType').val(), linkNodes);",
        "",
        "",
        "})(jQuery);",
        "</script>",
        ""
    ],


    /**
     * <h4>chainHeaderTemplate<hr></h4>
     * <p>the edit panel template for the overall chain header</p>
     *
     */
    chainHeaderTemplate:
        [
            "<input type=\"hidden\" id=\"edit\" value=\"link\">",
            "<div class=\"col-grid col-1-1-1-1\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-name\" for=\"name-input\">name</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"name\" type=\"text\" class=\"form-control\" id=\"name-input\" value=\"{{name}}\">",
            "        </div>",
            "    </div>",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"ada-title\" for=\"title-input\">title</label>",
            "    </div>",
            "    <div class=\"\">",
            "        <div class=\"form-group\">",
            "            <input name=\"title\" type=\"text\" class=\"form-control\" id=\"title-input\" value=\"{{title}}\">",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid col-1-1-1-1 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-path\" for=\"path-input\">path</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"path\" type=\"text\" class=\"form-control\" id=\"path-input\" value=\"{{path}}\">",
            "        </div>",
            "    </div>",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-method\" for=\"method-input\">method</label>",
            "    </div>",
            "    <div class=\"\">",
            "        <div class=\"form-group select\">",
            "            <label id=\"label-method-2\" for=\"method-select-input\">",
            "            <select aria-describedby=\"ada-method-select-describedby\" aria-labelledby=\"ada-method-select-describedby\" name=\"method\" class=\"form-control\" id=\"method-select-input\">",
            "                <option value=\"{{method}}\">{{method}}</option>",
            "                <option value=\"\">----- Other values -----</option>",
            "                <option value=\"none\">none</option>",
            "                <option value=\"POST\">POST</option>",
            "                <option value=\"GET\">GET</option>",
            "            </select>",
            "            </label>",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid col-1-3 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-testQuery\" for=\"testQuery-input\">testQuery</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"testQuery\" type=\"text\" class=\"form-control\" id=\"testQuery-input\" value=\"{{testQuery}}\">",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid col-1-3 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-testPost\" for=\"testPost-input\">testPost</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"testPost\" type=\"text\" class=\"form-control\" id=\"testPost-input\" value=\"{{testPost}}\">",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid\">",
            "    <div class=\"align-left area-name\">",
            "        <label id=\"label-description\" for=\"description-input\">description</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <textarea name=\"description\" type=\"text\" class=\"form-control\" id=\"description-input\" rows=\"10\">{{arrayText description}}</textarea>",
            "        </div>",
            "    </div>",
            "</div>",
            ""
        ],

    /**
     * <h4>headTemplate<hr></h4>
     * <p>the edit panel template for the link head elements</p>
     *
     */
    headTemplate:
    [
        "<input type=\"hidden\" id=\"edit\" value=\"link\">",
        "<div class=\"col-grid col-1-1-1-1-2\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-id\" for=\"id-input\">id</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"id\" type=\"text\" class=\"form-control\" id=\"id-input\" value=\"{{id}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"ada-exec\" for=\"exec-input\">exec</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group\">",
        "            <input name=\"exec\" type=\"text\" class=\"form-control\" id=\"exec-input\" value=\"{{exec}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-title\" for=\"title-input\">title</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"title\" type=\"text\" class=\"form-control\" id=\"title-input\" value=\"{{title}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3-1 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"ada-link\" for=\"link-input\">link</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group\">",
        "            <input name=\"link\" type=\"text\" class=\"form-control\" id=\"link-input\" value=\"{{link}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-center link-field\">",
        "        <div class=\"form-group\">",
        "            <input type=\"button\" class=\"form-control flat-button\" style=\"padding: 0\" id=\"link-chooser-button\" name=\"link-chooser\" value=\"Choose\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<script type=\"text/javascript\">",
        "(function($) {",
        "    $('#link-chooser-button').on('click', function(event) {",
        "        event.preventDefault();",
        "        var thisId = $('#id-input').val(),",
        "            options = {",
        "                url: '/anklet/bracelet/link',",
        "                data: 'id=' + thisId + '&choose=true',",
        "                success: function(data) {",
        "                    $('#editLink').html(data);",
        "                }",
        "            };",
        "",
        "        sendPost(options);",
        "        $('#thisEditDialog').dialog('option', 'title', 'Choose Link ' + thisId + '\\'s link type');",
        "        $('#thisEditDialog').dialog('open');",
        "    });",
        "})(jQuery);",
        "</script>"
    ],

    /**
     * <h4>bodyTemplate<hr></h4>
     * <p>the basic template for rendering link fields with no other templating defs</p>
     *
     */
    bodyTemplate:
        [
            "{{#each this}}",
            "<div class=\"col-grid col-1-3 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-{{@key}}\" for=\"{{@key}}-input\">{{@key}}</label>",
            "    </div>",
            "    <div class=\"\">",
            "        <div class=\"form-group\">",
            "            <input name=\"{{@key}}\" type=\"text\" class=\"form-control\" id=\"{{@key}}-input\" value=\"{{this}}\">",
            "        </div>",
            "    </div>",
            "</div>",
            "{{/each}}",
            ""
        ]
};

module.exports.Bracelet = Bracelet;
