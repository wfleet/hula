'use strict';
/**
 * <h4>bracelet_routeList<hr></h4>
 *
 * <p>set up a list of Routes for the Route object</p>
 */

let bracelet_routeList = [

	// bracelet routes -- don't edit these
	{
		basePath: '/bracelet',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/main_page.json'
	},
	{
		basePath: '/bracelet/list',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/chain_list.json'
	},
	{
		basePath: '/bracelet/info',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/chain_info.json'
	},
	{
		basePath: '/bracelet/code',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/chain_code.json'
	},
	{
		basePath: '/bracelet/modal',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/page_modals.json'
	},
	{
		basePath: '/bracelet/view',
		type: 'chain',
		target: Anklet.Config.constants.bracelet_chainFolder + '/render_window.json'
	},

    // static folders
	{
		basePath: '/bracelet/assets/{filename*}',
		type: 'static',
		target: Anklet.Config.constants.bracelet_assetsFolder
	},
    {
        basePath: '/anklet/bracelet/assets/css/{filename*}',
        type: 'static',
        target: Anklet.Config.constants.bracelet_assetsFolder + '/css'
    },
    {
        basePath: '/anklet/bracelet/assets/js/{filename*}',
        type: 'static',
        target: Anklet.Config.constants.bracelet_assetsFolder + '/js'
    },


    // bracelet action calls below
	{
		basePath: '/bracelet/apply',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/apply_chain_code.json'
	},
	{
		basePath: '/bracelet/add',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/add_link.json'
	},
	{
		basePath: '/bracelet/move',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/move_link.json'
	},
	{
		basePath: '/bracelet/trash-link',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/trash_link.json'
	},
	{
		basePath: '/bracelet/load',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/load_chain.json'
	},
	{
		basePath: '/bracelet/save',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/save_chain.json'
	},
	{
		basePath: '/bracelet/edit',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/edit_link.json'
	},
	{
		basePath: '/bracelet/link',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/list_link.json'
	},
	{
		basePath: '/bracelet/choose-link',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/choose_link.json'
	},
	{
		basePath: '/bracelet/get-header',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/get_header.json'
	},
	{
		basePath: '/bracelet/edit-header',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/edit_header.json'
	},
	{
		basePath: '/bracelet/object-tree',
		type: 'post',
		target: Anklet.Config.constants.bracelet_chainFolder + '/object.json'
	}
];

module.exports.BraceletRouteList = bracelet_routeList;
