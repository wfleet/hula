/**
 * <h4>Hula<hr></h4>
 *
 * <p>A Unified Resource Broker (URB)</p>
 * <img src="./bracelet/assets/hula.png" height="176" width="468" style="float: right">
 * <p>the Anything Framework</p>
 *
 * <ul>
 *     <li>Hula is a framework for generating structured documents
 *     <li>If consists of two frameworks:
 *     <li>Anklet, the service comnponent
 *     <li>Bracelet, the editor interface
 *     <li>Typically, all other modules/classes are subbed from this one
 * </ul>
 *
 * @example
 * // to invoke from the command line:
 * node hula.js
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Hula
 * @version 0.1
 *
 * @type {{init: Hula.init}}
 */
Hula = {

	init: function() {

		// read in anklet and run it
		console.info('Hula: loading the Anklet service...');
		Anklet = require('./anklet/anklet.js').Anklet;

		// run anklet
		Anklet.init();

		/**
		 * if we are going to use bracelet, run it here
		 * if not, don't run it
		 */
		// read in bracelet and run it
		if (Anklet.Config.loadBracelet) {
			console.info('Hula: loading the Bracelet service...');
			Bracelet = require('./bracelet/bracelet.js').Bracelet;

			// run bracelet
			Bracelet.init();
		}
		else {
			console.info('Hula: Bracelet service is not loading...');
		}

	}

};

// start the hula application
console.info('Hula: starting the hula application..');
Hula.init();

