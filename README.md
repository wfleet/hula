# hula

_Hula, the anything framework_

hula is a framework for generating structured documents from multiple sources.

It does this by running Chains of Links, collecting their output, and returning it to the requestor.

A Link is an individual component, or operator, that gathers, transforms, or performs computation on various network and local resources.

A Chain is a document that describes a series of Links, and all of their operating parameters.

Hula consists of Anklet, the service that executes the Chains, and Bracelet, the editing environment.

Bracelet is implemented as a web application, rendered by a series of Anklet calls.

Hula's overall paradigm is a Chain, or series of Links, or of Lego-like bricks, that are easily snapped together 
to generate any form of document in response to a web service call.



### running hula from node.js command line

hula requires node.js version 8 or better. Ensure that this version is available and enabled.

`node hula`



### running hula within a Docker container

A little more complicated, but safer, and won't collide with other node apps.

Docker wants:
* a working Docker installation, running

If system security requires it, you may need to prepend the `sudo` command to connect to the running Docker instance.

##### to build:

`docker build -t hula .`

##### to run:

`docker run hula`

_The logs will show the Anklet startup notices, ending with_

_`Anklet service started and listening at http://{image_id}:{port}`_

##### to stop:

`docker stop {image_id}`

##### to restart:

`docker restart {image_id}`

_get the image_id from the startup notices above, or enter `docker images` and find the one for the hula repository._



### running hula in a Docker container with IntelliJ debug

In addition to the node.js and Docker requirements above, IntelliJ wants:
* the _NodeJs_ and _Node.js Remote Interpreter_ plugins installed and enabled (required)
* the _Docker Integration_ plugin (optional, but handy)

In the `~/tools` folder is the `JetBrains.Docker_debug_snippet.xml` file. 
Copy and paste its contents into the `/.idea/workspace.xml` document in the `RunManager` component section. 
Or you can set up a Node.js runtime configuration and aet the values manually.

Inspect the IntelliJ runtime configuration dialog to ensure there are no alerts or errors.

If you haven't already done so, run build on a command line: `docker build -t hula .` 
This will build the hula instance into a named docker container.

In IntelliJ, run (or debug) the _hula_docker_ run configuration (or whatever you named it). 
It may take a moment but the debugger console should fill up with startup notifications. 
IntelliJ's debug features should now be available, including breakpoints and object inspection.

### working with hula in a Docker container

Typically, you should be able to observe running code as you are writing it. 

To push code changes into the running Docker container, enter `docker build -t hula .` on a command line. This will copy 
the changes into the Docker container. If the changes are only to chains, then the changes should be available 
immediately. If they are not, or if the changes are to link modules or other javascript components, then restart 
the container:
* in IntelliJ, click the restart button in the debugger.
* in command mode, enter `docker restart {image_id}` (to get the image_id, enter `docker images` and find the 
one for the hula repository).

To stop, click the stop button in IntelliJ's debugger, or enter `docker stop {image_id}` on a command line.