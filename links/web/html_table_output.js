/**
 * <h4>Link.table.output<hr></h4>
 *
 * <p>Definition for the output link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.html_table_output
 * @version 0.1
 */

const html_table_output = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'html_table_output',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.web.html_table_output',

    /**
     * <h4>outputType<hr></h4>
     * @type {string}
     */
    outputType: 'html',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'outputType',
        'target',
        ''
    ],

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            workspace.output[this.id] = this.outputTable(workspace);
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     * @type {string[]}
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-outputType\" for=\"outputType-select-input\">outputType</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-outputType-2\" for=\"outputType-select-input\">",
        "            <select aria-describedby=\"ada-outputType-select-describedby\" aria-labelledby=\"ada-outputType-select-describedby\" name=\"outputType\" class=\"form-control\" id=\"outputType-select-input\">",
        "                <option value=\"{{outputType}}\">{{outputType}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"html\">html</option>",
        "                <option value=\"markdown\">markdown</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
    ],

    /**
     * <h4>outputTable()<hr></h4>
     * <p>renders a hula table as either html or markdown</p>
     * <p>more formats may be added later</p>
     *
     * @returns {string|*}
     */
    outputTable: function(workspace) {

        const Handlebars = require('handlebars');

        let workTable = workspace[this.target].table,
            thisRow,
            thisOutput,
            tableStart,
            tableClose,
            rowStart,
            rowClose,
            thisCell,
            thisCellOut;

        if (this.outputType === 'html') {
            // it's an html table
            tableStart = '\n<table border="1" cellspacing="2" cellspacing="2">\n';
            tableClose = '</table>\n';
            rowStart = '<tr>\n';
            rowClose = '</tr>\n';
            thisCell = '<td style="{{#if font}}font-family: {{font}}; {{/if}}{{#if size}}font-size: {{size}}; {{/if}}{{#if color}}color: {{color}}; {{/if}}{{#if align}}text-align: {{align}}{{/if}}">{{value}}</td>\n';
        }
        else {
            // for now, the other default is markdown
            tableStart = '\n';
            tableClose = '\n';
            rowStart = '';
            rowClose = ' |\n';
            thisCell = '| {{value}}';
        }
        thisCellOut = Handlebars.compile(thisCell);

        thisOutput = tableStart;
        while (workTable.length > 0) {
            thisOutput += rowStart;
            thisRow = workTable.shift();
            while (thisRow.length > 0) {
                thisCell = thisRow.shift();
                thisOutput += thisCellOut(thisCell);
            }
            thisOutput += rowClose;
        }
        thisOutput += tableClose;

        return thisOutput;

    }
};

module.exports.html_table_output = html_table_output;