/**
 * <h4>Link.web.web_call<hr></h4>
 *
 * <p>Definition for the web_call link module</p>
 *
 * <p>Allows meta calls to web functions.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.web.web_call
 * @version 0.1
 * @type {{name: string, fullname: string, command: string, fetch: Link.web.web_call.fetch, render: Link.web.web_call.render}}
 */

let web_call = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'web_call',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.web.web_call',

    /**
     * <h4>command<hr></h4>
     * <p>The web function to invoke with this call</p>
     * @type {string}
     */
    command: '',

    /**
     * <h4>module<hr></h4>
     * <p>The module containing the command to invoke</p>
     * @type {string}
     */
    module: '',

    /**
     * <h4>fetch()<hr></h4>
     * <p>overrides the core method</p>
     * <p>here, we find the command for a web call and call the appropriate method in its module</p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const thisCommand = this.command,
                thisModule = this.module;
            workspace.output[this.id] = Anklet[thisModule][thisCommand];
        }
        catch(error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p>overrides the core method a little bit</p>
     *
     * @param workspace
     */
    render: function(workspace) {
        try {
            const thisOutput = workspace.output[this.id];
            if (thisOutput && thisOutput.trim().length > 0) {
                workspace.output[this.id] = thisOutput;
            }
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

};

module.exports.web_call = web_call;

