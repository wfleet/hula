/**
 * <h4>Link.web.html_block<hr></h4>
 *
 * <p>Definition for the html_block link module</p>
 *
 * <p>Emits an HTML segment.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.web.html_block
 * @version 0.1
 * @type {{name: string, fullname: string, content: Array, script: Array, render: Link.web.html_block.render, customList: [string , string], template: Array}}
 */

let html_block = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'html_block',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.web.html_block',

    /**
     * <h4>content<hr></h4>
     * <p>Holder for the html block</p>
     *
     * @type {Array}
     */
    content: [],

    /**
     * <h4>script<hr></h4>
     * <p>Holder for script to run on the client</p>
     *
     * @type {Array}
     */
    script: [],
    top: '',
    left: '',
    context: '',

    /**
     * <h4>render()<hr></h4>
     * <p>renders the handlebars template</p>
     * <p>uses the link's 'data' element as the model for the render</p>
     *
     * @param workspace
     */
	render: function(workspace) {

		try {
			// a bunch of constants
			const Handlebars = require("handlebars");

			let thisTemplate = this.content,
				thatTemplate,
                thisData;

			if (Array.isArray(thisTemplate)) {
                thatTemplate = Handlebars.compile(thisTemplate.join('\n'));
            }
            else {
                thatTemplate = Handlebars.compile(thisTemplate);
            }

            if (this.context) {
                // TODO: - clean up eval usage
                thisData = eval(this.context);
            }
            else {
                thisData = '';
            }

            if (this.script && this.script.length > 0) {
				let thisScript = '<script type="text/javascript">\n' + this.script.join('\n') + '\n</script>';
                workspace.output[this.id] = thatTemplate(thisData) + thisScript;
            }
            else {
                workspace.output[this.id] = thatTemplate(thisData);
            }
		}
		catch (error) {
		    console.error(error);
			throw error;
		}
	},

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'content',
        'script',
        'top',
        'left',
        'context'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-top\" for=\"top-input\">top</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"top\" type=\"text\" class=\"form-control\" id=\"top-input\" value=\"{{top}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-left\" for=\"left-input\">left</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"left\" type=\"text\" class=\"form-control\" id=\"left-input\" value=\"{{left}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-context\" for=\"context-input\">context</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"context\" type=\"text\" class=\"form-control\" id=\"context-input\" value=\"{{context}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-content\" for=\"content-input\">content</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"content\" type=\"text\" class=\"form-control nowrap\" id=\"content-input\" rows=\"10\">{{arrayText content}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-script\" for=\"script-input\">script</label>",
        "    </div>",
        "        <div class=\"form-group\">",
        "            <textarea name=\"script\" type=\"text\" class=\"form-control nowrap\" id=\"script-input\" rows=\"10\">{{arrayText script}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]
};

module.exports.html_block = html_block;

