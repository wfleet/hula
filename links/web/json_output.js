/**
 * <h4>Link.web.json_output<hr></h4>
 *
 * <p>Definition for the html_section link module</p>
 *
 * <p>Emits an HTML page section.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.web.json_output
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let json_output = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'json_output',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.html.json_output',

    context: '',

    /**
     * <h4>render()<hr></h4>
     * <p>basically just outputs the stringified JSON tree</p>
     *
     * @param workspace
     */
    render(workspace) {

	    let thisSource = this.context.replace(/@/, ''),
            thisContext = workspace[thisSource];

        if (!workspace.output) {
            workspace.output = {};
        }
        workspace.output[this.id] = '<pre>' + JSON.stringify(thisContext, null, 4) + '</pre>';
    }


};

module.exports.json_output = json_output;

