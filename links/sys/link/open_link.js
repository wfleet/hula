/**
 * <h4>Link.sys.link.open_link<hr></h4>
 *
 * <p>Begins a link description chain</p>
 *
 * <p>This is part of a toolset that will expediate the stubbing out of link modules for Anklet.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.sys.link.open_link
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let open_link = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'open_link',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.sys.link.open_link',

	/**
	 * <h4>includes<hr></h4>
	 * <p>whether or not to override the base functions</p>
	 */
	link_name: '',
	include_fetch: true,
    include_render: true,
    include_zombieFetch: false,
    include_zombieRender: false,
    zombieFetch_name: false,
    zombieRender_name: false,

	/**
	 * <h4>include_properties<hr></h4>
	 * <p>a list of properties to include</p>
	 */
	include_properties: [],
	
	/**
	 * <h4>include_customList<hr></h4>
	 * <p>a list of properties for the template</p>
	 */
    include_customList: [],

	/**
	 * <h4>include_template<hr></h4>
	 * <p>the template</p>
	 */
	include_template: [],
	
    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
		'link_name',
        'include_fetch',
        'include_render',
        'include_zombieFetch',
        'include_zombieRender',
        'zombieFetch_name',
        'zombieRender_name',
		'include_properties',
		'include_customList',
		'include_template'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>renders the link from templates below</p>
     * <p>really just calls the zombieRender() function</p>
     *
     * @param workspace
     */
	render: function(workspace) {
		try {
			workspace.outputHeaders = [
				'Content-Type: text'
			];
			workspace.output[this.id] = this.zombieRender(workspace);
		}
		catch(error) {
			console.error(error);
			throw error;
		}
	},
	
	/**
	 * <h4>zombieRender()<hr></h4>
	 * <p>renders link from templates below</p>
	 * <p>called by render()</p>
     *
	 * @param workspace
     */
	zombieRender: function(workspace) {
		try {
			const Handlebars = require('handlebars');
			let linkDataObject = {},
				baseFunctionNames = {},
				linkOutput,
                templateOutput = '',
				thisTemplate;
			
			linkDataObject.pathName = this.link_name;
			linkDataObject.name = this.link_name.split(/\./g).pop();
			linkDataObject.propertyNames = this.include_properties;
			linkDataObject.customList = this.include_customList;

			// save the new link name for later
			workspace.name = linkDataObject.name;

			// set the header
			thisTemplate = Handlebars.compile(this.linkHeadTemplate.join('\n'));
			linkOutput = thisTemplate(linkDataObject);

			// set any properties
			if (linkDataObject.propertyNames.length > 0) {
			    if (!Array.isArray(linkDataObject.propertyNames)) {
                    linkDataObject.propertyNames = [ linkDataObject.propertyNames ];
                }
				thisTemplate = Handlebars.compile(this.propertyTemplate.join('\n'));
				linkOutput += thisTemplate(linkDataObject);
			}
			
			// set any customList items
			if (this.include_customList.length > 0) {
				thisTemplate = Handlebars.compile(this.customListTemplate.join('\n'));
				linkOutput += thisTemplate(this.getCustomList());
			}

            // some globals
            baseFunctionNames.link_name = this.link_name;
			if (this.include_properties) {
                baseFunctionNames.call_params = this.include_properties.join(', ');
                baseFunctionNames.caller_params = 'this.' + this.include_properties.join(', this.');
            }

            // figure out the fetches
			baseFunctionNames.functionName = 'fetch';
			baseFunctionNames.sideFunctionName = this.zombieFetch_name;
			baseFunctionNames.zombieName = 'z' + this.zombieFetch_name.charAt(0).toUpperCase() + this.zombieFetch_name.substring(1);
			if (this.include_fetch == 'true' && this.include_zombieFetch == 'true') {
				thisTemplate = Handlebars.compile(this.baseFunctionWithSideTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			else if (this.include_fetch == 'true') {
				thisTemplate = Handlebars.compile(this.baseFunctionTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			else if (this.include_zombieFetch == 'true') {
				thisTemplate = Handlebars.compile(this.sideFunctionTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			linkOutput += templateOutput;
			if (templateOutput && (this.include_render == 'true' || this.include_zombieRender == 'true')) {
                linkOutput += ",\n\n";
			}
			templateOutput = '';

			// figure out the renders
			baseFunctionNames.functionName = 'render';
			baseFunctionNames.sideFunctionName = this.zombieRender_name;
            baseFunctionNames.zombieName = 'z' + this.zombieRender_name.charAt(0).toUpperCase() + this.zombieRender_name.substring(1);
			if (this.include_render == 'true' && this.include_zombieRender == 'true') {
				thisTemplate = Handlebars.compile(this.baseFunctionWithSideTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			else if (this.include_render == 'true') {
				thisTemplate = Handlebars.compile(this.baseFunctionTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			else if (this.include_zombieRender == 'true') {
				thisTemplate = Handlebars.compile(this.sideFunctionTemplate.join('\n'));
                templateOutput = thisTemplate(baseFunctionNames);
			}
			if (templateOutput) {
                linkOutput += templateOutput;
            }

			// throw in the template to match the customList
            templateOutput = this.getCustomTemplate();
            if (templateOutput) {
                linkOutput += ",\n\n";
				thisTemplate = Handlebars.compile(this.templateTemplate.join('\n'));
				linkOutput += thisTemplate(templateOutput);
            }

			return linkOutput;
		}
		catch(error) {
			console.error(error);
			throw error;
		}

	},

    /**
     * <h4>getCustomList()<hr></h4>
     * <p>returns the basic customList from the input field</p>
     */
	getCustomList: function() {
		let thisItem,
			outItems = [];

		if (!Array.isArray(this.include_customList)) {
            this.include_customList = [ this.include_customList ];
        }

		for (thisItem of this.include_customList) {
			if (thisItem.indexOf(':') > -1) {
				thisItem = thisItem.split(':')[0];
			}
			if (thisItem.indexOf('|') > -1) {
				thisItem = thisItem.split('|')[0];
			}
			outItems.push(thisItem);
		}
		return {'customList': outItems};
	},

    /**
     * <h4>getCustomTemplate()<hr></h4>
     * <p>builds a template from the customList input field</p>
     * <p>each entry takes the form [name]:[type]|[number]</p>
     * <p>[name] is entered name for the field</p>
     * <p>[type] is either check', 'area' or 'select'. if none or other, defaults to 'input'</p>
     * <p>[number] is either 1 or 2, either full- or half-width, default is 1</p>
     *
     * @param workspace
     */
	getCustomTemplate: function() {
		const Handlebars = require('handlebars'),
			col_1 = 'col-1-3',
			col_2 = 'col-1-1-1-1',
			startRowTemp = Handlebars.compile(this.startRowTemplate.join('\n')),
			inputTemp = Handlebars.compile(this.inputTemplateTemplate.join('\n')),
			areaTemp = Handlebars.compile(this.areaTemplateTemplate.join('\n')),
			checkTemp = Handlebars.compile(this.checkTemplateTemplate.join('\n')),
			selectTemp = Handlebars.compile(this.selectTemplateTemplate.join('\n'));

		let thisItem,
			thisSplit,
            thisNumber,
			rowItems =[],
			outItems = [],
            fieldName,
            fieldType,
            fieldWidth,
			cols = col_1,
			colCount = 0,
            i;

		// make sure we're iterating an array
        if (!Array.isArray(this.include_customList)) {
            this.include_customList = [ this.include_customList ];
        }
		
		// figure out the rows and items
		for (thisItem of this.include_customList) {

            // set defaults
		    thisNumber = 1;
		    fieldType = 'input';
            // if (thisItem.indexOf('*') > 0) {
		     //    thisNumber = 2;
            // }
			if (thisItem.indexOf(':') > 0) {
				// we have attributes
				thisSplit = thisItem.split(/:/g);
				fieldName = thisSplit[0];
				for (i = 1; i < thisSplit.length; i++) {
                    if (/^\d/.test(thisSplit[i])) {
				        // it's a number
                        fieldWidth = thisSplit[i];
                        // right now, if it's not two, it's one
                        if (fieldWidth != 2) {
                            fieldWidth = 1;
                        }
                    }
				    else {
				        // it's a type
                        if (thisSplit[i].charAt(0) === 'a') {
                            fieldType = 'area';
                        }
                        else if (thisSplit[i].charAt(0) === 'c') {
                            fieldType = 'check';
                        }
                        else if (thisSplit[i].charAt(0) === 's') {
                            fieldType = 'select';
                        }
                        else {
                            fieldType = 'input';
                        }
                    }
                }
				rowItems.push(
					{
						'fieldName': fieldName,
						'type': fieldType,
						'number': fieldWidth
					}
				);
			}
			else {
				// we just have a name
				rowItems.push(
					{
						'fieldName': thisItem,
						'type': 'input',
						'number': 1
					}
				);
			}
		}

		// run through the assembled row items
		for (thisItem of rowItems) {
			// add a row wrapper or not?
			if (thisItem.number == 2 && colCount == 0) {
				// start a 2-column
				cols = col_2;
				colCount++;
				outItems.push(startRowTemp({'cols': cols}));
			}
			else if (thisItem.number == 2) {
				// complete a 2-column
				colCount = 0;
			}
			else if (thisItem.number == 1 && colCount > 0) {
				// it's a 1-column after a partial 2-column
				outItems.push(this.endRowTemplate.join('\n'));
				cols = col_1;
				colCount = 0;
				outItems.push(startRowTemp({'cols': cols}));
			}
			else {
				// it's a 1-column
				cols = col_1;
				colCount = 0;
				outItems.push(startRowTemp({'cols': cols}));
			}

			// next, insert the field by type
			if (thisItem.type == 'area') {
				outItems.push(areaTemp(thisItem));
			}
			else if (thisItem.type == 'check') {
				outItems.push(checkTemp(thisItem));
			}
			else if (thisItem.type == 'select') {
				outItems.push(selectTemp(thisItem));
			}
			else {
				outItems.push(inputTemp(thisItem));
			}

			// finally, close the row or not?
			if (colCount == 0) {
				outItems.push(this.endRowTemplate.join('\n'));
			}
		}

		// make it an arrayText object
		rowItems = [];
		for (thisItem of outItems) {
			// fix the escaped brackets
			thisItem = thisItem.replace(/\{\|/g, '{{');
			thisItem = thisItem.replace(/\|\}/g, '}}');
			rowItems = rowItems.concat(thisItem.split(/\n/g));
		}

		// return JSON.stringify(rowItems, null, 4);
		return { 'template' : rowItems };
	},

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
		"<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-link_name\" for=\"link_name-input\">link_name</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"link_name\" type=\"text\" class=\"form-control\" id=\"link_name-input\" value=\"{{link_name}}\">",
        "        </div>",
        "    </div>",
        "</div>",
		"",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Standard functions to implement:",
        "    </div>",
        "</div>",
		"",
		"<div class=\"col-grid col-1-1-2\">",
		"    <div class=\"align-right field-name short-line\">",
		"        <label id=\"label-include_fetch-checkbox-1\" for=\"include_fetch-checkbox\">fetch()</label>",
		"    </div>",
		"    <div class=\"link-field\">",
		"        <div class=\"form-group short-line\">",
		"            <label id=\"label-include_fetch-checkbox-2\" for=\"include_fetch-checkbox\">",
		"            <input type=\"checkbox\" class=\"form-control-narrow short-line\" id=\"include_fetch-checkbox\" name=\"include_fetch\"{{#isEqual include_fetch 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}> include</label>",
		"        </div>",
		"    </div>",
		"</div>",
		"",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-include_zombieFetch-checkbox-1\" for=\"include_zombieFetch-checkbox\">zombie for fetch()</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-include_zombieFetch-checkbox-2\" for=\"include_zombieFetch-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"include_zombieFetch-checkbox\" name=\"include_zombieFetch\"{{#isEqual include_zombieFetch 'true'}} value=\"true\" checked=\"checked\"{{/isEqual}}> include</label>",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-zombieFetch_name-checkbox-1\" for=\"zombieFetch_name-checkbox\">name</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"zombieFetch_name\" type=\"text\" class=\"form-control\" id=\"zombieFetch_name-input\" value=\"{{zombieFetch_name}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
		"<div class=\"col-grid col-1-1-2\">",
		"    <div class=\"align-right field-name short-line\">",
		"        <label id=\"label-include_render-checkbox-1\" for=\"include_render-checkbox\">render()</label>",
		"    </div>",
		"    <div class=\"link-field\">",
		"        <div class=\"form-group short-line\">",
		"            <label id=\"label-include_render-checkbox-1\" for=\"include_render-checkbox\">",
		"            <input type=\"checkbox\" class=\"form-control-narrow short-line\" id=\"include_render-checkbox\" name=\"include_render\"{{#isEqual include_render 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}> include</label>",
		"        </div>",
		"    </div>",
		"</div>",
		"",
		"<div class=\"col-grid col-1-1-1-1\">",
		"    <div class=\"align-right field-name\">",
		"        <label id=\"label-include_zombieRender-checkbox-1\" for=\"include_zombieRender-checkbox\">zombie for render()</label>",
		"    </div>",
		"    <div class=\"link-field\">",
		"        <div class=\"form-group\">",
		"            <label id=\"label-include_zombieRender-checkbox-2\" for=\"include_zombieRender-checkbox\">",
		"            <input type=\"checkbox\" class=\"form-control-narrow short-line\" id=\"include_zombieRender-checkbox\" name=\"include_zombieRender\"{{#isEqual include_zombieRender 'true'}} value=\"true\" checked=\"checked\"{{/isEqual}}> include</label>",
		"        </div>",
		"    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-zombieRender_name-checkbox-1\" for=\"zombieRender_name-checkbox\">name</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"zombieRender_name\" type=\"text\" class=\"form-control\" id=\"zombieRender_name-input\" value=\"{{zombieRender_name}}\">",
        "        </div>",
        "    </div>",
		"</div>",
		"",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
		"<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-include_properties\" for=\"include_properties-input\">properties</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"include_properties\" type=\"text\" class=\"form-control\" id=\"include_properties-input\" rows=\"5\">{{arrayText include_properties}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
		"",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Default type is text input field, full width, unless a code is appended, like 'name:select:2'",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- ':area', ':select', or ':check' indicates field type",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- ':1' (default) or ':2' indicates number of fields on a line",
        "    </div>",
        "</div>",
        "",
		"<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-include_customList\" for=\"include_customList-input\">customList</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"include_customList\" type=\"text\" class=\"form-control\" id=\"include_customList-input\" rows=\"5\">{{arrayText include_customList}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
		"",
		""
	],

    linkHeadTemplate: [
        "/**",
        " * <h4>Link.{{pathName}}<hr></h4>",
        " *",
        " * <p>Definition for the {{name}} link module</p>",
        " *",
        " * <p>@TODO Please unstub this link module.</p>",
        " *",
        " * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.",
        " * @author William Fleet <william.h.fleet@lowes.com>",
        " * @class Link.{{pathName}}",
        " * @version 0.1",
        " */",
        "",
        "const {{name}} = {",
        "",
        "    /**",
        "     * <h4>name<hr></h4>",
        "     * @type {string}",
        "     */",
        "    name: '{{name}}',",
        "",
        "    /**",
        "     * <h4>fullname<hr></h4>",
        "     * @type {string}",
        "     */",
        "    fullname: 'Anklet.links.{{pathName}}',",
        "",
        ""
	],
	
	propertyTemplate: [
		"{{#each propertyNames}}",
		"    /**",
		"     * <h4>{{this}}<hr></h4>",
		"     */",
		"    {{this}}: '',",
		"",
		"{{/each}}",
		""
	],

	customListTemplate: [
		"    /**",
		"     * <h4>customList<hr></h4>",
		"     */",
		"    customList: [",
		"{{#each customList}}",
		"        '{{this}}',",
		"{{/each}}",
		"        ''",
		"    ],",
		"",
		""
	],

	templateTemplate: [
		"    /**",
		"     * <h4>template<hr></h4>",
		"     * <p>The custom template, for those special fields</p>",
		"     */",
		"    template: [",
        "{{#each template}}",
        "        \"{{{this}}}\",",
        "{{/each}}",
		"    ]"
	],

	baseFunctionTemplate: [
		"    /**",
		"     * <h4>{{functionName}}()<hr></h4>",
		"     * <p></p>",
		"     * ",
		"     * @param workspace",
		"     */",
		"    {{functionName}}: function({{workspace}}) {",
		"",
		"        try {",
		"            workspace.output[this.id] = [",
		"                '**',",
		"                'This is a stub for {{link_name}}.{{functionName}}(). Please finish this stubbed function before completion.',",
		"                '**'",
		"            ].join('\\n');",
		"        }",
		"        catch (error) {",
        "            console.error(error);",
        "            throw error;",
		"        }",
		"",
		"    }"
	],

	sideFunctionTemplate: [
		"    /**",
		"     * <h4>{{sideFunctionName}}()<hr></h4>",
        "     * <p>for external (zombie) call, use:</p>",
        "     * <p>    {{zombieName}} = Anklet.LinkManager.zombifyThis('{{link_name}}', '{{sideFunctionName}}');</p>",
        "     * ",
		"     * @param workspace",
		"     */",
		"    {{sideFunctionName}}: function({{call_params}}) {",
		"",
		"        try {",
		"            return [",
		"                '**',",
		"                'This is a stub for {{link_name}}.{{sideFunctionName}}(). Please finish this stubbed function before completion.',",
		"                '**',",
		"                ''",
		"            ].join('\\n');",
		"        }",
		"        catch (error) {",
        "            console.error(error);",
        "            throw error;",
        "        }",
		"",
		"    }"
	],

	baseFunctionWithSideTemplate: [
		"    /**",
		"     * <h4>{{sideFunctionName}}()<hr></h4>",
		"     * <p>for external (zombie) call, use:</p>",
		"     * <p>    {{zombieName}} = Anklet.LinkManager.zombifyThis('{{link_name}}', '{{sideFunctionName}}');</p>",
		"     * ",
		"     * @param workspace",
		"     */",
		"    {{sideFunctionName}}: function({{call_params}}) {",
		"",
		"        try {",
		"            return [",
		"                '**',",
		"                'This is a stub for {{link_name}}.{{sideFunctionName}}(). Please finish this stubbed function before completion.',",
		"                '**',",
		"                ''",
		"            ].join('\\n');",
		"        }",
		"        catch (error) {",
        "            console.error(error);",
        "            throw error;",
        "        }",
		"",
		"    },",
		"",
		"    /**",
		"     * <h4>{{functionName}}()<hr></h4>",
		"     * <p></p>",
		"     * ",
		"     * @param workspace",
		"     */",
		"    {{functionName}}: function(workspace) {",
		"",
		"        try {",
		"            workspace.output[this.id] = this.{{sideFunctionName}}({{caller_params}});",
		"            workspace.output[this.id] += [",
		"                '**',",
		"                'This is a stub for {{link_name}}.{{functionName}}(). Please finish this stubbed function before completion.',",
		"                '**'",
		"            ].join('\\n');",
		"        }",
		"        catch (error) {",
        "            console.error(error);",
        "            throw error;",
        "        }",
		"",
		"    }"
	],

	inputTemplateTemplate: [
        "    <div class=\\\"align-right field-name\\\">",
        "        <label id=\\\"label-{{fieldName}}\\\" for=\\\"{{fieldName}}-input\\\">{{fieldName}}</label>",
        "    </div>",
        "    <div class=\\\"link-field\\\">",
        "        <div class=\\\"form-group\\\">",
        "            <input name=\\\"{{fieldName}}\\\" type=\\\"text\\\" class=\\\"form-control\\\" id=\\\"{{fieldName}}-input\\\" value=\\\"{|{{fieldName}}|}\\\">",
        "        </div>",
		"    </div>",
		""
	],
	
	/**
	 * the brackets to pass through are escaped as '{|' and '|}'
	 */
	areaTemplateTemplate: [
        "    <div class=\\\"align-right field-name\\\">",
        "        <label id=\\\"label-{{fieldName}}\\\" for=\\\"{{fieldName}}-input\\\">{{fieldName}}</label>",
        "    </div>",
        "    <div class=\\\"link-field\\\">",
        "        <div class=\\\"form-group\\\">",
        "            <textarea name=\\\"{{fieldName}}\\\" type=\\\"text\\\" class=\\\"form-control\\\" id=\\\"{{fieldName}}-input\\\" rows=\\\"5\\\">{|arrayText {{fieldName}}|}</textarea>",
        "        </div>",
		"    </div>",
		""
	],

	/**
	 * the brackets to pass through are escaped as '{|' and '|}'
	 */
	checkTemplateTemplate: [
		"    <div class=\\\"align-right field-name\\\">",
		"        <label id=\\\"label-{{fieldName}}-checkbox-1\\\" for=\\\"{{fieldName}}-checkbox\\\">{{fieldName}}</label>",
		"    </div>",
		"    <div class=\\\"link-field\\\">",
		"        <div class=\\\"form-group\\\">",
		"            <label id=\\\"label-{{fieldName}}-checkbox-2\\\" for=\\\"{{fieldName}}-checkbox\\\">",
		"            <input type=\\\"checkbox\\\" class=\\\"form-control-narrow\\\" id=\\\"{{fieldName}}-checkbox\\\" name=\\\"{{fieldName}}\\\"{|#isEqual {{fieldName}} 'true'|} value=\\\"true\\\" checked=\\\"checked\\\"{|else|} value=\\\"false\\\"{|/isEqual|}> label</label>",
		"        </div>",
		"    </div>",
		""
	],
	
	selectTemplateTemplate: [
        "    <div class=\\\"align-right field-name\\\">",
        "        <label id=\\\"label-{{fieldName}}\\\" for=\\\"{{fieldName}}-select-input\\\">{{fieldName}}</label>",
        "    </div>",
        "    <div class=\\\"\\\">",
        "        <div class=\\\"form-group select\\\">",
        "            <label id=\\\"label-{{fieldName}}-2\\\" for=\\\"{{fieldName}}-select-input\\\">",
        "            <select aria-describedby=\\\"ada-{{fieldName}}-select-describedby\\\" aria-labelledby=\\\"ada-{{fieldName}}-select-describedby\\\" name=\\\"{{fieldName}}\\\" class=\\\"form-control\\\" id=\\\"{{fieldName}}-select-input\\\">",
        "                <option value=\\\"\\\"></option>",
        "                <option value=\\\"\\\">----- Other values -----</option>",
        "            </select>",
        "            </label>",
        "        </div>",
		"    </div>",
		""
	],

	startRowTemplate: [
		"",
        "<div class=\\\"col-grid {{cols}}\\\">\n"
	],

	endRowTemplate: [
		"</div>",
		""
	]

};

module.exports.open_link = open_link;

