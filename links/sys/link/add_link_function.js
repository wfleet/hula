/**
 * <h4>Link.sys.link.add_link_function<hr></h4>
 *
 * <p>Adds an additional function to the link being written</p>
 *
 * <p>This is part of a toolset that will expediate the stubbing out of link modules for Anklet.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.sys.link.add_link_function
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let add_link_function = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'add_link_function',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.sys.link.add_link_function',

	function_name: '',
	function_code: [
		"function(workspace) {",
		"",
		"        try {",
		"            return [",
		"                '**',",
		"                'This is a stub. Please finish this stubbed function before completion.',",
		"                '**'",
		"            ].join('\\n');",
		"        }",
		"        catch (error) {",
		"           console.error(error);",
		"           throw error;",
		"        }",
		"",
		"    }",
		""
	],

	customList: [
		'function_name',
		'function_code'
	],
	
	/**
     * <h4>render()<hr></h4>
     * <p>renders the link close from templates below</p>
     * @param workspace
     */
	render: function(workspace) {
		try {
			const Handlebars = require('handlebars');
			let thisTemplate = Handlebars.compile(this.functionTemplate.join('\n'))
				thisData = {
					'functionName': this.function_name,
					'functionCode': this.function_code.join('\n')
				};
			workspace.output[this.id] = thisTemplate(thisData);
		}
		catch(error) {
			console.error(error);
			throw error;
		}
	},

	functionTemplate: [
		",",
		"",
		"    /**",
		"     * <h4>{{functionName}}()<hr></h4>",
		"     * <p></p>",
		"     */",
		"    {{functionName}}: {{{functionCode}}}",
		""
	],

	template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Enter a fully formed anonymous function, and its name.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-function_name\" for=\"function_name-input\">function_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group\">",
        "            <input name=\"function_name\" type=\"text\" class=\"form-control\" id=\"function_name-input\" value=\"{{function_name}}\">",
        "        </div>",
        "    </div>",
        "</div>",
		"<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-function_code\" for=\"content-input\">function_code</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"function_code\" type=\"text\" class=\"form-control nowrap\" id=\"function_code-input\" rows=\"10\">{{arrayText function_code}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
	]


};

module.exports.add_link_function = add_link_function;

