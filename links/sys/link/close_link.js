/**
 * <h4>Link.sys.link.close_link<hr></h4>
 *
 * <p>Closes a link description chain</p>
 *
 * <p>This is part of a toolset that will expediate the stubbing out of link modules for Anklet.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.sys.link.close_link
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let close_link = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'close_link',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.sys.link.close_link',
	
	/**
     * <h4>render()<hr></h4>
     * <p>renders the link close from templates below</p>
     * @param workspace
     */
	render: function(workspace) {
		try {
			const Handlebars = require('handlebars');
			let thisTemplate = Handlebars.compile(this.closeTemplate.join('\n'));
			workspace.output[this.id] = thisTemplate({'name': workspace.name});
		}
		catch(error) {
			console.error(error);
			throw error;
		}
	},

	closeTemplate: [
		'',
		'};',
		'',
		'module.exports.{{name}} = {{name}};'
	]


};

module.exports.close_link = close_link;

