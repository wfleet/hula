/**
 * <h4>Link.js.run_script<hr></h4>
 *
 * <p>Definition for the run_script link module</p>
 *
 * <p>Gets a response from a net api call, and stores it.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.js.run_script
 * @version 0.1
 * @type {{name: string, fullname: string, script: string, fetch: Link.js.run_script.fetch, runScript: Link.js.run_script.runScript}}
 */

let run_script = {

    /**
     * <h4>name<hr></h4>
     * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
     * @type {string}
     */
    name: 'run_script',

    /**
     * <h4>fullname<hr></h4>
     * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
     * @type {string}
     */
    fullname: 'Anklet.links.js.run_script',

    /**
     * <h4>script<hr></h4>
     * <p>holder for the script to run</p>
     * @type {Array}
     */
    script: [],

    /**
     * <h4>fetch()<hr></h4>
     * <p>gets the payload data and stores it</p>
     *
     * @param workspace
     * @return {*}
     */
    fetch: function(workspace) {
        try {
            return this.runScript(workspace);
        } catch (error) {
            console.error(error);
            throw error;
        }
        return true;
    },

    /**
     * <h4>runScript()<hr></h4>
     * <p>runs the script string held in the script element</p>
     * <p>this is heavily sandboxed for protection, so operation is thus:</p>
     * <ul>
     *      <li>build a sandbox object with the post and query data, and an outVars object to hold the result
     *      <li>pass the sandbox into the vm and run the script
     *      <li>pull the outVars result out the sandbox and merge it into the chain's workspace
     * </ul>
     *
     * @param workspace
     * @return {boolean}
     */
    runScript: function(workspace) {
        try {
        const vm = require('vm'),
            extend = require('util')._extend,
            thisScript = this.script.join('\n');
        let thisSandbox = {
                workspace: workspace,
                outVars: {}
            };

            vm.createContext(thisSandbox);
            vm.runInContext(thisScript, thisSandbox);
            workspace = extend(workspace, thisSandbox.outVars);
        }
        catch (error) {
            console.log(error);
            throw error;
        }
        return true;
    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'script'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-script\" for=\"script-input\">script</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"script\" type=\"text\" class=\"form-control\" id=\"script-input\" rows=\"10\">{{arrayText script}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]

};

module.exports.run_script = run_script;

