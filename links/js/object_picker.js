/**
 * <h4>Link.js.object_picker<hr></h4>
 *
 * <p>Definition for the object_picker link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.js.object_picker
 * @version 0.1
 */

const object_picker = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'object_picker',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.js.object_picker',

    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>indent<hr></h4>
     */
    indent: 2,

    /**
     * <h4>window_name<hr></h4>
     */
    window_name: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'source',
        'window_name',
        'html_block1',
        'html_block2',
        'indent',
        ''
    ],

    /**
     * <h4>makeButton()<hr></h4>
     * <p>makes the common button to push the key path onto the clipboard</p>
     *
     * @param label
     * @param path
     * @returns {string}
     */
    makeButton: function(label, path) {
        return '<input type="button" value="' + label + '" data-path="' + path + '" class="clipLink tree-button" title="Add \'' + path + '\' to the clipboard">';
    },

    /**
     * <h4>buildList()<hr></h4>
     * <p>builds the active list of the object passed in</p>
     *
     * @param listObject
     * @param thisOutput
     * @param indent
     * @param path
     * @returns {*}
     */
    buildList: function(listObject, thisOutput, indent, path) {

        try {
            const theseKeys = Object.keys(listObject);
            let thisKey,
                thisObject,
                thisSubIndex,
                thisSubObject,
                thisIndent;

            for (thisKey of theseKeys) {
                thisObject = listObject[thisKey];

                // thisObject will be either Array, Object or String
                if (Array.isArray(thisObject)) {
                    thisIndent = indent + this.indent;
                    if (thisObject.length === 0) {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': [ ]');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': [ ]</div>');
                    }
                    else {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': [');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': [</div>');
                        thisIndent += this.indent;
                        for (thisSubIndex in thisObject) {
                            thisSubObject = thisObject[thisSubIndex];
                            if (typeof(thisSubObject) === 'string') {
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' "' + thisSubObject + '"</div>');
                            }
                            else {
                                // thisOutput.push(indent + '        ' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' {');
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton('>', path + '.' + thisKey + '.' + thisSubIndex) + ' {</div>');
                                thisOutput = this.buildList(thisSubObject, thisOutput, thisIndent, path + '.' + thisKey + '.' + thisSubIndex);
                                // thisOutput.push(indent + '        }');
                                thisOutput.push('<div style="padding-left:' + thisIndent + 'em">}</div>');
                            }
                        }
                        thisOutput.push('<div style="padding-left:' + (thisIndent - this.indent) + 'em">]</div>');
                    }
                }
                else if (typeof(thisObject) === 'object') {
                    thisIndent = indent + this.indent;
                    if (thisObject === null) {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': null');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': null</div>');
                    }
                    else {
                        // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': {');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': {</div>');
                        thisOutput = this.buildList(thisObject, thisOutput, thisIndent, path + '.' + thisKey);
                        // thisOutput.push(indent + '    }');
                        thisOutput.push('<div style="padding-left:' + thisIndent + 'em">}</div>');
                    }
                }
                else if (typeof(thisObject) === 'string') {
                    thisIndent = indent + this.indent;
                    // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': "' + thisObject + '"');
                    thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': "' + thisObject + '"</div>');
                }
                else {
                    thisIndent = indent + this.indent;
                    // thisOutput.push(indent + '    ' + this.makeButton(thisKey, path + '.' + thisKey) + ': ' + thisObject);
                    thisOutput.push('<div style="padding-left:' + thisIndent + 'em">' + this.makeButton(thisKey, path + '.' + thisKey) + ': ' + thisObject + '</div>');
                }
            }
        }
        catch(error) {
            console.log(JSON.stringify(thisOutput, null, 4));
            console.error(error);
        }

        return thisOutput;
    },

    /**
     * <h4>objectOutput()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zObjectOutput = Anklet.LinkManager.zombifyThis('js.object_picker', 'objectOutput');</p>
     *
     * @param workspace
     */
    objectOutput: function(source, workspace) {

        try {
            let thisContext = workspace[source],
                thisPath = '@' + source,
                thisOutput = [
                    '<div class="grid-container">',
                    '<div>{\n' + this.buildList(thisContext, [], 0, thisPath).join('\n') + '\n}</div>',
                    '</div>'
                ];

            if (!thisOutput) {
                thisOutput = {};
            }

            return this.html_block1.join('\n') + this.source + this.html_block2.join('\n') + thisOutput.join('\n') + this.html_block3.join('\n');
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            workspace.outputStream = this.objectOutput(this.source, workspace);
            // workspace.outputHeaders = [
            //     'Content-Type: application/pdf',
            //     'Content-Disposition: inline; filename="' + workspace.outputName + '"'
            // ];

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        ""
    ],

    /**
     * <h4>thml_block<hr></h4>
     * <p>holder for the object viewer page</p>
     *
     */
    html_block1: [
        "<html>",
        "<head>",
        "    <title>Object Tree Browser</title>",
        "    <script src=\"/anklet/bracelet/assets/lib/jquery.js\"></script>",
        "    <script src=\"/anklet/bracelet/assets/lib/jquery-ui.js\"></script>",
        "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/anklet/bracelet/assets/css/scaffold-large.css\">",
        "    <link rel=\"stylesheet\" href=\"/anklet/bracelet/assets/css/jquery-ui.css\">",
        "    <link href=\"/anklet/bracelet/assets/css/bracelet.css\" rel=\"stylesheet\" type=\"text/css\" />",
        "",
        "</head>",
        "<body class=\"\">",
        "",
        "<div>&nbsp;</div>",
        "<div class=\"col-grid after-10\">",
        "    <div class=\"position-centered chain-info\">",
        "        <div class=\"graybox\">",
        "            <div id=\"chain_info\">Tree for object in workspace."
    ],

    html_block2: [
        "</div>",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<div id=\"info_box\" class=\"graybox hide\" style=\"position: fixed; top: 100px; left: 10%25; width: 80%25; border: 2px solid #FF0000;\">",
        "    <div id=\"info_text\">&nbsp;</div>",
        "</div>",
        ""
    ],

    html_block3: [
        "",
        "<script type=\"text/javascript\">",
        "(function($) {",
        "    // set the panel components",
        "    $('.clipLink').on('click', function(event) {",
        "        event.preventDefault();",
        "        const $thisItem = $(event.target),",
        "             thisValue = $thisItem.data('path');",
        "        let $thisInput = $('<input>');",
        "        $(\"body\").append($thisInput);",
        "        $thisInput.val(thisValue).select();",
        "        document.execCommand(\"copy\");",
        "        $thisInput.remove();",
        "",
        "        $('#info_text').html('The path \"' + thisValue + '\" has been copied to the clipboard.');",
        "        $('#info_box').fadeIn(200, function() {$('#info_box').fadeOut(2000)});",
        "    });",
        "",
        "})(jQuery)",
        "</script>",
        "<script src=\"https://code.jquery.com/jquery.js\"></script>",
        "",
        "</body>",
        "</html>",
        ""
    ]
};

module.exports.object_picker = object_picker;