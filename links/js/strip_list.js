/**
 * <h4>Link.js.strip_list<hr></h4>
 *
 * <p>Definition for the strip_list link module</p>
 *
 * <p>This link will remove key/value lines that match certain criteria, or replace values within them.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.js.strip_list
 * @version 0.1
 */

let strip_list = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'strip_list',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.js.strip_list',

    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     */
    target: '',

    /**
     * <h4>killEmpties<hr></h4>
     */
    killEmpties: '',

    /**
     * <h4>limit<hr></h4>
     */
    limit: '50',

    /**t
     * <h4>deletes<hr></h4>
     */
    deletes: [''],

    /**
     * <h4>replaces<hr></h4>
     */
    replaces: [''],

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'source',
        'target',
        'killEmpties',
        'limit',
        'deletes',
        'replaces',
        ''
    ],

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let thisData = Anklet.LinkManager.resolveValue(this.source, workspace),
                nextData = [],
                theseDeletes = this.deletes,
                thisDelete,
                thisItem,
                notFoundFlag;

            // make sure they're arrays
            if (!Array.isArray(this.deletes)) {
                this.deletes = [this.deletes];
            }
            if (!Array.isArray(this.replaces)) {
                this.replaces = [this.replaces];
            }

            // go after the empties and N/As
            if (this.killEmpties || this.killEmpties == 'true') {
                for (thisItem of thisData) {
                    // assumed: that all items are objects with 'Key' and 'Value' attributes
                    if (thisItem.Value && thisItem.Value !== 'N/A' && thisItem.Value !== '') {
                        nextData.push(thisItem);
                    }
                }
                // save to start over
                thisData = nextData;
                nextData = [];
            }
            // go after the deletes
            for (thisItem of thisData) {
                notFoundFlag = true;
                // we're delete-matching on the key
                for (thisDelete of theseDeletes) {
                    if (thisDelete !== '' && thisItem.Key.search(thisDelete) >= 0) {
                        notFoundFlag = false;
                    }
                }
                // did we find it or not?
                if (notFoundFlag) {
                    nextData.push(thisItem);
                }
            }
            // save to start over
            thisData = nextData;

            if (thisData.length > this.limit) {
                thisData.length = this.limit;
            }
            // put back at the target
            workspace[this.target] = thisData;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-killEmpties-checkbox-1\" for=\"killEmpties-checkbox\">killEmpties</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-killEmpties-checkbox-2\" for=\"killEmpties-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"killEmpties-checkbox\" name=\"killEmpties\"{{#isEqual killEmpties 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-limit\" for=\"limit-input\">limit</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"limit\" type=\"text\" class=\"form-control\" id=\"limit-input\" value=\"{{limit}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-deletes\" for=\"deletes-input\">deletes</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"deletes\" type=\"text\" class=\"form-control\" id=\"deletes-input\" rows=\"5\">{{arrayText deletes}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-replaces\" for=\"replaces-input\">replaces</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"replaces\" type=\"text\" class=\"form-control\" id=\"replaces-input\" rows=\"5\">{{arrayText replaces}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.strip_list = strip_list;