/**
 * <h4>Link.js.copy_tree<hr></h4>
 *
 * <p>Definition for the copy_tree link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.js.copy_tree
 * @version 0.1
 */

const copy_tree = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'copy_tree',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.js.copy_tree',

    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     */
    target: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'source',
        'target',
        ''
    ],

    /**
     * <h4>copyTree()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zCopyTree = Anklet.LinkManager.zombifyThis('js.copy_tree', 'copyTree');</p>
     *
     * @param workspace
     * @param source
     * @return {*}
     */
    copyTree: function (workspace, source) {

        try {
            let nodeList = source.replace(/@/, ''),
                thisTree = Object.assign({}, workspace),
                theseNodes = nodeList.split(/\./g),
                thisNode;
            for (thisNode of theseNodes) {
                thisTree = thisTree[thisNode];
            }

            return thisTree;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>pasteTree()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zPasteTree = Anklet.LinkManager.zombifyThis('js.copy_tree', 'pasteTree');</p>
     *
     * @param workspace
     * @param value
     * @param target
     * @return {*}
     */
    pasteTree: function (workspace, value, target) {

        try {
            let nodeList = target.replace(/@/, ''),
                thisTree = workspace,
                // thisTree = Object.assign({}, workspace),
                theseNodes = nodeList.split(/\./g),
                lastNode = theseNodes.pop(),
                thisNode;

            for (thisNode of theseNodes) {
                if (!thisTree[thisNode]) {
                    thisTree[thisNode] = {};
                }
                thisTree = thisTree[thisNode];
            }
            thisTree[lastNode] = value;

            return workspace;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    fetch: function (workspace) {

        try {
            let thisTree;

            thisTree = this.copyTree(workspace, this.source);
            workspace = this.pasteTree(workspace, thisTree, this.target);

       }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        ""
    ]
};

module.exports.copy_tree = copy_tree;