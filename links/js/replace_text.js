/**
 * <h4>Link.js.replace_text<hr></h4>
 *
 * <p>Definition for the replace_text link module</p>
 *
  * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.js.replace_text
 * @version 0.1
 */

let replace_text = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'replace_text',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.js.replace_text',

    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     */
    target: '',

    /**
     * <h4>replaces<hr></h4>
     */
    replaces: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'source',
        'target',
        'replaces',
        ''
    ],

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {

            let thisData = Anklet.LinkManager.resolveValue(this.source, workspace),
                theseReplaces = this.replaces,
                thisReplace,
                thisSplit,
                thisRegEx;

            if (thisData === undefined || thisData === null) {
                workspace[this.target] = '';
                return;
            }

            if (!Array.isArray(this.replaces)) {
                theseReplaces = [this.replaces];
            }
            for (thisReplace of theseReplaces) {
                thisSplit = thisReplace.split(/::/);
                thisRegEx = thisSplit[0].split(/\//g);
                if (thisRegEx.length == 3) {
                    thisRegEx = new RegExp(thisRegEx[1], thisRegEx[2]);
                }
                else {
                    thisRegEx = new RegExp(thisRegEx);
                }
                thisData = thisData.replace(thisRegEx, thisSplit[1]);
            }
            workspace[this.target] = thisData;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-replaces\" for=\"replaces-input\">replaces</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"replaces\" type=\"text\" class=\"form-control\" id=\"replaces-input\" rows=\"5\">{{arrayText replaces}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.replace_text = replace_text;