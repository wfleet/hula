/**
 * <h4>Link.core<hr></h4>
 *
 * <p>Definition for the core link module</p>
 *
 * <p>This is the initial link used as the basis for all subsequent links.
 * Once this link is loaded, it is extended for the new Link and
 * the new Link's objects and functions are overlaid.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.Core
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let core = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'core',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.core'

};

module.exports.core = core;

