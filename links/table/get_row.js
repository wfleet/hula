/**
 * <h4>Link.table.get_row<hr></h4>
 *
 * <p>Definition for the get_row link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.get_row
 * @version 0.1
 */

const get_row = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'get_row',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.get_row',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>numberOfCells<hr></h4>
     * @type {string}
     */
    numberOfCells: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'numberOfCells',
        'startColumn',
        ''
    ],

    /**
     * <h4>getRow()<hr></h4>
     * <p>for zombie call, use: </p>
     * <p>    zGetRow = Anklet.LinkManager.zombifyThis('table.get_row', 'getRow');</p>
     *
     * @param startRow
     * @param startCol
     * @param thisTable
     * @return {Array}
     */
    getRow: function(startRow, startCol, numberOfCells, thisTable) {

        try {
            let workTable = thisTable,
                thisRow,
                endCol,
                thisNewRow = [],
                i;

            // integerize
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            numberOfCells = parseInt(numberOfCells);

            if (startRow >= workTable.length) {
                // if we're out of range, return empty
                return [];
            }

            thisRow = thisTable[startRow];
            if (startCol >= thisRow.length) {
                // again, if we're out of range, return empty
                return [];
            }

            if (startCol + numberOfCells >= thisRow.length) {
                endCol = thisRow.length;
            }
            else {
                endCol = startCol + numberOfCells;
            }

            // slice the row
            for (i = startCol; i < endCol; i++) {
                thisNewRow.push(Object.assign({}, thisRow[i]));
            }
            // thisNewRow = thisRow.slice(startCol, endCol);

            return thisNewRow;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let thisRow = this.getRow(this.startRow, this.startColumn, this.numberOfCells, workspace[this.source].table);

            workspace[this.target] = thisRow;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfCells\" for=\"numberOfCells-input\">numberOfCells</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfCells\" type=\"text\" class=\"form-control\" id=\"numberOfCells-input\" value=\"{{numberOfCells}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        ""
    ]


};

module.exports.get_row = get_row;