/**
 * <h4>Link.table.add_table<hr></h4>
 *
 * <p>Definition for the add_table link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.add_table
 * @version 0.1
 * @type {{name: string, fullname: string, source: string, target: string, startRow: string, numberOfRows: string, startColumn: string, numberOfColumns: string, customList: string[], addTable: Link.table.add_table.addTable, fetch: Link.table.add_table.fetch, template: string[], figureOutSource: Link.table.add_table.figureOutSource, tableFromObject: Link.table.add_table.tableFromObject, tableFromJSON: Link.table.add_table.tableFromJSON, tableFromText: Link.table.add_table.tableFromText}}
 */

const add_table = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'add_table',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.add_table',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>numberOfRows<hr></h4>
     * @type {string}
     */
    numberOfRows: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>numberOfColumns<hr></h4>
     * @type {string}
     */
    numberOfColumns: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'numberOfRows',
        'startColumn',
        'numberOfColumns',
        ''
    ],

    /**
     * <h4>addTable()<hr></h4>
     * <p>overlays the table info in the source field onto the target table</p>
     * <p>for zombie call, use: </p>
     * <p>    zAddTable = Anklet.LinkManager.zombifyThis('table.add_table', 'addTable');</p>
     *
     * @param source
     * @param targetTable
     * @param startRow
     * @param startCol
     * @param numRows
     * @param numCols
     * @return {*}
     */
    addTable: function(source, targetTable, startRow, startCol, numRows, numCols) {

        try {
            let thisNewTable,
                overlayRow,
                rowCount,
                i;

            const sourceType = this.figureOutSource(source),
                zAddRow = Anklet.LinkManager.zombifyThis('table.add_row', 'addRow'),
                zMakeTable = Anklet.LinkManager.zombifyThis('table.create_table', 'makeTable');

            // integerize inputs
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            numRows = parseInt(numRows);
            numCols = parseInt(numCols);

            if (sourceType === 'object') {
                thisNewTable = this.tableFromObject(source);
            }
            else if (sourceType === 'json') {
                thisNewTable = this.tableFromJSON(source);
            }
            else if (sourceType === 'tabbed') {
                thisNewTable = this.tableFromText(source, numRows, numCols, 'tab');
            }
            else {
                // default: handle as csv
                thisNewTable = this.tableFromText(source, numRows, numCols, 'comma');
            }

            // if there is no target table, create one
            if (!targetTable) {
                targetTable = zMakeTable(startCol + numCols, startRow + numRows);
            }

            // once we have the new table, overlay it onto the target table
            rowCount = thisNewTable.table.length;
            for (i = 0; i < rowCount; i++) {
                overlayRow = thisNewTable.table[i];
                targetTable = zAddRow(startRow + i, startCol, overlayRow, targetTable);
            }

            // return the finalized table
            return targetTable;

        }
        catch (error) {
            console.error(error);
            throw error;
        }
    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let tableSource;

            // resolve source if it is a workspace reference
            if (typeof this.source === 'object') {
                tableSource = this.source;
            }
            else {
                tableSource = Anklet.LinkManager.resolveValue(this.source, workspace);
            }

            const thisTable = this.addTable(tableSource, workspace[this.target], this.startRow, this.startColumn, this.numberOfRows, this.numberOfColumns);
            workspace[this.target] = thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     * @type {string[]}
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" rows=\"4\">{{arrayText source}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfRows\" for=\"numberOfRows-input\">numberOfRows</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfRows\" type=\"text\" class=\"form-control\" id=\"numberOfRows-input\" value=\"{{numberOfRows}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfColumns\" for=\"numberOfColumns-input\">numberOfColumns</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfColumns\" type=\"text\" class=\"form-control\" id=\"numberOfColumns-input\" value=\"{{numberOfColumns}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ],

    /**
     * <h4>figureOutSource()<hr></h4>
     * <p>looks at this.source and figures out which format it is</p>
     * <p>returns csv, tabbed, JSON or object</p>
     *
     * @param theSource
     * @returns {string}
     */
    figureOutSource: function(theSource) {

        let thisResult,
            theseRows,
            thisRow,
            hasTabs = false;

        try {
            // see if it's already an object
            if (typeof theSource === 'object') {
                // is it an array?
                if (Array.isArray(theSource)) {
                    for (thisRow of theSource) {
                        if (typeof thisRow === 'object') {
                            return 'object';
                        }
                        if (typeof thisRow === 'string' && thisRow.match(/\t/)) {
                            hasTabs = true;
                        }
                    }
                    if (hasTabs) {
                        return 'tabbed';
                    }
                    else {
                        return 'csv';
                    }
                }
                return 'object';
            }
            // see if it's JSON format
            thisResult = JSON.parse(theSource);
            if (typeof thisResult === 'object') {
                return 'json';
            }
            else {
                // it's a text format - throw and handle in the error catcher
                throw new Error('not json');
            }
        }
        catch (error) {
            // it's a text format
            theseRows = theSource.split();
            for (thisRow of theseRows) {
                if (thisRow.match(/\t/)) {
                    hasTabs = true;
                }
            }
            if (hasTabs) {
                return 'tabbed';
            }
            else {
                return 'csv';
            }
        }
        // just in case we need it
        return 'csv';
    },


    /**
     * <h4>tableFromObject()<hr></h4>
     * <p>makes a table, returns it</p>
     *
     * @returns {*}
     */
    tableFromObject: function(source) {

        try {
            const zMakeTable = Anklet.LinkManager.zombifyThis('table.create_table', 'makeTable');

            let newObject = Object.assign({}, source),
                newTable,
                newRow,
                thisRow,
                theseKeys,
                thisKey,
                i;

            // assuming we have an array of objects
            if (Array.isArray(source)) {
                newTable = zMakeTable(0, 0);
                for (i = 0; i < source.length; i++) {
                    thisRow = source[i];
                    theseKeys = Object.getOwnPropertyNames(thisRow);
                    newRow = [];
                    for (thisKey of theseKeys) {
                        newRow.push({'value': thisRow[thisKey]});
                    }
                    newTable.table.push(newRow);
                }
            }

            return newTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },


    /**
     * <h4>tableFromJSON()<hr></h4>
     * <p>makes a table, returns it</p>
     * <p>assumed: the resolved JSON is an array of arrays, or at least an array</p>
     *
     * @param source
     * @returns {any}
     */
    tableFromJSON: function(source) {

        try {
            const thisNewTable = JSON.parse(source);

            return thisNewTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>tableFromText()<hr></h4>
     * <p>builds a table from tabbed or csv text format</p>
     *
     * @param source
     * @param numOfRows
     * @param numOfColumns
     * @param separator
     * @returns {*}
     */
    tableFromText: function(source, numOfRows, numOfColumns, separator) {

        try {
            const zMakeTable = Anklet.LinkManager.zombifyThis('table.create_table', 'makeTable');
            let theseRows,
                thisNewTable,
                thisRow,
                thatRow,
                thisNumOfColumns = numOfColumns,
                theseCells,
                thisCell,
                i, j;

            // is source already an array?
            if (Array.isArray(source)) {
                theseRows = source;
            }
            else {
                theseRows = source.split(/\n/g);
            }

            // make a workspace table
            thisNewTable = zMakeTable(numOfColumns, numOfRows);
            // if there are fewer source rows, shorten the operation
            if (theseRows.length < numOfRows) {
                numOfRows = theseRows.length;
            }

            // walk through the rows
            for (i = 0; i < numOfRows; i++) {
                // get the table row
                thisRow = thisNewTable.table[i];
                thatRow = theseRows[i];
                if (separator === 'comma') {
                    // @TODO: come up with better split algorithm for commas
                    theseCells = thatRow.split(/,/g);
                }
                else {
                    // it's tabbed
                    theseCells = thatRow.split(/\t/g);
                }
                if (theseCells.length < thisNumOfColumns) {
                    thisNumOfColumns = theseCells.length;
                }
                for (j = 0; j < numOfColumns; j++) {
                    thisCell = thisRow[j];
                    thisCell.value = theseCells[j];
                }
                // reset thisNumOfColumns
                thisNumOfColumns = numOfColumns;
            }

            return thisNewTable;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    }
};

module.exports.add_table = add_table;