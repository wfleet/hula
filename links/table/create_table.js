/**
 * <h4>Link.table.create_table<hr></h4>
 *
 * <p>Definition for the create_table link module</p>
 *
 * <p>Makes an otherwise empty table.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.create_table
 * @version 0.1
 * @type {{name: string, fullname: string, target: string, initWidth: string, initHeight: string, customList: string[], makeTable: (function(*=, *=): {table: Array}), fetch: Link.table.create_table.fetch, template: string[]}}
 */

const create_table = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'create_table',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.create_table',

    /**
     * <h4>location<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>initWidth<hr></h4>
     * @type {string}
     */
    initWidth: '4',

    /**
     * <h4>initHeight<hr></h4>
     * @type {string}
     */
    initHeight: '4',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'target',
        'initWidth',
        'initHeight',
        ''
    ],

    /**
     * <h4>makeTable()<hr></h4>
     * <p>Makes an empty table.</p>
     * <p>for zombie call, use: </p>
     * <p>    zMakeTable = Anklet.LinkManager.zombifyThis('table.create_table', 'makeTable');</p>
     *
     * @param cols
     * @param rows
     * @returns {{table: Array}}
     */
    makeTable: function (cols, rows) {

        const thisCell = {
            "value": ""
        };
        let thisTable = {
                "table": []
            },
            thisRow,
            rowIndex, colIndex;

        // fill out the rows and stack them
        for (rowIndex = 0; rowIndex < parseInt(rows); rowIndex++) {
            thisRow = [];
            for (colIndex = 0; colIndex < parseInt(cols); colIndex++) {
                thisRow.push(Object.assign({}, thisCell));
            }
            thisTable.table.push(thisRow);
        }
        return thisTable;

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const newTable = this.makeTable(this.initWidth, this.initHeight);

            // stick it in the target in workspace
            workspace[this.target] = newTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-initWidth\" for=\"initWidth-input\">initWidth</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"initWidth\" type=\"text\" class=\"form-control\" id=\"initWidth-input\" value=\"{{initWidth}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-initHeight\" for=\"initHeight-input\">initHeight</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"initHeight\" type=\"text\" class=\"form-control\" id=\"initHeight-input\" value=\"{{initHeight}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]

};

module.exports.create_table = create_table;