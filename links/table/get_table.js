/**
 * <h4>Link.table.get_table<hr></h4>
 *
 * <p>Definition for the get_table link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.get_table
 * @version 0.1
 * @type {{name: string, fullname: string, source: string, target: string, startRow: string, numberOfRows: string, startColumn: string, numberOfColumns: string, customList: string[], getTable: Link.table.get_table.sideFetch, fetch: Link.table.get_table.fetch, template: string[], getTable: Link.table.get_table.getTable, getRow: Link.table.get_table.getRow, getColumn: Link.table.get_table.getColumn}}
 */

const get_table = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'get_table',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.get_table',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>numberOfRows<hr></h4>
     * @type {string}
     */
    numberOfRows: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>numberOfColumns<hr></h4>
     * @type {string}
     */
    numberOfColumns: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'numberOfRows',
        'startColumn',
        'numberOfColumns',
        ''
    ],

    /**
     * <h4>getTable()<hr></h4>
     * <p>gets a sub-table from a table object</p>
     * <p>for zombie call, use: </p>
     * <p>    zGetTable = Anklet.LinkManager.zombifyThis('table.get_table', 'getTable');</p>
     *
     * @param startRow
     * @param startCol
     * @param numRows
     * @param numCols
     * @param source
     * @return {Array}
     */
    getTable: function(startRow, startCol, numRows, numCols, source) {

        try {
            const zGetRow = Anklet.LinkManager.zombifyThis('table.get_row', 'getRow');
            let workTable,
                newTable = [],
                thisRow,
                i;

            // check inputs
            if (source.hasOwnProperty('table')) {
                workTable = source.table;
            }
            else {
                workTable = source;
            }

            // integerize
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            numRows = parseInt(numRows);
            numCols = parseInt(numCols);

            // just walk through the rows, it will safety-check on the fly
            for (i = startRow; i < startRow + numRows; i++) {
                thisRow = zGetRow(i, startCol, numCols, workTable);
                newTable.push(thisRow);
            }

            return newTable;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            workspace[this.target] = this.getTable(this.startRow, this.startColumn, this.numberOfRows, this.numberOfColumns, workspace[this.source]);
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfRows\" for=\"numberOfRows-input\">numberOfRows</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfRows\" type=\"text\" class=\"form-control\" id=\"numberOfRows-input\" value=\"{{numberOfRows}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfColumns\" for=\"numberOfColumns-input\">numberOfColumns</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfColumns\" type=\"text\" class=\"form-control\" id=\"numberOfColumns-input\" value=\"{{numberOfColumns}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]

};

module.exports.get_table = get_table;