/**
 * <h4>Link.table.utils<hr></h4>
 *
 * <p>Definition for the utils link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.utils
 * @version 0.1
 */

const utils = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'utils',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.utils',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>numberOfRows<hr></h4>
     * @type {string}
     */
    numberOfRows: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>numberOfColumns<hr></h4>
     * @type {string}
     */
    numberOfColumns: '',

    /**
     * <h4>flipTable<hr></h4>
     * @type {string}
     */
    flip: '',

    /**
     * <h4>axis<hr></h4>
     * @type {string}
     */
    axis: '',

    /**
     * <h4>pivotTable<hr></h4>
     * @type {string}
     */
    pivot: '',

    /**
     * <h4>direction<hr></h4>
     * @type {string}
     */
    direction: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'numberOfRows',
        'startColumn',
        'numberOfColumns',
        'flip',
        'axis',
        'pivot',
        'direction',
        ''
    ],

    /**
     * <h4>fill()<hr></h4>
     * <p>fills a section of a table with empty cells, if needed</p>
     * <p>for zombie call, use: </p>
     * <p>    zFill = Anklet.LinkManager.zombifyThis('table.utils', 'fill');</p>
     *
     * @param startRow
     * @param startCol
     * @param numRows
     * @param numCols
     * @param source
     */
    fill: function(startRow, startCol, numRows, numCols, source) {

        try {

            const zAddRow = Anklet.LinkManager.zombifyThis('table.add_row', 'addRow');
            let workTable = source.table,
                thisRow,
                i;

            // ensure these are integers
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            numRows = parseInt(numRows);
            numCols = parseInt(numCols);

            for (i = startRow; i < startRow + numRows; i++) {
                if (workTable.length <= i) {
                    // we're past the end of the table, fake a row
                    thisRow = [];
                }
                else {
                    // get the row
                    thisRow = workTable[i];
                }
                while (thisRow.length <= startCol + numCols) {
                    // fill out the row
                    thisRow.push({"value": ""});
                }
                workTable = zAddRow(i, 0, thisRow, workTable);
            }
            source.table = workTable;
            return source;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    },

    /**
     * <h4>flipTable()<hr></h4>
     * <p>flips a section of a table horizontally, or vertically, or both</p>
     * <p>for zombie call, use: </p>
     * <p>    zFlip = Anklet.LinkManager.zombifyThis('table.utils', 'flipTable');</p>
     *
     * @param startRow
     * @param startCol
     * @param numRows
     * @param numCols
     * @param axis
     * @param source
     * @return {*}
     */
    flipTable: function(startRow, startCol, numRows, numCols, axis, source) {

        try {
            const zGetTable = Anklet.LinkManager.zombifyThis('table.get_table', 'getTable');

            let workTable = zGetTable(startRow, startCol, numRows, numCols, source.table),
                thisRow,
                i;

            if (axis === 'horizontal' || axis === 'both') {
                // we're doing a horizontal flipTable of the subtable
                for (i = 0; i < workTable.length; i++) {
                    thisRow = workTable[i];
                    workTable[i] = thisRow.reverse();
                }
            }
            if (axis === 'vertical' || axis === 'both') {
                // we're flipping it vertically, just reverse the rows
                workTable = workTable.reverse();
            }

            source.table = workTable;

            return source;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <p>rotates a section of a table to the east, south, or west</p>
     * <p>for zombie call, use: </p>
     * <p>    zPivot = Anklet.LinkManager.zombifyThis('table.utils', 'pivotTable');</p>
     *
     * @param startRow
     * @param startCol
     * @param numRows
     * @param numCols
     * @param direction
     * @param source
     * @return {*}
     */
    pivotTable: function(startRow, startCol, numRows, numCols, direction, source) {

        try {
            const zGetTable = Anklet.LinkManager.zombifyThis('table.get_table', 'getTable'),
                zAddRow = Anklet.LinkManager.zombifyThis('table.add_row', 'addRow'),
                zAddColumn = Anklet.LinkManager.zombifyThis('table.add_column', 'addColumn');

            let workTable = zGetTable(startRow, startCol, numRows, numCols, source.table),
                newTable = [],
                thisRow,
                i, j;

            if (direction == 'east') {
                // clockwise 90 degrees
                j = 0;
                for (i = workTable.length -1; i >= 0; i--) {
                    thisRow = workTable[i];
                    newTable = zAddColumn(0, j, thisRow, newTable);
                    j++;
                }
            }
            else if (direction === 'west') {
                // counter-clockwise 90 degrees
                for (i = 0; i < workTable.length; i++) {
                    thisRow = workTable[i];
                    thisRow = thisRow.reverse();
                    newTable = zAddColumn(0, i, thisRow, newTable);
                }
            }
            else if (direction === 'south') {
                // 180 degrees rotate
                j = 0;
                for (i = workTable.length -1; i >= 0; i--) {
                    thisRow = workTable[i];
                    thisRow = thisRow.reverse();
                    newTable = zAddRow(j, 0, thisRow, newTable);
                    j++;
                }
            }
            source.table = newTable;
            return source;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let workTable = this.fill(this.startRow, this.startColumn, this.numberOfRows, this.numberOfColumns, Object.assign({}, workspace[this.source]));

            if (this.flip === 'true') {
                workTable = this.flipTable(this.startRow, this.startColumn, this.numberOfRows, this.numberOfColumns, this.axis, workTable);
            }
            if (this.pivot === 'true') {
                workTable = this.pivotTable(this.startRow, this.startColumn, this.numberOfRows, this.numberOfColumns, this.direction, workTable);
            }

            workspace[this.target] = workTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfRows\" for=\"numberOfRows-input\">numberOfRows</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfRows\" type=\"text\" class=\"form-control\" id=\"numberOfRows-input\" value=\"{{numberOfRows}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfColumns\" for=\"numberOfColumns-input\">numberOfColumns</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfColumns\" type=\"text\" class=\"form-control\" id=\"numberOfColumns-input\" value=\"{{numberOfColumns}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-flip-checkbox-1\" for=\"flip-checkbox\">flip</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-flip-checkbox-2\" for=\"flip-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"flip-checkbox\" name=\"flip\"{{#isEqual flip 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-axis\" for=\"axis-select-input\">axis</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-axis-2\" for=\"axis-select-input\">",
        "            <select aria-describedby=\"ada-axis-select-describedby\" aria-labelledby=\"ada-axis-select-describedby\" name=\"axis\" class=\"form-control\" id=\"axis-select-input\">",
        "                <option value=\"horizontal\">horizontal</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"horizontal\">horizontal</option>",
        "                <option value=\"vertical\">vertical</option>",
        "                <option value=\"both\">both</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-pivot-checkbox-1\" for=\"pivot-checkbox\">pivot</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-pivot-checkbox-2\" for=\"pivot-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"pivot-checkbox\" name=\"pivot\"{{#isEqual pivot 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-direction\" for=\"direction-select-input\">direction</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-direction-2\" for=\"direction-select-input\">",
        "            <select aria-describedby=\"ada-direction-select-describedby\" aria-labelledby=\"ada-direction-select-describedby\" name=\"direction\" class=\"form-control\" id=\"direction-select-input\">",
        "                <option value=\"east\">east</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"east\">east</option>",
        "                <option value=\"south\">south</option>",
        "                <option value=\"west\">west</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]

};

module.exports.utils = utils;