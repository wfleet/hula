/**
 * <h4>Link.table.set_attribute<hr></h4>
 *
 * <p>Definition for the set_attribute link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.set_attribute
 * @version 0.1
 */

const set_attribute = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'set_attribute',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.set_attribute',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: 'workTable',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: 'workTable',

    /**
     * <h4>font<hr></h4>
     * @type {string}
     */
    font: '',

    /**
     * <h4>size<hr></h4>
     * @type {string}
     */
    size: '',

    /**
     * <h4>color<hr></h4>
     * @type {string}
     */
    color: '',

    /**
     * <h4>align<hr></h4>
     * @type {string}
     */
    align: '',

    /**
     * <h4>width<hr></h4>
     * @type {string}
     */
    width: '',

    /**
     * <h4>border<hr></h4>
     * @type {string}
     */
    border: '',

    /**
     * <h4>border_color<hr></h4>
     * @type {string}
     */
    border_color: '#000000',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>numberOfCells<hr></h4>
     * @type {string}
     */
    numberOfCells: '',

    /**
     * <h4>rowOrColumn<hr></h4>
     * @type {string}
     */
    rowOrColumn: '',

    /**
     * <h4>additionalFontList<hr></h4>
     * @type {string}
     */
    additionalFontList: Anklet.fontList,

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'font',
        'size',
        'color',
        'align',
        'width',
        'border',
        'border_color',
        'startRow',
        'startColumn',
        'numberOfCells',
        'rowOrColumn',
        'additionalFontList',
        ''
    ],

    /**
     * <h4>setAttribute()<hr></h4>
     * <p></p>
     */
    setAttribute: function(startRow, startCol, numCells, rowOrCol, font, size, color, align, width, border, borderColor, source) {

        try {
            const zGetRow = Anklet.LinkManager.zombifyThis('table.get_row', 'getRow'),
                zGetColumn = Anklet.LinkManager.zombifyThis('table.get_column', 'getColumn'),
                zAddRow = Anklet.LinkManager.zombifyThis('table.add_row', 'addRow'),
                zAddColumn = Anklet.LinkManager.zombifyThis('table.add_column', 'addColumn');
            let workTable = source,
                theseCells,
                i;

            if (rowOrCol === 'row') {
                theseCells = zGetRow(startRow, startCol, numCells, workTable.table);
                for (i = 0; i < theseCells.length; i++) {
                    if (font) {
                        theseCells[i]['font'] = font;
                    }
                    if (size) {
                        theseCells[i]['size'] = size;
                    }
                    if (color) {
                        theseCells[i]['color'] = color;
                    }
                    if (align) {
                        theseCells[i]['align'] = align;
                    }
                    if (width) {
                        theseCells[i]['width'] = width;
                    }
                    if (border) {
                        theseCells[i]['border'] = JSON.parse(border);
                    }
                    if (borderColor) {
                        theseCells[i]['border_color'] = borderColor;
                    }
                }
                workTable = zAddRow(startRow, startCol, theseCells, workTable);
            }
            else {
                theseCells = zGetColumn(startRow, startCol, numCells, workTable.table);
                for (i = 0; i < theseCells.length; i++) {
                    if (font) {
                        theseCells[i]['font'] = font;
                    }
                    if (size) {
                        theseCells[i]['size'] = size;
                    }
                    if (color) {
                        theseCells[i]['color'] = color;
                    }
                    if (align) {
                        theseCells[i]['align'] = align;
                    }
                    if (width) {
                        theseCells[i]['width'] = width;
                    }
                    if (border) {
                        theseCells[i]['border'] = JSON.parse(border);
                    }
                    if (borderColor) {
                        theseCells[i]['border_color'] = borderColor;
                    }
                }
                workTable = zAddColumn(startRow, startCol, theseCells, workTable);
            }

            return workTable;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            workspace[this.target] = this.setAttribute(this.startRow, this.startColumn, this.numberOfCells, this.rowOrColumn, this.font, this.size, this.color, this.align, this.width, this.border, this.border_color, workspace[this.source]);

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />Font and appearance",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font\" for=\"font-input\">font</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font-2\" for=\"font-select-input\">",
        "            <select aria-describedby=\"ada-font-select-describedby\" aria-labelledby=\"ada-font-select-describedby\" name=\"font\" class=\"form-control\" id=\"font-select-input\">",
        "                <option value=\"{{font}}\">{{font}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "{{{additionalFontList}}}",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-size\" for=\"size-input\">size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"size\" type=\"text\" class=\"form-control\" id=\"size-input\" value=\"{{size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-color\" for=\"color-input\">color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"color\" type=\"text\" class=\"form-control\" id=\"color-input\" value=\"{{color}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-align\" for=\"align-input\">align</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-align-2\" for=\"align-select-input\">",
        "            <select aria-describedby=\"ada-align-select-describedby\" aria-labelledby=\"ada-align-select-describedby\" name=\"align\" class=\"form-control\" id=\"align-select-input\">",
        "                <option value=\"{{align}}\">{{align}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"left\">left</option>",
        "                <option value=\"center\">center</option>",
        "                <option value=\"right\">right</option>",
        // "                <option value=\"top-left\">top-left</option>",
        // "                <option value=\"top-center\">top-center</option>",
        // "                <option value=\"top-right\">top-right</option>",
        // "                <option value=\"bottom-left\">bottom-left</option>",
        // "                <option value=\"bottom-center\">bottom-center</option>",
        // "                <option value=\"bottom-right\">bottom-right</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />Cell borders - array of point values as NESW: [0, 0, 1, 0]",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-border\" for=\"border-input\">border</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"border\" type=\"text\" class=\"form-control\" id=\"border-input\" value=\"{{border}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-border_color\" for=\"border_color-input\">border_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"border_color\" type=\"text\" class=\"form-control\" id=\"border_color-input\" value=\"{{border_color}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />Positioning",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfCells\" for=\"numberOfCells-input\">numberOfCells</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfCells\" type=\"text\" class=\"form-control\" id=\"numberOfCells-input\" value=\"{{numberOfCells}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-rowOrColumn\" for=\"rowOrColumn-select-input\">rowOrColumn</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-rowOrColumn-2\" for=\"rowOrColumn-select-input\">",
        "            <select aria-describedby=\"ada-rowOrColumn-select-describedby\" aria-labelledby=\"ada-rowOrColumn-select-describedby\" name=\"rowOrColumn\" class=\"form-control\" id=\"rowOrColumn-select-input\">",
        "                <option value=\"{{rowOrColumn}}\">{{rowOrColumn}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"row\">row</option>",
        "                <option value=\"column\">column</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]


};

module.exports.set_attribute = set_attribute;