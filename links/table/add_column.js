/**
 * <h4>Link.table.add_column<hr></h4>
 *
 * <p>Definition for the add_column link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.add_column
 * @version 0.1
 * @type {{name: string, fullname: string, source: string, target: string, startRow: string, startColumn: string, customList: string[], fetch: Link.table.add_column.fetch, template: string[], addColumn: Link.table.add_column.addColumn}}
 */

const add_column = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'add_column',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.add_column',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'startColumn',
        ''
    ],

    /**
     * <h4>addColumn()<hr></h4>
     * <p>Adds a column of cells to a table object</p>
     * <p>for zombie call, use: </p>
     * <p>    zAddColumn = Anklet.LinkManager.zombifyThis('table.add_column', 'addColumn');</p>
     *
     * @param startRow
     * @param startCol
     * @param theseCells
     * @param thisTable
     * @return {*}
     */
    addColumn: function(startRow, startCol, theseCells, thisTable) {

        try {
            let workTable,
                numRows,
                thisRowIndex,
                workRow,
                thisCell;

            // check inputs
            if (thisTable.hasOwnProperty('table')) {
                workTable = thisTable.table;
            }
            else {
                workTable = thisTable;
            }
            numRows = workTable.length;

            // ensure these are integers
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            thisRowIndex = startRow;

            while (theseCells.length > 0) {
                // ensure we have enough rows
                if (numRows <= thisRowIndex) {
                    // we need to add some rows
                    while (workTable.length <= thisRowIndex) {
                        workTable.push([]);
                    }
                    workRow = [];
                }
                else {
                    workRow = workTable[thisRowIndex];
                }

                // ensure we have enough columns
                if (workRow.length <= startCol + 1) {
                    // add cells to the row
                    while (workRow.length <= startCol) {
                        workRow.push({});
                    }
                }

                // put the cell in the row at the column location
                thisCell = theseCells.shift();
                workRow[startCol] = Object.assign({}, thisCell);

                // put the row back
                workTable[thisRowIndex] = workRow;

                // increment the row counter
                thisRowIndex++;
            }

            // return it
            thisTable.table = workTable;
            return thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const thisRow = this.readSource(this.source),
                thisTable = this.addColumn(this.startRow, this.startColumn, thisRow, workspace[this.target]);

            workspace[this.target] = thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ],

    /**
     * <h4>readSource()<hr></h4>
     * <p>assumed, the 'source' input field contains a JSON array of values</p>
     *
     * @returns {Array}
     */
    readSource: function() {

        let thisList = [],
            thisSource = JSON.parse(this.source),
            thisValue;

        for (thisValue of thisSource) {
            thisList.push({'value': thisValue});
        }

        return thisList;

    }


};

module.exports.add_column = add_column;