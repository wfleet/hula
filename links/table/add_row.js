/**
 * <h4>Link.table.add_row<hr></h4>
 *
 * <p>Definition for the add_row link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.add_row
 * @version 0.1
 * @type {{name: string, fullname: string, target: string, startRow: string, numberOfRows: string, startColumn: string, source: string, customList: string[], fetch: Link.table.add_row.fetch, template: string[], readSource: (function(): Array), addRow: Link.table.add_row.addRow}}
 */

const add_row = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'add_row',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.add_row',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'target',
        'startRow',
        'numberOfRows',
        'startColumn',
        'source',
        ''
    ],

    /**
     * <h4>addRow()<hr></h4>
     * <p>Adds a row of cells to a table object</p>
     * <p>for zombie call, use: </p>
     * <p>    zAddRow = Anklet.LinkManager.zombifyThis('table.add_row', 'addRow');</p>
     *
     * @param startRow
     * @param startCol
     * @param theseCells
     * @param thisTable
     * @return {*}
     */
    addRow: function(startRow, startCol, theseCells, thisTable) {

        try {
            let workTable,
                numRows,
                thisColIndex,
                workRow,
                i;

            // check inputs
            if (thisTable.hasOwnProperty('table')) {
                workTable = thisTable.table;
            }
            else {
                workTable = thisTable;
            }
            numRows = workTable.length;


            // ensure these are integers
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);

            // ensure there are enough rows
            if (numRows <= startRow) {
                // we need to add some rows
                while (workTable.length <= startRow) {
                    workTable.push([]);
                }
                workRow = [];
            }
            else {
                workRow = workTable[startRow];
            }

            // ensure there are enough columns
            if (workRow.length < startCol + 1) {
                // add cells to the row
                while (workRow.length < startCol + 1) {
                    workRow.push({});
                }
            }

            // walk through the row to add
            for (i = 0; i < theseCells.length; i++) {
                thisColIndex = startCol + i;
                if (thisColIndex >= workRow.length) {
                    workRow.push(Object.assign({}, theseCells[i]));
                }
                else {
                    workRow[thisColIndex] = Object.assign({}, theseCells[i]);
                }
            }

            // push the row back into the table
            workTable[startRow] = workRow;

            // return it
            thisTable.table = workTable;
            return thisTable;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const thisRow = this.readSource(this.source),
                thisTable = this.addRow(this.startRow, this.startColumn, thisRow, workspace[this.target]);

            workspace[this.target] = thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ],

    /**
     * <h4>readSource()<hr></h4>
     * <p>assumed, the 'source' input field contains a JSON array of values</p>
     *
     * @returns {Array}
     */
    readSource: function() {

        let thisList = [],
            thisSource = JSON.parse(this.source),
            thisValue;

        for (thisValue of thisSource) {
            thisList.push({'value': thisValue});
        }

        return thisList;

    }


};

module.exports.add_row = add_row;