/**
 * <h4>Link.table.get_column<hr></h4>
 *
 * <p>Definition for the get_column link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.table.get_column
 * @version 0.1
 * @type {{name: string, fullname: string, source: string, target: string, startRow: string, startColumn: string, numberOfCells: string, customList: string[], getColumn: Link.table.get_column.getColumn, fetch: Link.table.get_column.fetch, template: string[]}}
 */

const get_column = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'get_column',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.table.get_column',

    /**
     * <h4>source<hr></h4>
     * @type {string}
     */
    source: '',

    /**
     * <h4>target<hr></h4>
     * @type {string}
     */
    target: '',

    /**
     * <h4>startRow<hr></h4>
     * @type {string}
     */
    startRow: '',

    /**
     * <h4>startColumn<hr></h4>
     * @type {string}
     */
    startColumn: '',

    /**
     * <h4>numberOfCells<hr></h4>
     * @type {string}
     */
    numberOfCells: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'source',
        'target',
        'startRow',
        'startColumn',
        'numberOfCells',
        ''
    ],

    /**
     * <h4>getColumn()<hr></h4>
     * <p> returns an Array of the table cells requested.</p>
     * <p>for zombie call, use: </p>
     * <p>    zGetColumn = Anklet.LinkManager.zombifyThis('table.get_column', 'getColumn');</p>
     *
     * @param startRow
     * @param startCol
     * @param numberOfCells
     * @param thisTable
     * @return {Array}
     */
    getColumn: function(startRow, startCol, numberOfCells, thisTable) {

        try {
            let workTable = thisTable,
                thisRow,
                endRow,
                theseColCells = [],
                i;

            // integerize
            startRow = parseInt(startRow);
            startCol = parseInt(startCol);
            numberOfCells = parseInt(numberOfCells);

            if (startRow >= workTable.length) {
                // if we're out of range, return empty
                return theseColCells;
            }
            else if (startRow + numberOfCells >= workTable.length) {
                // if we don't have enough rows, truncate the return
                endRow = workTable.length - startRow + 1;
            }
            else {
                // otherwise, return the numbers of cells called for
                endRow = startRow + numberOfCells;
            }

            for (i = startRow; i < endRow; i++) {
                thisRow = thisTable[i];
                if (!thisRow || startCol >= thisRow.length) {
                    // again, if we're out of range, return empty
                    // in effect, if we are walking down a column and find a short row, we end it there and return what we have so far
                    return theseColCells;
                }
                else {
                    theseColCells.push(thisRow[startCol]);
                }
            }

            return theseColCells;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let thisColumn = this.getColumn(this.startRow, this.startColumn, this.numberOfCells, workspace[this.source].table);

            workspace[this.target] = thisColumn;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startRow\" for=\"startRow-input\">startRow</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startRow\" type=\"text\" class=\"form-control\" id=\"startRow-input\" value=\"{{startRow}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startColumn\" for=\"startColumn-input\">startColumn</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startColumn\" type=\"text\" class=\"form-control\" id=\"startColumn-input\" value=\"{{startColumn}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-numberOfCells\" for=\"numberOfCells-input\">numberOfCells</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"numberOfCells\" type=\"text\" class=\"form-control\" id=\"numberOfCells-input\" value=\"{{numberOfCells}}\">",
        "        </div>",
        "    </div>",
        ""
    ]

};

module.exports.get_column = get_column;