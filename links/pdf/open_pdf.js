/**
 * <h4>Link.pdf.open_pdf<hr></h4>
 *
 * <p>Definition for the open_pdf link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.open_pdf
 * @version 0.1
 */

const open_pdf = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'open_pdf',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.open_pdf',

    /**
     * <h4>layout<hr></h4>
     */
    layout: '',

    /**
     * <h4>size<hr></h4>
     */
    size: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'layout',
        'size',
        ''
    ],

    /**
     * <h4>openPDF()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zOpenPDF = Anklet.LinkManager.zombifyThis('pdf.open_pdf', 'openPDF');</p>
     *
     * @param thisLayout
     * @param thisSize
     * @param workspace
     * @return {PDFDocument}
     */
    openPDF: function(thisLayout, thisSize, workspace) {

        try {
            const PDFDocument = require('pdfkit');
            let width,
                height;

            // start the pdf doc
            let thisDoc = new PDFDocument({
                autoFirstPage: false,
                margin: 0,
                compress: false,
                layout: thisLayout
            });

            // register additional fonts
            if (Anklet.Config.fonts.length > 0) {
                const path = require('path');
                let thisFont,
                    thisFontName,
                    thisFontPath;
                for (thisFont of Anklet.Config.fonts) {
                    thisFontName = Object.getOwnPropertyNames(thisFont)[0];
                    thisFontPath = path.join(Anklet.Config.constants.hula_root, thisFont[thisFontName]);
                    console.log('Registering font ' + thisFontName + ': ' + thisFontPath);
                    thisDoc.registerFont(thisFontName, thisFontPath);
                }
            }


            // set the page size
            if (thisSize.charAt(0) === '[' && Array.isArray(JSON.parse(thisSize))) {
                // if it's a size array, set clipping box
                let sizeArray = JSON.parse(thisSize);
                width = sizeArray[0];
                height = sizeArray[1];

                // add one with the correct size
                thisDoc.addPage({
                    size: [width, height],
                    width: width,
                    height: height,
                    margin: 0
                    });
            }
            else {
                thisDoc.addPage({
                    size: thisSize,
                    margin: 0
                })
            }

            return thisDoc;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            workspace.outputPDF = this.openPDF(this.layout, this.size, workspace);
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-layout\" for=\"layout-select-input\">layout</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-layout-2\" for=\"layout-select-input\">",
        "            <select aria-describedby=\"ada-layout-select-describedby\" aria-labelledby=\"ada-layout-select-describedby\" name=\"layout\" class=\"form-control\" id=\"layout-select-input\">",
        "                <option value=\"portrait\">portrait</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"portrait\">portrait</option>",
        "                <option value=\"landscape\">landscape</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        size is a predefined size name, like 'letter', or an array of point sizes, like '[612, 792]'",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-size\" for=\"size-input\">size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"size\" type=\"text\" class=\"form-control\" id=\"size-input\" value=\"{{size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.open_pdf = open_pdf;