/**
 * <h4>Link.pdf.pdf_text_line<hr></h4>
 *
 * <p>Definition for the pdf_text_line link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_text_line
 * @version 0.1
 */

const pdf_text_line = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_text_line',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_text_line',

    /**
     * <h4>text<hr></h4>
     */
    text: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>font_name<hr></h4>
     */
    font_name: '',

    /**
     * <h4>font_size<hr></h4>
     */
    font_size: '',

    /**
     * <h4>font_color<hr></h4>
     */
    font_color: '',

    /**
     * <h4>additionalFontList<hr></h4>
     */
    additionalFontList: Anklet.fontList,

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'text',
        'x',
        'y',
        'font_name',
        'font_size',
        'font_color',
        'additionalFontList',
        ''
    ],

    /**
     * <h4>textLine()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zTextLine = Anklet.LinkManager.zombifyThis('pdf.pdf_text_line', 'textLine');</p>
     *
     * @param text
     * @param x
     * @param y
     * @param font_name
     * @param font_size
     * @param font_color
     * @return {string}
     * @param workspace
     */
    textLine: function(text, x, y, font_name, font_size, font_color, thisDoc) {

        try {
            let options = {},
                spacing;

            // set the font attributes
            if (font_name) {
                thisDoc.font(font_name);
            }
            if (font_size) {
                thisDoc.fontSize(parseFloat(font_size));
            }
            if (font_color) {
                thisDoc.fill(font_color);
            }

            // // set spacing options
            // if (thisDoc.thisLineGap) {
            //     options.lineGap = thisDoc.thisLineGap;
            // }
            // if (thisDoc.thisParagraphGap) {
            //     options.paragraphGap = thisDoc.thisParagraphGap;
            // }
            //
            // if x and y are not supplied, just use thisDoc.x and thisDoc.y
            if (!x) {
                x = thisDoc.x;
            }
            else {
                // ensure it's a number
                x = parseFloat(x);
            }
            if (!y) {
                thisDoc.moveDown();
                // y = parseFloat(thisDoc.y);
                thisDoc.text(text, x);
            }
            // else if (typeof y === 'string' && (y.charAt(0) === ' ' || y.charAt(0) === '+' || y.charAt(0) === '-')) {
            //     // add (or remove the space from the current point)
            //     spacing = parseFloat(y);
            //     y = thisDoc.y + spacing;
            //     thisDoc.text(text, x, y, options);
            // }
            else {
                y = parseFloat(y);
                thisDoc.text(text, x, y, options);
            }

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            // in the case of a pdfkit PDF doc, the workspace.outputPDF references the doc object
            const thisLine = Anklet.LinkManager.interpolateValue(this.text, workspace);
            let thisDoc = workspace.outputPDF;

            workspace.outputPDF = this.textLine(thisLine, this.x, this.y, this.font_name, this.font_size, this.font_color, thisDoc);
         }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        a single line of text, of possibly infinite length.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-text\" for=\"text-input\">text</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"text\" type=\"text\" class=\"form-control\" id=\"text-input\" value=\"{{text}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "       x and y origin, in points.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "{{{additionalFontList}}}",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        ""
    ]
};

module.exports.pdf_text_line = pdf_text_line;