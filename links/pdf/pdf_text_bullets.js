/**
 * <h4>Link.pdf.pdf_text_bullets<hr></h4>
 *
 * <p>Definition for the pdf_text_bullets link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_text_bullets
 * @version 0.1
 */

const pdf_text_bullets = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_text_bullets',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_text_bullets',

    /**
     * <h4>text<hr></h4>
     */
    text: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>width<hr></h4>
     */
    width: '',

    /**
     * <h4>height<hr></h4>
     */
    height: '',

    /**
     * <h4>font_name<hr></h4>
     */
    font_name: 'Helvetica',

    /**
     * <h4>font_size<hr></h4>
     */
    font_size: '12',

    /**
     * <h4>font_color<hr></h4>
     */
    font_color: '#000000',

    /**
     * <h4>indent<hr></h4>
     */
    indent: '18',

    /**
     * <h4>bullet<hr></h4>
     */
    bullet: 'disc',

    /**
     * <h4>bullet_size<hr></h4>
     */
    bullet_size: '',

    /**
     * <h4>line_space<hr></h4>
     */
    line_space: '',

    /**
     * <h4>para_space<hr></h4>
     */
    para_space: '',

    /**
     * <h4>additionalFontList<hr></h4>
     */
    additionalFontList: Anklet.fontList,

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'text',
        'x',
        'y',
        'width',
        'height',
        'font_name',
        'font_size',
        'font_color',
        'bullet',
        'bullet_size',
        'indent',
        'line_space',
        'para_space',
        'additionalFontList',
        ''
    ],

    /**
     * <h4>textBullets()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zTextBullets = Anklet.LinkManager.zombifyThis('pdf.pdf_text_bullets', 'textBullets');</p>
     *
     * @param workspace
     */
    textBullets: function(texts, x, y, indent, bullet, bullet_size, font_name, font_size, font_color, options, thisDoc) {

        try {

            const zTextLine = Anklet.LinkManager.zombifyThis('pdf.pdf_text_line', 'textLine'),
                zTextBlock = Anklet.LinkManager.zombifyThis('pdf.pdf_text_block', 'textBlock');

            let thisItem,
                thisBulletX,
                thisX,
                thisBulletY,
                thisY,
                thisBulletGlyph,
                thisWidth,
                thisHeight,
                thisItemHeight,
                thisLeading,
                thisParaSpace,
                spacing;

            // set the font defaults
            if (!font_name) {
                font_name = 'Helvetica';
            }
            if (!font_size) {
                font_size = 12;
            }
            else {
                font_size = parseFloat(font_size);
            }
            if (!font_name) {
                font_color = '#000000';
            }

            // map out the bullet
            if (!indent) {
                // default
                indent = 24;
            }
            else {
                indent = parseFloat(indent);
            }
            if (!bullet_size) {
                // default
                bullet_size = font_size * .7;
            }
            else {
                bullet_size = parseFloat(bullet_size);
            }

            // the exes
            if (!x) {
                thisBulletX = thisDoc.x;
            }
            // else if (typeof x === 'string' && (x.charAt(0) === ' ' || x.charAt(0) === '+' || x.charAt(0) === '-')) {
            //     // add (or remove the space from the current point)
            //     spacing = parseFloat(x);
            //     thisBulletX = thisDoc.x + spacing;
            // }
            else {
                thisBulletX = parseFloat(x);
            }
            thisX = thisBulletX + indent;

            // figure out why
            if (y) {
                if (typeof y === 'string' && (y.charAt(0) === ' ' || y.charAt(0) === '+' || y.charAt(0) === '-')) {
                    // add (or remove the space from the current point)
                    spacing = parseFloat(y);
                    thisY = parseFloat(thisDoc.y) + spacing;
                }
                else {
                    thisY = parseFloat(y);
                }
            }
            else {
                thisY = thisDoc.y;
            }
            thisBulletY = thisY + (font_size * .6);

            // the options
            thisWidth = thisHeight = thisLeading = thisParaSpace = undefined;
            if (Object.keys(options).length > 0) {
                if (options.width) {
                    thisWidth = parseFloat(options.width) - indent;
                    options.width = thisWidth;
                }
                if (options.height) {
                    thisHeight = parseFloat(options.height);
                    options.height = thisHeight;
                }
                if (options.lineGap) {
                    thisLeading = parseFloat(options.lineGap);
                    options.lineGap = thisLeading;
                }
                if (options.paragraphGap) {
                    thisParaSpace = parseFloat(options.paragraphGap);
                    options.paragraphGap = thisParaSpace;
                }
            }

            // the bullet glyphs
            switch (bullet) {
                case 'disc':
                    thisBulletGlyph = '\154';
                    break;
                case 'circle':
                    thisBulletGlyph = '\155';
                    break;
                case 'block':
                    thisBulletGlyph = '\156';
                    break;
                case 'box':
                    thisBulletGlyph = '\157';
                    break;
                default:
                    thisBulletGlyph = '\154';
            }

            // walk through the items
            texts = texts.split(/\n/g);
            for (thisItem of texts) {
                // console.log('bullets: ' + thisItem);

                // put the bullet there
                thisDoc.font('ZapfDingbats');
                thisDoc.fontSize(bullet_size);
                thisDoc = zTextLine(thisBulletGlyph, thisBulletX, thisBulletY, '', '', '', thisDoc);
                thisDoc.y = thisY;

                // set the font attributes
                if (font_name) {
                    thisDoc.font(font_name);
                }
                if (font_size) {
                    thisDoc.fontSize(font_size);
                }
                if (font_name) {
                    thisDoc.fill(font_color);
                }

                // figure out if block or line
                if (!options.width) {
                    // text, x, y - it's a text line
                    thisDoc = zTextLine(thisItem, thisX, thisY, '', '', '', thisDoc);
                }
                else {
                    // text, x, y, width - it's a text block
                    thisDoc = zTextBlock(thisItem, thisX, thisY, '', '', '', options, thisDoc);
                }

                // calculate the next line y values
                if (thisHeight) {
                    thisItemHeight = thisHeight;
                }
                else {
                    thisItemHeight = thisDoc.heightOfString(thisItem, options);
                }
                thisY += thisItemHeight;
                thisBulletY += thisItemHeight;
            }

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisText = Anklet.LinkManager.interpolateValue(this.text, workspace).join('\n');
            let thisDoc = workspace.outputPDF,
                options = {};

            // figure out the options
            options.align = 'left';
            if (this.width) {
                options.width = parseFloat(this.width);
            }
            if (this.height) {
                options.height = parseFloat(this.height);
            }
            if (this.line_space) {
                options.lineGap = this.line_space;
            }
            if (this.para_space) {
                options.paragraphGap = this.para_space;
            }

            workspace.outputPDF = this.textBullets(thisText, this.x, this.y, this.indent, this.bullet, this.bullet_size, this.font_name, this.font_size, this.font_color, options, thisDoc);

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-text\" for=\"text-input\">text</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"text\" type=\"text\" class=\"form-control\" id=\"text-input\" rows=\"5\">{{arrayText text}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        values in points.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "{{{additionalFontList}}}",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet\" for=\"bullet-select-input\">bullet</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-bullet-2\" for=\"bullet-select-input\">",
        "            <select aria-describedby=\"ada-bullet-select-describedby\" aria-labelledby=\"ada-bullet-select-describedby\" name=\"bullet\" class=\"form-control\" id=\"bullet-select-input\">",
        "                <option value=\"disc\">disc</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"disc\">disc</option>",
        "                <option value=\"circle\">circle</option>",
        "                <option value=\"block\">block</option>",
        "                <option value=\"box\">box</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet_size\" for=\"bullet_size-input\">bullet_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bullet_size\" type=\"text\" class=\"form-control\" id=\"bullet_size-input\" value=\"{{bullet_size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-indent\" for=\"indent-input\">indent</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"indent\" type=\"text\" class=\"form-control\" id=\"indent-input\" value=\"{{indent}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        additional space in points between each line and paragraph.",
        "    </div>",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_space\" for=\"line_space-input\">line_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_space\" type=\"text\" class=\"form-control\" id=\"line_space-input\" value=\"{{line_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-para_space\" for=\"para_space-input\">para_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"para_space\" type=\"text\" class=\"form-control\" id=\"para_space-input\" value=\"{{para_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.pdf_text_bullets = pdf_text_bullets;