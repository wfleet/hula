/**
 * <h4>Link.pdf.pdf_image<hr></h4>
 *
 * <p>Definition for the pdf_image link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_image
 * @version 0.1
 */

const pdf_image = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_image',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_image',

    // /**
    //  * <h4>url<hr></h4>
    //  */
    // url: '',
    //
    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>width<hr></h4>
     */
    width: '',

    /**
     * <h4>height<hr></h4>
     */
    height: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        // 'url',
        'source',
        'x',
        'y',
        'width',
        'height',
        ''
    ],

    // /**
    //  * <h4>getImage()<hr></h4>
    //  * <p>for external (zombie) call, use:</p>
    //  * <p>    zGetImage = Anklet.LinkManager.zombifyThis('pdf.pdf_image', 'getImage');</p>
    //  *
    //  * @param url
    //  * @return {string}
    //  */
    // getImage: function(url, workspace) {
    //
    //     try {
    //         const requestPromise = require('request-promise-native'),
    //             thisURI = Anklet.LinkManager.interpolateValue(this.url, workspace),
    //
    //             // set up the options object for the http image fetch
    //             theseOptions = {
    //                 uri: thisURI,
    //                 method: 'GET',
    //                 encoding: null,
    //                 headers: {
    //                     'Connection': 'keep-alive',
    //                     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
    //                     'Upgrade-Insecure-Requests': '1',
    //                     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    //                     'Accept-Encoding': 'gzip, deflate',
    //                     'Accept-Language': 'en-US,en;q=0.9'
    //                 }
    //             };
    //
    //         // run the GET as a promise
    //         return requestPromise(theseOptions)
    //             .then((response) => {
    //                 // handle the response as a buffer-to-image
    //                 let thisImage = new Buffer(response.data, 'binary').toString('base64');
    //
    //                 return thisImage;
    //             });
    //     }
    //     catch (error) {
    //         console.error(error);
    //         throw error;
    //     }
    //
    // },

    // /**
    //  * <h4>fetch()<hr></h4>
    //  * <p></p>
    //  *
    //  * @param workspace
    //  */
    // fetch: function(workspace) {
    //
    //     try {
    //         if (this.url) {
    //             // if there's a url, get the image and place it the target
    //             workspace[this.target] = this.getImage(this.url, workspace);
    //         }
    //     }
    //     catch (error) {
    //         console.error(error);
    //         throw error;
    //     }
    //
    // },

     /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

         try {
             let thisImage,
                 pdfDoc = workspace.outputPDF,
                 thisWidth = this.width,
                 thisHeight = this.height,
                 x = (this.x) ? parseFloat(this.x) : parseFloat(pdfDoc.x),
                 // y = (this.y) ? parseFloat(this.y) : parseFloat(pdfDoc.y),
                 y = this.y,
                 spacing;

             // see if y is relative or not
             if (!y) {
                 y = parseFloat(pdfDoc.y);
             }
             else if (typeof y === 'string' && (y.charAt(0) === ' ' || y.charAt(0) === '+' || y.charAt(0) === '-')) {
                 // add (or remove the space from the current point)
                 spacing = parseFloat(y);
                 y = pdfDoc.y + spacing;
                 pdfDoc.y = y;
             }
             else {
                 y = parseFloat(y);
             }

             // figure out the image resource
             if (this.source.charAt(0) === '#') {
                 // it's a file reference - get the path and pass it through
                 const fs = require('fs'),
                     path = require('path'),
                     filename = this.source.replace(/#/, ''),
                     filePath = path.join(Anklet.Config.constants.assetsFolder, filename),
                     thisBase64String = fs.readFileSync(filePath, { encoding: 'base64' });

                 thisImage = 'data:image/png;base64,' + thisBase64String;
             }
             else {
                 thisImage = Anklet.LinkManager.resolveValue(this.source, workspace);
             }

             if (!thisWidth && !thisHeight) {
                 pdfDoc.image(thisImage, x, y);
             }
             else if (thisWidth && !thisHeight) {
                 pdfDoc.image(thisImage, x, y, {width: thisWidth});
             }
             else if (!thisWidth && thisHeight) {
                 pdfDoc.image(thisImage, x, y, {height: thisHeight});
             }
             else if (thisWidth && thisHeight) {
                 pdfDoc.image(thisImage, x, y, {fit: [thisWidth, thisHeight]});
             }

             workspace.outputPDF = pdfDoc;

         }
         catch (error) {
             console.error(error);
             throw error;
         }
    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        url of image to place, or source reference to already fetched image.",
        "    </div>",
        "</div>",
        "",
        "",
        // "<div class=\"col-grid col-1-3\">",
        // "",
        // "    <div class=\"align-right field-name\">",
        // "        <label id=\"label-url\" for=\"url-input\">url</label>",
        // "    </div>",
        // "    <div class=\"link-field\">",
        // "        <div class=\"form-group\">",
        // "            <input name=\"url\" type=\"text\" class=\"form-control\" id=\"url-input\" value=\"{{url}}\">",
        // "        </div>",
        // "    </div>",
        // "",
        // "</div>",
        // "",
        // "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        x and y origin in points, and width and height in points. if width and height are omitted, image is placed at full size. If width OR height are supplied, image will be scaled to fit that dimension. If width AND height are supplied, image will be fit within that box.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.pdf_image = pdf_image;