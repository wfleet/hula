/**
 * <h4>Link.pdf.pdf_table_output<hr></h4>
 *
 * <p>Definition for the pdf_table_output link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_table_output
 * @version 0.1
 * @type {{name: string, fullname: string, source: string, x: string, y: string, width: string, height: string, min_cell_width: string, max_cell_width: string, cell_margin: string, cell_padding: string, customList: string[], tableOutput: Link.pdf.pdf_table_output.tableOutput, render: Link.pdf.pdf_table_output.render, template: string[]}}
 */

const pdf_table_output = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_table_output',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_table_output',

    /**
     * <h4>source<hr></h4>
     */
    source: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>width<hr></h4>
     */
    width: '',

    /**
     * <h4>height<hr></h4>
     */
    height: '',

    /**
     * <h4>min_cell_width<hr></h4>
     */
    min_cell_width: '',

    /**
     * <h4>max_cell_width<hr></h4>
     */
    max_cell_width: '',

    /**
     * <h4>cell_margin<hr></h4>
     */
    cell_margin: '',

    /**
     * <h4>cell_padding<hr></h4>
     */
    cell_padding: '',

    /**
     * <h4>show_cell_grid<hr></h4>
     */
    show_cell_grid: 0,

    /**
     * <h4>cell_grid_color<hr></h4>
     */
    cell_grid_color: '#CCCCCC',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'source',
        'x',
        'y',
        'width',
        'height',
        'min_cell_width',
        'max_cell_width',
        'cell_margin',
        'cell_padding',
        'show_cell_grid',
        'cell_grid_color',
        ''
    ],

    /**
     * <h4>assessTable()<hr></h4>
     * <p>first pass</p>
     * <p>runs an assessment on the table, counting rows and columns, and setting widths</p>
     *
     * @param thisTable
     * @param thisDoc
     * @returns {*}
     */
    assessTable: function(thisTable, thisDoc) {

        try {
            let numCols = 0,
                colWidths = [],
                thisRowHeight,
                thisRow,
                thisCell,
                thisCellHeight,
                tableWidth,
                colsRemaining,
                baseCellWidth,
                i;

            // set base fonts and color
            thisTable.base_font = thisDoc._font.name;
            thisTable.base_font_size = thisDoc._fontSize;
            thisTable.base_font_color = "#000000";
            thisTable.show_cell_grid = this.show_cell_grid;
            thisTable.cell_grid_color = this.cell_grid_color;

            // count the columns
            for (thisRow of thisTable.table) {
                // tally up the cell widths of each row
                numCols = Math.max(numCols, thisRow.length);

                // gather any set width values
                for (i = 0; i < thisRow.length; i++) {
                    thisCell = thisRow[i];
                    if (!colWidths[i]) {
                        colWidths[i] = 0;
                    }
                    if (thisCell.width) {
                        colWidths[i] = Math.max(colWidths[i], thisCell.width);
                    }
                }
            }

            // divide up the remaining widths
            tableWidth = thisTable.width;
            tableWidth = tableWidth - (numCols * parseFloat(thisTable.cell_margin) * 2);
            tableWidth = (tableWidth < 0) ? 0 : tableWidth;

            // after the set cell widths, how much space is left, and how many remaining cells?
            colsRemaining = colWidths.length;
            if (tableWidth > 0) {
                for (i = 0; i < colWidths.length; i++) {
                    if (colWidths[i]) {
                        if (thisTable.max_cell_width) {
                            colWidths[i] = Math.min(colWidths[i], thisTable.max_cell_width);
                        }
                        colsRemaining--;
                        tableWidth = tableWidth - colWidths[i];
                    }
                }
            }

            // divide the remaining space into the remaining cols, then make sure they are within limits
            baseCellWidth = Math.min(Math.max((tableWidth / colsRemaining), thisTable.min_cell_width), thisTable.max_cell_width);

            // put the finalized widths back into the colWidths array
            for (i = 0; i < colWidths.length; i++) {
                if (colWidths[i] === 0) {
                    colWidths[i] = baseCellWidth;
                }
            }

            // figure out the row and cell heights
            for (thisRow of thisTable.table) {
                // set a minimum
                thisRowHeight = 6;
                // walk through the cells in the row
                for (i = 0; i < thisRow.length; i++) {
                    thisCell = thisRow[i];
                    // set font and sizing and color defaults
                    if (thisCell.font) {
                        thisDoc.font(thisCell.font);
                    }
                    else {
                        thisDoc.font(thisTable.base_font);
                        thisCell.font = thisTable.base_font;
                    }
                    if (thisCell.size) {
                        thisDoc.fontSize(thisCell.size);
                    }
                    else {
                        thisDoc.fontSize(thisTable.base_font_size);
                        thisCell.size = thisTable.base_font_size;
                    }
                    if (!thisCell.color) {
                        thisCell.color = thisTable.base_font_color;
                    }
                    thisCellHeight = thisDoc.heightOfString(thisCell.value, {width: colWidths[i]});
                    thisRowHeight = Math.max(thisRowHeight, thisCellHeight);
                }
                // walk through again and set the size values
                for (i = 0; i < thisRow.length; i++) {
                    thisCell = thisRow[i];
                    thisCell.width = colWidths[i];
                    thisCell.height = thisRowHeight;
                }
            }

            return thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    },

    /**
     * <h4>blockTable()<hr></h4>
     * <p>second pass</p>
     * <p>blocks out the table cells, figuring out placement, spacing and borders</p>
     *
     * @param thisTable
     * @returns {*}
     */
    blockTable: function(thisTable) {

        try {
            const startX = thisTable.x,
                startY = thisTable.y,
                thisInset = thisTable.cell_margin + thisTable.cell_padding;

            let thisX,
                thisY = startY,
                thisRow,
                thisCell,
                i;

            // walk through the rows
            for (thisRow of thisTable.table) {
                // set y offset
                thisY = thisY + thisInset;
                thisX = startX;

                // walk through the cells in each row
                for (thisCell of thisRow) {
                    thisX += thisInset;
                    thisCell.x = thisX;
                    thisCell.y = thisY;
                    thisX += thisCell.width + thisInset;
                }

                // finish y offset for start of next row
                thisY += thisCell.height + thisInset;
            }

            return thisTable;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    },

    /**
     * <h4>drawCell()<hr></h4>
     * <p>takes a defined cell and draws it into the pdfDoc</p>
     *
     * @param thisCell
     * @param thisTable
     * @param pdfDoc
     * @return {*}
     */
    drawCell: function(thisCell, thisTable, pdfDoc) {

        try {
            let cellPad = thisTable.cell_padding,
                leftX = thisCell.x - cellPad,
                rightX = thisCell.x + thisCell.width + cellPad,
                topY = thisCell.y - cellPad,
                bottomY = thisCell.y + thisCell.height + cellPad
                options = {};

            // set the font and size and all
            if (thisCell.font) {
                pdfDoc.font(thisCell.font);
            }
            if (thisCell.size) {
                pdfDoc.fontSize(thisCell.size);
            }
            if (thisCell.color) {
                pdfDoc.fillColor(thisCell.color);
            }

            // draw the border grid, if enabled
            if (thisTable.show_cell_grid) {
                thisTable.show_cell_grid = parseInt(thisTable.show_cell_grid);
            }
            if (thisTable.show_cell_grid) {
                pdfDoc.strokeColor(thisTable.cell_grid_color);
                pdfDoc.lineWidth(.25);
                pdfDoc.rect(leftX, topY, thisCell.width + (cellPad * 2), thisCell.height + (cellPad * 2));
                pdfDoc.stroke();
            }

            // draw the border attribute, if used
            if (thisCell.border) {
                // border is numeric array of NESW widths
                let north_border = thisCell.border[0],
                    east_border,
                    south_border,
                    west_border;

                // in case there's less than 4 values in the array
                east_border = (thisCell.border[1]) ? thisCell.border[1] : 0;
                south_border = (thisCell.border[2]) ? thisCell.border[2] : 0;
                west_border = (thisCell.border[3]) ? thisCell.border[3] : 0;

                pdfDoc.strokeColor(thisCell.border_color);
                if (north_border) {
                    pdfDoc.lineWidth(north_border);
                    pdfDoc.moveTo(leftX, topY);
                    pdfDoc.lineTo(rightX, topY);
                    pdfDoc.stroke();
                }
                if (east_border) {
                    pdfDoc.lineWidth(east_border);
                    pdfDoc.moveTo(rightX, topY);
                    pdfDoc.lineTo(rightX, bottomY);
                    pdfDoc.stroke();
                }
                if (south_border) {
                    pdfDoc.lineWidth(south_border);
                    pdfDoc.moveTo(leftX, bottomY);
                    pdfDoc.lineTo(rightX, bottomY);
                    pdfDoc.stroke();
                }
                if (west_border) {
                    pdfDoc.lineWidth(west_border);
                    pdfDoc.moveTo(leftX, topY);
                    pdfDoc.lineTo(leftX, bottomY);
                    pdfDoc.stroke();
                }
            }

            // place the text
            options.width = thisCell.width;
            options.height = thisCell.height;
            if (thisCell.align) {
                options.align = thisCell.align;
            }
            pdfDoc.text(thisCell.value, thisCell.x, thisCell.y, options);

            return pdfDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>tableOutput()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zTableOutput = Anklet.LinkManager.zombifyThis('pdf.pdf_table_output', 'tableOutput');</p>
     *
     * @param thisTable
     * @param x
     * @param y
     * @param width
     * @param height
     * @param min_cell_width
     * @param max_cell_width
     * @param cell_margin
     * @param cell_padding
     * @param thisDoc
     * @return {*}
     */
    tableOutput: function(thisTable, x, y, width, height, cell_margin, cell_padding, min_cell_width, max_cell_width, thisDoc) {

        try {
            let thisRow,
                thisCell;

            // set some metadata for the table
            thisTable.x = parseFloat(x);
            thisTable.y = parseFloat(y);
            thisTable.width = parseFloat(width) | 0;
            thisTable.height = parseFloat(height) | 0;
            thisTable.cell_margin = parseFloat(cell_margin) | 0;
            thisTable.cell_padding = parseFloat(cell_padding) | 0;
            thisTable.min_cell_width = parseFloat(min_cell_width) | 0;
            thisTable.max_cell_width = parseFloat(max_cell_width) | 0;

            // assess the table - count rows and columns, figure out widths
            thisTable = this.assessTable(thisTable, thisDoc);

            // block out the table - figure out placement, spacing and borders
            thisTable = this.blockTable(thisTable, thisDoc);

            // draw the table into the pdfDoc
            for (thisRow of thisTable.table) {
                for (thisCell of thisRow) {
                    thisDoc = this.drawCell(thisCell, thisTable, thisDoc);
                }
            }

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisTable = Anklet.LinkManager.resolveValue(this.source, workspace);
            let thisDoc = workspace.outputPDF;

            workspace.outputPDF = this.tableOutput(thisTable, this.x, this.y, this.width, this.height, this.cell_margin, this.cell_padding, this.min_cell_width, this.max_cell_width, thisDoc);
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-source\" for=\"source-input\">source</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"source\" type=\"text\" class=\"form-control\" id=\"source-input\" value=\"{{source}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-min_cell_width\" for=\"min_cell_width-input\">min_cell_width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"min_cell_width\" type=\"text\" class=\"form-control\" id=\"min_cell_width-input\" value=\"{{min_cell_width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-max_cell_width\" for=\"max_cell_width-input\">max_cell_width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"max_cell_width\" type=\"text\" class=\"form-control\" id=\"max_cell_width-input\" value=\"{{max_cell_width}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-cell_margin\" for=\"cell_margin-input\">cell_margin</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"cell_margin\" type=\"text\" class=\"form-control\" id=\"cell_margin-input\" value=\"{{cell_margin}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-cell_padding\" for=\"cell_padding-input\">cell_padding</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"cell_padding\" type=\"text\" class=\"form-control\" id=\"cell_padding-input\" value=\"{{cell_padding}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        draw all cell borders in a separate color",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-show_cell_grid-checkbox-1\" for=\"show_cell_grid-checkbox\">show_cell_grid</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-show_cell_grid-checkbox-2\" for=\"show_cell_grid-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"show_cell_grid-checkbox\" name=\"show_cell_grid\"{{#isEqual show_cell_grid '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-cell_grid_color\" for=\"cell_grid_color-input\">cell_grid_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"cell_grid_color\" type=\"text\" class=\"form-control\" id=\"cell_grid_color-input\" value=\"{{cell_grid_color}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        ""
    ]
};

module.exports.pdf_table_output = pdf_table_output;