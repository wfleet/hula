/**
 * <h4>Link.pdf.pdf_draw<hr></h4>
 *
 * <p>Definition for the pdf_draw link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_draw
 * @version 0.1
 */

const pdf_draw = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_draw',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_draw',

    /**
     * <h4>drawType<hr></h4>
     */
    drawType: '',

    /**
     * <h4>start_x<hr></h4>
     */
    start_x: '',

    /**
     * <h4>start_y<hr></h4>
     */
    start_y: '',

    /**
     * <h4>end_x<hr></h4>
     */
    end_x: '',

    /**
     * <h4>end_y<hr></h4>
     */
    end_y: '',

    /**
     * <h4>lineWidth<hr></h4>
     */
    lineWidth: '',

    /**
     * <h4>fillColor<hr></h4>
     */
    fillColor: '',

    /**
     * <h4>strokeColor<hr></h4>
     */
    strokeColor: '',

    /**
     * <h4>opacity<hr></h4>
     */
    opacity: '',

    /**
     * <h4>fillOpacity<hr></h4>
     */
    fillOpacity: '',

    /**
     * <h4>strokeOpacity<hr></h4>
     */
    strokeOpacity: '',

    /**
     * <h4>lineCap<hr></h4>
     */
    lineCap: '',

    /**
     * <h4>lineJoin<hr></h4>
     */
    lineJoin: '',

    /**
     * <h4>miterLimit<hr></h4>
     */
    miterLimit: '',

    /**
     * <h4>dash<hr></h4>
     */
    dash: '',

    /**
     * <h4>space<hr></h4>
     */
    space: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'drawType',
        'start_x',
        'start_y',
        'end_x',
        'end_y',
        'lineWidth',
        'fillColor',
        'strokeColor',
        'opacity',
        'fillOpacity',
        'strokeOpacity',
        'lineCap',
        'lineJoin',
        'miterLimit',
        'dash',
        'space',
        ''
    ],

    /**
     * <h4>pdfDraw()<hr></h4>
     * <p>draws boxes and lines for now, will add in other shapes and paths</p>
     * <p>for external (zombie) call, use:</p>
     * <p>    zPdfDraw = Anklet.LinkManager.zombifyThis('pdf.pdf_draw', 'pdfDraw');</p>
     *
     * @param start_x
     * @param start_y
     * @param end_x
     * @param end_y
     * @param lineWidth
     * @param fillColor
     * @param strokeColor
     * @param opacity
     * @param fillOpacity
     * @param strokeOpacity
     * @param lineCap
     * @param lineJoin
     * @param miterLimit
     * @param dash
     * @param space
     * @param thisDoc
     * @return {*}
     */
    pdfDraw: function(drawType, start_x, start_y, end_x, end_y, lineWidth, fillColor, strokeColor, fillOpacity, strokeOpacity, lineCap, lineJoin, miterLimit, dash, space, thisDoc) {

        try {
            // set line width
            if (lineWidth) {
                thisDoc.lineWidth(lineWidth);
            }

            // set color and opacity
            if (fillColor) {
                if (fillOpacity) {
                    thisDoc.fillColor(fillColor, fillOpacity);
                }
                else {
                    thisDoc.fillColor(fillColor);
                }
            }
            if (strokeColor) {
                if (strokeOpacity) {
                    thisDoc.strokeColor(strokeColor, strokeOpacity);
                }
                else {
                    thisDoc.strokeColor(strokeColor);
                }
            }

            // set miscellaneous line options
            if (lineCap) {
                thisDoc.lineCap(lineCap);
            }
            if (lineJoin) {
                thisDoc.lineJoin(lineJoin);
            }
            if (miterLimit) {
                thisDoc.miterLimit(miterLimit);
            }
            if (dash) {
                if (space) {
                    thisDoc.dash(dash, {'space': space});
                }
                else {
                    thisDoc.dash(dash);
                }
            }
            else {
                thisDoc.undash();
            }

            if (drawType === 'box') {
                // draw the box and fill/stroke it
                thisDoc.rect(start_x, start_y, (end_x - start_x), (end_y - start_y));
                if (lineWidth && fillColor) {
                    thisDoc.fillAndStroke();
                }
                else if (lineWidth) {
                    thisDoc.stroke();
                }
                else {
                    thisDoc.fill();
                }
            }
            else {
                // it's a line
                thisDoc.moveTo(start_x, start_y)
                    .lineTo(end_x, end_y)
                    .stroke();
            }

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            let thisDoc = workspace.outputPDF;

            thisDoc = this.pdfDraw(this.drawType, this.start_x, this.start_y, this.end_x, this.end_y, this.lineWidth, this.fillColor, this.strokeColor, this.fillOpacity, this.strokeOpacity, this.lineCap, this.lineJoin, this.miterLimit, this.dash, this.space, thisDoc);
            workspace.outputPDF =  thisDoc;

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at top left of the page.",
        "    </div>",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-drawType\" for=\"drawType-select-input\">drawType</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-drawType-2\" for=\"drawType-select-input\">",
        "            <select aria-describedby=\"ada-drawType-select-describedby\" aria-labelledby=\"ada-drawType-select-describedby\" name=\"drawType\" class=\"form-control\" id=\"drawType-select-input\">",
        "                <option value=\"{{drawType}}\">{{drawType}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"box\">box</option>",
        "                <option value=\"line\">line</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-lineWidth\" for=\"lineWidth-input\">lineWidth</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"lineWidth\" type=\"text\" class=\"form-control\" id=\"lineWidth-input\" value=\"{{lineWidth}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_x\" for=\"start_x-input\">start_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_x\" type=\"text\" class=\"form-control\" id=\"start_x-input\" value=\"{{start_x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_y\" for=\"start_y-input\">start_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_y\" type=\"text\" class=\"form-control\" id=\"start_y-input\" value=\"{{start_y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-end_x\" for=\"end_x-input\">end_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"end_x\" type=\"text\" class=\"form-control\" id=\"end_x-input\" value=\"{{end_x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-end_y\" for=\"end_y-input\">end_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"end_y\" type=\"text\" class=\"form-control\" id=\"end_y-input\" value=\"{{end_y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "",
        "    <div class=\"help-line\">",
        "        color values are an array specifying an RGB or CMYK color, a hex color string, or use any of the named CSS colors.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- example: [1, 0.5, 0], '#eee', '#4183C4', 'blue'.",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-fillColor\" for=\"fillColor-input\">fillColor</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"fillColor\" type=\"text\" class=\"form-control\" id=\"fillColor-input\" value=\"{{fillColor}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-strokeColor\" for=\"strokeColor-input\">strokeColor</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"strokeColor\" type=\"text\" class=\"form-control\" id=\"strokeColor-input\" value=\"{{strokeColor}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        additional options. Leave blank unless needed.",
        "    </div>",
        "</div>",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        opacity values are decimal values from 0 to 1.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-opacity\" for=\"opacity-input\">opacity</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"opacity\" type=\"text\" class=\"form-control\" id=\"opacity-input\" value=\"{{opacity}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-fillOpacity\" for=\"fillOpacity-input\">fillOpacity</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"fillOpacity\" type=\"text\" class=\"form-control\" id=\"fillOpacity-input\" value=\"{{fillOpacity}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-strokeOpacity\" for=\"strokeOpacity-input\">strokeOpacity</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"strokeOpacity\" type=\"text\" class=\"form-control\" id=\"strokeOpacity-input\" value=\"{{strokeOpacity}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-lineCap\" for=\"lineCap-select-input\">lineCap</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-lineCap-2\" for=\"lineCap-select-input\">",
        "            <select aria-describedby=\"ada-lineCap-select-describedby\" aria-labelledby=\"ada-lineCap-select-describedby\" name=\"lineCap\" class=\"form-control\" id=\"lineCap-select-input\">",
        "                <option value=\"\"></option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"butt\">butt</option>",
        "                <option value=\"round\">round</option>",
        "                <option value=\"square\">square</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-lineJoin\" for=\"lineJoin-select-input\">lineJoin</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-lineJoin-2\" for=\"lineJoin-select-input\">",
        "            <select aria-describedby=\"ada-lineJoin-select-describedby\" aria-labelledby=\"ada-lineJoin-select-describedby\" name=\"lineJoin\" class=\"form-control\" id=\"lineJoin-select-input\">",
        "                <option value=\"\"></option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"miter\">miter</option>",
        "                <option value=\"round\">round</option>",
        "                <option value=\"bevel\">bevel</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        values below are in points.",
        "    </div>",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-miterLimit\" for=\"miterLimit-select-input\">miterLimit</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <input name=\"miterLimit\" type=\"text\" class=\"form-control\" id=\"miterLimit-input\" value=\"{{miterLimit}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-dash\" for=\"dash-input\">dash</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"dash\" type=\"text\" class=\"form-control\" id=\"dash-input\" value=\"{{dash}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-space\" for=\"space-input\">space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"space\" type=\"text\" class=\"form-control\" id=\"space-input\" value=\"{{space}}\">",
        "        </div>",
        "    </div>",
        ""
    ]
};

module.exports.pdf_draw = pdf_draw;