/**
 * <h4>Link.pdf.close_pdf<hr></h4>
 *
 * <p>Definition for the close_pdf link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.close_pdf
 * @version 0.1
 * @type {{name: string, fullname: string, outputName: string, customList: string[], render: Link.pdf.close_pdf.render, template: string[]}}
 */

const close_pdf = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'close_pdf',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.close_pdf',

    /**
     * <h4>outputName<hr></h4>
     */
    outputName: 'outputPDF',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'outputName',
        ''
    ],

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            return new Promise(function (resolve, reject) {
                let thisDoc = workspace.outputPDF,
                    PDFparts = [],
                    pdfData;

                thisDoc.on('data', (data) => {
                    PDFparts.push(data);
                });
                thisDoc.on('end', () => {
                    pdfData = Buffer.concat(PDFparts);
                    resolve(pdfData);
                });
                thisDoc.end();
            })
            .then((pdfData) => {
                console.log(pdfData);
                workspace.output[this.id] = pdfData;
                if (this.outputName.search(/\.pdf/gi) < 0) {
                    workspace.outputName = this.outputName + '.pdf';
                }
                else {
                    workspace.outputName = this.outputName;
                }
                workspace.outputStream = pdfData;
                workspace.outputHeaders = [
                    'Content-Type: application/pdf',
                    'Content-Disposition: inline; filename="' + workspace.outputName + '"'
                ];
            });
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "       there are no other params to set, this link simply names, closes and completes the PDF document.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-outputName\" for=\"outputName-input\">outputName</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"outputName\" type=\"text\" class=\"form-control\" id=\"outputName-input\" value=\"{{outputName}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.close_pdf = close_pdf;