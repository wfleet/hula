/**
 * <h4>Link.pdf.pdf_text_items<hr></h4>
 *
 * <p>Definition for the pdf_text_items link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_text_items
 * @version 0.1
 */

const pdf_text_items = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_text_items',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_text_items',

    /**
     * <h4>items<hr></h4>
     */
    items: '',

    /**
     * <h4>font_name<hr></h4>
     */
    font_name: '',

    /**
     * <h4>font_size<hr></h4>
     */
    font_size: '',

    /**
     * <h4>font_color<hr></h4>
     */
    font_color: '',

    /**
     * <h4>line_space<hr></h4>
     */
    line_space: '',

    /**
     * <h4>para_space<hr></h4>
     */
    para_space: '',

    /**
     * <h4>additionalFontList<hr></h4>
     */
    additionalFontList: Anklet.fontList,

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'items',
        'font_name',
        'font_size',
        'font_color',
        'line_space',
        'para_space',
        'additionalFontList',
        ''
    ],

    /**
     * <h4>textItems()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zItems = Anklet.LinkManager.zombifyThis('pdf.pdf_text_items', 'items');</p>
     *
     * @param items
     * @param font_name
     * @param font_size
     * @param font_color
     * @param line_space
     * @param para_space
     * @param thisDoc
     * @return {string}
     */
    textItems: function(items, font_name, font_size, font_color, line_space, para_space, thisDoc) {

        try {
            const zTextLine = Anklet.LinkManager.zombifyThis('pdf.pdf_text_line', 'textLine'),
                zTextBlock = Anklet.LinkManager.zombifyThis('pdf.pdf_text_block', 'textBlock');

            let thisItem,
                thisText,
                thisX,
                thisY,
                thisWidth,
                thisHeight,
                options = {};

            // set the font attributes
            if (font_name) {
                thisDoc.font(font_name);
            }
            if (font_size) {
                thisDoc.fontSize(font_size);
            }
            if (font_name) {
                thisDoc.fill(font_color);
            }

            // walk through the items
            for (thisItem of items) {
                // console.log(thisItem);
                thisItem = JSON.parse(thisItem);

                // reset position values
                thisX = thisY = thisWidth = thisHeight = undefined;

                thisText = thisItem[0];
                thisX = thisItem[1];

                // x and y
                if (!thisX) {
                    thisX = thisDoc.x;
                }
                if (thisItem[2]) {
                    thisY = thisItem[2];
                }
                else {
                    thisY = thisDoc.y;
                }

                // width? height? options
                if (thisItem[3]) {
                    options.width = thisItem[3];
                }
                if (thisItem[4]) {
                    options.height = thisItem[4];
                }
                if (line_space) {
                    options.lineGap = line_space;
                }
                if (para_space) {
                    options.paragraphGap = para_space;
                }

                // figure out if block or line
                if (thisItem.length === 3) {
                    // text, x, y - it's a text line
                    thisDoc = zTextLine(thisText, thisX, thisY, '', '', '', thisDoc);
                }
                else if (thisItem.length > 3) {
                    // text, x, y, width - it's a text block
                    thisDoc = zTextBlock(thisText, thisX, thisY, '', '', '', options, thisDoc);
                }
            }

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const theseItems = Anklet.LinkManager.interpolateValue(this.items, workspace);
            let thisDoc = workspace.outputPDF;

            workspace.outputPDF = this.textItems(theseItems, this.font_name, this.font_size, this.font_color, this.line_space, this.para_space, thisDoc);

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line after-5\">",
        "        a shortcut for placing a number of text lines and boxes, with similar font and color attributes.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- a list of text items and references, with x, y, width and height.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- if width is included, a text block is created.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- if width and height are included, a clipped text block is created.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- each item takes the form '[\"content\", x, y, [width], [height]]'.",
        "    </div>",
        "    <div class=\"help-line\">",
        "        -- \"content\" may be a string or a workspace reference. all dimensions are in points.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-items\" for=\"items-input\">items</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"items\" type=\"text\" class=\"form-control\" id=\"items-input\" rows=\"5\">{{arrayText items}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "{{{additionalFontList}}}",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        additional space in points between each line and paragraph, for text blocks.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_space\" for=\"line_space-input\">line_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_space\" type=\"text\" class=\"form-control\" id=\"line_space-input\" value=\"{{line_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-para_space\" for=\"para_space-input\">para_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"para_space\" type=\"text\" class=\"form-control\" id=\"para_space-input\" value=\"{{para_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
    ]
};

module.exports.pdf_text_items = pdf_text_items;