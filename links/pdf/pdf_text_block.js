/**
 * <h4>Link.pdf.pdf_text_block<hr></h4>
 *
 * <p>Definition for the pdf_text_block link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_text_block
 * @version 0.1
 * @type {{name: string, fullname: string, text: string, x: string, y: string, width: string, height: string, font_name: string, font_size: string, font_color: string, line_space: string, para_space: string, customList: string[], textBlock: Link.pdf.pdf_text_block.textBlock, render: Link.pdf.pdf_text_block.render, template: string[]}}
 */

const pdf_text_block = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_text_block',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_text_block',

    /**
     * <h4>text<hr></h4>
     */
    text: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>width<hr></h4>
     */
    width: '',

    /**
     * <h4>height<hr></h4>
     */
    height: '',

    /**
     * <h4>font_name<hr></h4>
     */
    font_name: '',

    /**
     * <h4>font_size<hr></h4>
     */
    font_size: '',

    /**
     * <h4>font_color<hr></h4>
     */
    font_color: '',

    /**
     * <h4>line_space<hr></h4>
     */
    line_space: '',

    /**
     * <h4>para_space<hr></h4>
     */
    para_space: '',

    /**
     * <h4>additionalFontList<hr></h4>
     */
    additionalFontList: Anklet.fontList,

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'text',
        'x',
        'y',
        'width',
        'height',
        'font_name',
        'font_size',
        'font_color',
        'line_space',
        'para_space',
        'additionalFontList',
        ''
    ],

    /**
     * <h4>textBlock()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zTextBlock = Anklet.LinkManager.zombifyThis('pdf.pdf_text_block', 'textBlock');</p>
     *
     * @param text
     * @param x
     * @param y
     * @param font_name
     * @param font_size
     * @param font_color
     * @param thisDoc
     * @return {*}
     * @param options
     */
    textBlock: function(text, x, y, font_name, font_size, font_color, options, thisDoc) {

        try {
            let spacing;

            // set the font attributes
            if (font_name) {
                thisDoc.font(font_name);
            }
            if (font_size) {
                thisDoc.fontSize(font_size);
            }
            if (font_name) {
                thisDoc.fill(font_color);
            }

            // set spacing options
            // if (!width) {
            //     throw new Error('text block requested with no width parameter');
            // }
            // else {
            //     options.width = parseFloat(width);
            // }
            // if (height) {
            //     options.height = parseFloat(height);
            // }
            // if (thisDoc.thisLineGap) {
            //     options.lineGap = thisDoc.thisLineGap;
            // }
            // if (thisDoc.thisParagraphGap) {
            //     options.paragraphGap = thisDoc.thisParagraphGap;
            // }

            // parseFloats - if x and y are not supplied, just use thisDoc.x and thisDoc.y
            if (!x) {
                x = thisDoc.x;
            }
            else if (typeof x === 'string' && (x.charAt(0) === ' ' || x.charAt(0) === '+' || x.charAt(0) === '-')) {
                // add (or remove the space from the current point)
                spacing = parseFloat(x);
                x = thisDoc.x + spacing;
            }
            else {
                // ensure it's a number
                x = parseFloat(x);
            }
            if (!y) {
                y = thisDoc.y;
            }
            else if (typeof y === 'string' && (y.charAt(0) === ' ' || y.charAt(0) === '+' || y.charAt(0) === '-')) {
                // add (or remove the space from the current point)
                spacing = parseFloat(y);
                y = thisDoc.y + spacing;
            }
            else {
                y = parseFloat(y);
            }

            // set it
            thisDoc.text(text, x, y, options);

            return thisDoc;
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>render()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            // in the case of a pdfkit PDF doc, the workspace.outputPDF references the doc object
            let thisDoc = workspace.outputPDF,
                thisLine = Anklet.LinkManager.interpolateValue(this.text, workspace),
                options = {};

            // in case it came through as an array, flatten it
            if (Array.isArray(thisLine)) {
                thisLine = thisLine.join('\n');
            }

            // figure out the options
            options.width = parseFloat(this.width);
            options.align = 'left';
            if (this.height) {
                options.height = parseFloat(this.height);
            }
            if (this.line_space) {
                options.lineGap = this.line_space;
            }
            if (this.para_space) {
                options.paragraphGap = this.para_space;
            }

            workspace.outputPDF = this.textBlock(thisLine, this.x, this.y, this.font_name, this.font_size, this.font_color, options, thisDoc);

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        a block of text, to fit within a box.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-text\" for=\"text-input\">text</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"text\" type=\"text\" class=\"form-control\" id=\"text-input\" rows=\"5\">{{arrayText text}}</textarea>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "       x and y origin and width amd height, in points. if height is supplied, text is clipped o that height.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "{{{additionalFontList}}}",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        additional space in points between each line and paragraph.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_space\" for=\"line_space-input\">line_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_space\" type=\"text\" class=\"form-control\" id=\"line_space-input\" value=\"{{line_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-para_space\" for=\"para_space-input\">para_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"para_space\" type=\"text\" class=\"form-control\" id=\"para_space-input\" value=\"{{para_space}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.pdf_text_block = pdf_text_block;