/**
 * <h4>Link.pdf.pdf_bullet<hr></h4>
 *
 * <p>Definition for the pdf_bullet link module</p>
 *
 * <p>@TODO Please unstub this link module.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.pdf.pdf_bullet
 * @version 0.1
 */

const pdf_bullet = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'pdf_bullet',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.pdf.pdf_bullet',

    /**
     * <h4>text<hr></h4>
     */
    text: '',

    /**
     * <h4>x<hr></h4>
     */
    x: '',

    /**
     * <h4>y<hr></h4>
     */
    y: '',

    /**
     * <h4>width<hr></h4>
     */
    width: '',

    /**
     * <h4>height<hr></h4>
     */
    height: '',

    /**
     * <h4>indent<hr></h4>
     */
    indent: '',

    /**
     * <h4>bullet<hr></h4>
     */
    bullet: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'text',
        'x',
        'y',
        'width',
        'height',
        'indent',
        'bullet',
        ''
    ],

    /**
     * <h4>bulletItem()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zBulletItem = Anklet.LinkManager.zombifyThis('pdf.pdf_bullet', 'bulletItem');</p>
     *
     * @param workspace
     */
    bulletItem: function(text, x, y, width, height, indent, bullet) {

        try {
            return [
                '**',
                'This is a stub for pdf.pdf_bullet.placeImage(). Please finish this stubbed function before completion.',
                '**',
                ''
            ].join('\n');
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            workspace.output[this.id] = this.bulletItem(this.text, this.x, this.y, this.width, this.height, this.indent, this.bullet);
            workspace.output[this.id] += [
                '**',
                'This is a stub for pdf.pdf_bullet.fetch(). Please finish this stubbed function before completion.',
                '**'
            ].join('\n');
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-text\" for=\"text-input\">text</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"text\" type=\"text\" class=\"form-control\" id=\"text-input\" value=\"{{text}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        values in points.",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-x\" for=\"x-input\">x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"x\" type=\"text\" class=\"form-control\" id=\"x-input\" value=\"{{x}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-y\" for=\"y-input\">y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"y\" type=\"text\" class=\"form-control\" id=\"y-input\" value=\"{{y}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-indent\" for=\"indent-input\">indent</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"indent\" type=\"text\" class=\"form-control\" id=\"indent-input\" value=\"{{indent}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet\" for=\"bullet-select-input\">bullet</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-bullet-2\" for=\"bullet-select-input\">",
        "            <select aria-describedby=\"ada-bullet-select-describedby\" aria-labelledby=\"ada-bullet-select-describedby\" name=\"bullet\" class=\"form-control\" id=\"bullet-select-input\">",
        "                <option value=\"disc\">disc</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"disc\">disc</option>",
        "                <option value=\"circle\">circle</option>",
        "                <option value=\"block\">block</option>",
        "                <option value=\"box\">box</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.pdf_bullet = pdf_bullet;