/**
 * <h4>Link.ps.pdf_to_image<hr></h4>
 *
 * <p>Definition for the pdf_to_image link module</p>
 *
 * <p>Closes the open postscript stream, calls the render engine, and emits the result.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.pdf_to_image
 * @version 0.1
 * @type {{name: string, fullname: string, runGS: (function(*=, *): Buffer), render: Link.ps.pdf_to_image.render}}
 */

let pdf_to_image = {

    /**
     * <h4>name<hr></h4>
     * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
     * @type {string}
     */
    name: 'pdf_to_image',

    /**
     * <h4>fullname<hr></h4>
     * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
     * @type {string}
     */
    fullname: 'Anklet.links.ps.pdf_to_image',

    /**
     * <h4>outputName<hr></h4>
     */
    outputName: '',

    /**
     * <h4>outputType<hr></h4>
     */
    outputType: '',

    /**
     * <h4>width<hr></h4>
     */
    height: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'outputType',
        'outputName',
        'width',
        'height'
    ],


    /**
     * <h4>runGS()<hr></h4>
     * <p>runs the GhostScript PS interpreter as a child process and returns its output as a Buffer</p>
     *
     * @param thisPDF
     * @param thisType
     * @param width
     * @param height
     * @returns {Buffer}
     */
    runGS: function(thisPDF, thisType, width, height) {

        const { spawnSync } = require('child_process'),
            fs = require('fs');

        let args,
            options = {
                input: thisPDF,
                stdio: 'pipe'
            },
            thisOutput,
            thisBuffer,
            thisErrorOut,
            thisWriter = 'png16m';

        if (thisType === 'jpg') {
            thisWriter = 'jpeg';
        }

        args = [
            '-dSAFER',
            '-dBATCH',
            '-dNOPAUSE',
            '-sDEVICE=' + thisWriter,
            '-dDEVICEWIDTHPOINTS=' + width,
            '-dDEVICEHEIGHTPOINTS=' + height,
            '-dFIXEDMEDIA',
            '-dFIXEDRESOLUTION',
            '-dUseCropBox',
            '-dTextAlphaBits=4',
            '-dGraphicsAlphaBits=4',
            '-sOutputFile=-',
            '-q',
            '-_'
        ];

        thisOutput = spawnSync('gs', args, options);
        thisBuffer = new Buffer( new Uint8Array(thisOutput.output[1]) );
        thisErrorOut = new Buffer( new Uint8Array(thisOutput.output[2]) );
        if (thisErrorOut.length > 0) {
            console.error(thisErrorOut.toString());
        }

        return thisBuffer;
    },

    /**
     * <h4>render()<hr></h4>
     * <p>compiles the various ps streams in order, and gets ghostscript to run them</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            let contentType,
                outputImage;

            return new Promise(function (resolve, reject) {
                let thisDoc = workspace.outputPDF,
                    PDFparts = [],
                    pdfData;

                // thisDoc.page;

                thisDoc.on('data', (data) => {
                    PDFparts.push(data);
                });
                thisDoc.on('end', () => {
                    pdfData = Buffer.concat(PDFparts);
                    resolve(pdfData);
                });
                thisDoc.end();
            })
            .then((pdfData) => {
                console.log(pdfData);
                // get the output image and resolve
                outputImage = this.runGS(pdfData, this.outputType, this.width, this.height);
                return outputImage;
            })
            .then((outputImage) => {
                // set the content-type
                if (this.outputType === 'jpg') {
                    contentType = 'image/jpeg'
                }
                else {
                    contentType = 'image/png'
                }
                // pass the image along to output
                workspace.outputStream = outputImage;
                workspace.outputHeaders = [
                    'Content-Type: ' + contentType,
                    'Content-Disposition: inline; filename="' + this.outputName + '.' + this.outputType +'"'
                ];
            });

        }
        catch(error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-outputName\" for=\"outputName-input\">outputName</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <input name=\"outputName\" type=\"text\" class=\"form-control\" id=\"outputName-input\" value=\"{{outputName}}\">",
        "            </label>",
        "        </div>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <select aria-describedby=\"ada-outputType-select-describedby\" aria-labelledby=\"ada-outputType-select-describedby\" name=\"outputType\" class=\"form-control\" id=\"outputType-select-input\">",
        "                <option value=\"{{outputType}}\">.{{outputType}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"jpg\">.jpg</option>",
        "                <option value=\"png\">.png</option>",
        "            </select>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-height\" for=\"height-input\">height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"height\" type=\"text\" class=\"form-control\" id=\"height-input\" value=\"{{height}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        ""
    ]

};

module.exports.pdf_to_image = pdf_to_image;

