/**
 * <h4>Link.get_file<hr></h4>
 *
 * <p>Definition for the get_file link module</p>
 *
 * <p>Gets a file from the local file system, then stores it.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.fs.get_file
 * @version 0.1
 * @type {{name: string, fullname: string, fetch: Link.fs.get_file.fetch, render: Link.fs.get_file.render}}
 */

let get_file = {

	/**
	 * <h4>name<hr></h4>
	 * @type {string}
	 */
	name: 'get_file',

	/**
	 * <h4>fullname<hr></h4>
	 * @type {string}
	 */
	fullname: 'Anklet.links.fs.get_file',

	/**
	 * <h4>filename<hr></h4>
	 * @type {string}
	 */
	filename: '#file',

	/**
	 * <h4>target<hr></h4>
	 * @type {string}
	 */
	target: 'file',

    /**
     * <h4>fetch()<hr></h4>
     * <p>overrides the core method</p>
     *
     * @param workspace
     */
	fetch: function(workspace) {

        try {
            const path = require('path'),
                fs = require('fs'),
                hula_root = Anklet.Config.constants.hula_root;
            let fetchFile,
                filePath,
                fileData,
                fileObj;

            // retrieve the target from the pathTable
            fetchFile = this.filename;
            // figure out its path
            filePath = path.join(hula_root, Anklet.Config.constants.baseURL, fetchFile);

            // get the chain json file
            fileData = fs.readFileSync(filePath, 'utf8');

            // was there a read error?
            if (typeof(fileData) === 'error') {
                console.error(fileData);
                throw (fileData);
            }

            // otherwise, parse the chain
            fileObj = JSON.parse(fileData);

            // was there a parse error?
            if (typeof(fileObj) === 'error') {
                console.error.log(fileObj);
                throw (fileObj);
            }

            // save the return data
            workspace[this.target] = fileObj;

        } catch (error) {
            console.error(error);
            throw error;
        }

	// },
    //
    // /**
     // * <h4>render()<hr></h4>
     // * <p>overrides the core method a little bit</p>
     // *
     // * @param workspace
     // */
	// render: function(workspace) {
    //
     //    try {
     //        this.output = this.workspace.file;
     //    } catch (error) {
     //        console.error(error);
     //        throw error;
     //    }
    //
    }


};

module.exports.get_file = get_file;

