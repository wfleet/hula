/**
 * <h4>Link.net.get_image<hr></h4>
 *
 * <p>Definition for the get_image link module</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.net.get_image
 * @version 0.1
 */

const get_image = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'get_image',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.net.get_image',

    /**
     * <h4>url<hr></h4>
     */
    url: '',

    /**
     * <h4>target<hr></h4>
     */
    target: '',

    /**
     * <h4>customList<hr></h4>
     */
    customList: [
        'url',
        'target',
        ''
    ],

    /**
     * <h4>getImage()<hr></h4>
     * <p>for external (zombie) call, use:</p>
     * <p>    zGetImage = Anklet.LinkManager.zombifyThis('net.get_image', 'getImage');</p>
     *
     * @param url
     * @param workspace
     * @return {* | PromiseLike<Buffer> | Promise<Buffer>}
     */
    getImage: function(url, workspace) {

        try {
            const requestPromise = require('request-promise-native'),
                thisURI = Anklet.LinkManager.interpolateValue(this.url, workspace),

                // set up the options object for the http image fetch
                theseOptions = {
                    uri: thisURI,
                    method: 'GET',
                    encoding: null,
                    headers: {
                        'Connection': 'keep-alive',
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                        'Upgrade-Insecure-Requests': '1',
                        'Accept': 'image/jpg;base64,image/png;base64',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'en-US,en;q=0.9'
                    }
                };

            // run the GET as a promise
            return requestPromise(theseOptions)
                .then((response) => {
                    // convert to ArrayBuffer for pdfKit
                    // let arrayBuffer = new ArrayBuffer(response.length),
                    //     thisView = new Uint8Array(arrayBuffer),
                    //     i;
                    //
                    // for (i = 0; i < response.length; i++) {
                    //     thisView[i] = response[i];
                    // }

                    // let thisBuffer = new Buffer(response),
                    //     thisArrayBuffer = thisBuffer.buffer.slice(thisBuffer.byteOffset, thisBuffer.byteOffset + thisBuffer.byteLength),
                    //     thisUi32Buffer = new Uint32Array(thisBuffer.buffer, thisBuffer.byteOffset, thisBuffer.byteLength / Uint32Array.BYTES_PER_ELEMENT);

                    return new Buffer(response, 'base64');

                // })
                // .then((response) => {
                //     // handle the response as a buffer-to-image
                //     let thisImage = response;
                //     // let thisImage = new Buffer(response, 'base64');
                //
                //     return thisImage;
                });
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            const requestPromise = require('request-promise-native'),
                thisURI = Anklet.LinkManager.interpolateValue(this.url, workspace),

                // set up the options object for the http image fetch
                theseOptions = {
                    uri: thisURI,
                    method: 'GET',
                    encoding: null,
                    headers: {
                        'Connection': 'keep-alive',
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                        'Upgrade-Insecure-Requests': '1',
                        'Accept': 'image/jpg;base64,image/png;base64',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'en-US,en;q=0.9'
                    }
                };

            // run the GET as a promise
            return requestPromise(theseOptions)
                .then((response) => {
                    workspace[this.target] = new Buffer(response, 'base64');
                })
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-url\" for=\"url-input\">url</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"url\" type=\"text\" class=\"form-control\" id=\"url-input\" value=\"{{url}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
    ]
};

module.exports.get_image = get_image;