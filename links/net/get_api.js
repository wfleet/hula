/**
 * <h4>Link.net.get_api<hr></h4>
 *
 * <p>Definition for the get_api link module</p>
 *
 * <p>Gets a response from a net api call, and stores it.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.net.get_api
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let get_api = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'get-api',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.net.get-api',

    /**
     * hostname, path, port, method, headers
     */
    hostname: '',
    path: '',
    port: '80',
    method: 'GET',
    dataValues: [],
    headers: [],
    protocol: 'HTTP',
    target: 'body',

    /**
     * <h4>fetch()<hr></h4>
     * <p>calls the web resource and stores it in the location in thisTarget. Returns a Promise.</p>
     *
     * @param workspace
     * @returns {Promise.<*>}
     */
    fetch: function(workspace) {

	    const reqProm = require('request-promise-native'),
            thisQuery = workspace.queryData;

	    let thisTarget = this.target,
            thisData = '',
            theseOptions,
            thisHeader,
            splitHeader,
            headerKey,
            headerValue,
            thisProtocol;

	    try {

            // complete the path
            this.path = Anklet.LinkManager.interpolateValue(this.path, workspace);
            this.path = this.path.replace(/%26/g, '&');
            workspace.apiPath = this.protocol.toLowerCase() + '://' + this.hostname + this.path;

            // compile the data
            if (Array.isArray(this.dataValues)) {
                thisData = this.dataValues.join('&');
            }
            if (thisData) {
                thisData = '?' + thisData;
            }

            if (this.protocol == 'HTTPS') {
                this.hostname = 'https://' + this.hostname;
            }
            else {
                this.hostname = 'http://' + this.hostname;
            }

            // set up the options object
            theseOptions = {
                uri: this.hostname + this.path + thisData,
                method: this.method,
                headers: {
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                    'Upgrade-Insecure-Requests': '1',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate',
                    'Accept-Language': 'en-US,en;q=0.9'
                },
                json: true
            };

            if (this.port) {
                theseOptions.port = this.port;
            }

            // push in the additional headers from input, if any
            for (thisHeader of this.headers) {
                if (thisHeader.indexOf(':') > -1) {
                    splitHeader = thisHeader.split(':');
                    headerKey = splitHeader[0].trim();
                    headerValue = splitHeader[1].trim();
                    theseOptions.headers[headerKey] = headerValue;
                }
            }

            return reqProm(theseOptions)
                .then((response) => {
                    if (Anklet.Config.constants.log_responses) {
                        console.log(JSON.stringify(response, null, 4));
                    }
                    workspace[thisTarget] = response;
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        catch(error) {
	        console.error(error);
        }

    },

    /**
     * <h4>getApi()<hr></h4>
     * <p>calls the web resource and stores it in the location in thisTarget. Returns a Promise.</p>
     * <p>for external (zombie) call, use:</p>
     * <p>    zGetApi = Anklet.LinkManager.zombifyThis('net.get-api', 'getApi');</p>
     *
     * @param workspace
     * @returns {Promise.<*>}
     */
    getApi: function(workspace) {

        try {
            return Promise.resolve()
                .then(() => {
                    this.fetch(workspace);
                });
        }
        catch(error) {
            console.error(error);
        }

    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'hostname',
        'path',
        'port',
        'method',
        'dataValues',
        'protocol',
        'headers',
        'target'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-protocol\" for=\"protocol-select-input\">protocol</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-protocol-2\" for=\"protocol-select-input\">",
        "                <select aria-describedby=\"ada-protocol-select-describedby\" aria-labelledby=\"ada-protocol-select-describedby\" name=\"protocol\" class=\"form-control\" id=\"protocol-select-input\">",
        "                    <option value=\"{{protocol}}\">{{protocol}}</option>",
        "                    <option value=\"\">----- Other values -----</option>",
        "                    <option value=\"HTTP\">HTTP</option>",
        "                    <option value=\"HTTPS\">HTTPS</option>",
        "                </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-hostname\" for=\"hostname-input\">hostname</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"hostname\" type=\"text\" class=\"form-control\" id=\"hostname-input\" name=\"hostname\" value=\"{{hostname}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-path\" for=\"path-input\">path</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"path\" type=\"text\" class=\"form-control\" id=\"path-input\" name=\"path\" value=\"{{path}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-port\" for=\"port-input\">port</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"port\" type=\"text\" class=\"form-control\" id=\"port-input\" name=\"port\" value=\"{{port}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-method\" for=\"method-input\">method</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"method\" type=\"text\" class=\"form-control\" id=\"method-input\" name=\"method\" value=\"{{method}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-dataValues\" for=\"dataValues-input\">dataValues</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"dataValues\" type=\"text\" class=\"form-control\" id=\"dataValues-input\" rows=\"5\">{{arrayText dataValues}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-headers\" for=\"headers-input\">headers</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"headers\" type=\"text\" class=\"form-control\" id=\"headers-input\" rows=\"5\">{{arrayText headers}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" name=\"target\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<hr>",
        "<div class=\"col-grid col-1-2-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-object-tree\" for=\"object-tree-input\">test api response</label>",
        "    </div>",
        "    <div class=\"help-line\">",
        "        using GET string in view box below, view the api response in a separate window",
        "    </div>",
        "    <div class=\"align-center link-field\">",
        "        <div class=\"form-group\">",
        "            <input type=\"button\" class=\"form-control flat-button\" style=\"padding: 0\" id=\"object-tree-input\" name=\"object-tree\" value=\"Test\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<script language=\"JavaScript\">",
        "    (function($) {",
        "",
        "        // bind the click to the chain call",
        "        $('#object-tree-input').on('click', function (event) {",
        "            event.preventDefault();",
        "            viewAPI();",
        "        });",
        "",
        "    })(jQuery)",
        "</script>",
        ""
    ]



};

module.exports.get_api = get_api;

