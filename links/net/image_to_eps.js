/**
 * <h4>Link.net.image_to_eps<hr></h4>
 *
 * <p>Definition for the image_to_eps link module</p>
 *
 * <p>Calls for a net file, then stores it.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.net.image_to_eps
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let image_to_eps = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'image_to_eps',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.net.image_to_eps',

    image_uri: '',
    image_type: 'jpg',
    target: '',

    /**
     * <h4>fetch()<hr></h4>
     * <p>gets the file at the uri supplied, and converts it to EPS format through ImageMagick</p>
     *
     * @param workspace
     */
    fetch: function(workspace) {

        // set up a bunch of resources
        const { URL } = require('url'),
            reqProm = require('request-promise-native'),
            { spawnSync } = require('child_process'),
            thisTarget = Anklet.LinkManager.interpolateValue(this.target, workspace),
            thisURI = Anklet.LinkManager.interpolateValue(this.image_uri, workspace),

            // set up the options object for the http image fetch
            theseOptions = {
                uri: thisURI,
                method: 'GET',
                encoding: null,
                headers: {
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
                    'Upgrade-Insecure-Requests': '1',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate',
                    'Accept-Language': 'en-US,en;q=0.9'
                }
            };

        // set up the args for the imageMagick spawn call
        let args = [
                'convert',
                this.image_type + ':fd:0',
                'eps:fd:1'
            ],
            thisImage;

        return reqProm(theseOptions)
            .then((response) => {

                // set up the spawn of imageMagick
                let args = [
                        // 'convert',
                        this.image_type + ':fd:0',
                        'eps:fd:1'
                    ],
                    thisImage = new Buffer( new Uint8Array(response) ),
                    epsOptions = {
                        stdio: 'pipe',
                        input: thisImage
                    },
                    thisOutput,
                    thisBuffer,
                    thisErrorOut;

                // run imageMagick and collect its output
                thisOutput = spawnSync('convert', args, epsOptions);
                thisBuffer = new Buffer( new Uint8Array(thisOutput.output[1]) );
                thisErrorOut = new Buffer( new Uint8Array(thisOutput.output[2]) );
                if (thisErrorOut.length > 0) {
                    console.error(thisErrorOut.toString());
                }

                // set the return
                workspace[thisTarget] = thisBuffer.toString();
                return workspace;

            })
            .catch(function(error) {
                console.error(error.message);
                console.error(error.stack);
            });

    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'image_uri',
        'target',
        'image_type'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-image_uri\" for=\"image_uri-input\">image_uri</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"image_uri\" type=\"text\" class=\"form-control\" id=\"image_uri-input\" value=\"{{image_uri}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target\" for=\"target-input\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-image_type\" for=\"image_type-input\">image_type</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group dropdown\">",
        "            <label id=\"label-image_type-2\" for=\"image_type-select-input\">",
        "            <select aria-describedby=\"ada-image_type-select-describedby\" aria-labelledby=\"ada-image_type-select-describedby\" name=\"image_type\" class=\"form-control\" id=\"image_type-select-input\">",
        "                <option value=\"{{image_type}}\">{{image_type}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"jpg\">jpg</option>",
        "                <option value=\"png\">png</option>",
        "                <option value=\"gif\">gif</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]


};

module.exports.image_to_eps = image_to_eps;

