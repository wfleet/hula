/**
 * <h4>Link.catalog.product<hr></h4>
 *
 * <p>Definition for the product link module</p>
 *
 * <p>Calls the CatalogServices/product api to return a single or multiple product response.</p>
 * <p>Zombifies /net/get_api to do so.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.catalog.product
 * @version 0.1
 * @type {{name: string, fullname: string, hostname: string, target: string, query: string, storeNumber: string, showExpandedFields: string, employee: string, maxResults: string, offset: string, sortMethod: string, priceFlag: string, refinement: string, showRefinements: string, showRelatedCats: string, showJumpNav: string, showProdLocations: string, showSpecs: string, showReviews: string, showRelatedItems: string, showRequiredItems: string, showEpp: string, showMarketingBullets: string, showQA: string, showGuides: string, showCustomerAlsoViewed: string, showRestrictions: string, inStock: string, showEpc: string, showRomanceCopy: string, showURL: string, rollUpVariants: string, showVariants: string, channelType: string, customList: string[], fetch: Link.catalog.product.fetch, template: string[]}}
 */

let product = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'product',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.catalog.product',

    /**
     * <h4>hostname<hr></h4>
     */
    hostname: 'm.lowes.com',

    /**
     * <h4>query<hr></h4>
     */
    target: 'product',

    /**
     * <h4>query<hr></h4>
     */
    query: '',

    /**
     * <h4>storeNumber<hr></h4>
     */
    storeNumber: '',

    /**
     * <h4>showExpandedFields<hr></h4>
     */
    showExpandedFields: '',

    /**
     * <h4>employee<hr></h4>
     */
    employee: '',

    /**
     * <h4>maxResults<hr></h4>
     */
    maxResults: '',

    /**
     * <h4>offset<hr></h4>
     */
    offset: '',

    /**
     * <h4>sortMethod<hr></h4>
     */
    sortMethod: '',

    /**
     * <h4>priceFlag<hr></h4>
     */
    priceFlag: '',

    /**
     * <h4>refinement<hr></h4>
     */
    refinement: '',

    /**
     * <h4>showRefinements<hr></h4>
     */
    showRefinements: '',

    /**
     * <h4>showRelatedCats<hr></h4>
     */
    showRelatedCats: '',

    /**
     * <h4>showJumpNav<hr></h4>
     */
    showJumpNav: '',

    /**
     * <h4>showProdLocations<hr></h4>
     */
    showProdLocations: '',

    /**
     * <h4>showSpecs<hr></h4>
     */
    showSpecs: '',

    /**
     * <h4>showReviews<hr></h4>
     */
    showReviews: '',

    /**
     * <h4>showRelatedItems<hr></h4>
     */
    showRelatedItems: '',

    /**
     * <h4>showRequiredItems<hr></h4>
     */
    showRequiredItems: '',

    /**
     * <h4>showEpp<hr></h4>
     */
    showEpp: '',

    /**
     * <h4>showMarketingBullets<hr></h4>
     */
    showMarketingBullets: '',

    /**
     * <h4>showQA<hr></h4>
     */
    showQA: '',

    /**
     * <h4>showGuides<hr></h4>
     */
    showGuides: '',

    /**
     * <h4>showCustomerAlsoViewed<hr></h4>
     */
    showCustomerAlsoViewed: '',

    /**
     * <h4>showRestrictions<hr></h4>
     */
    showRestrictions: '',

    /**
     * <h4>inStock<hr></h4>
     */
    inStock: '',

    /**
     * <h4>showEpc<hr></h4>
     */
    showEpc: '',

    /**
     * <h4>showRomanceCopy<hr></h4>
     */
    showRomanceCopy: '',

    /**
     * <h4>showURL<hr></h4>
     */
    showURL: '',

    /**
     * <h4>rollUpVariants<hr></h4>
     */
    rollUpVariants: '',

    /**
     * <h4>showVariants<hr></h4>
     */
    showVariants: '',

    /**
     * <h4>channelType<hr></h4>
     */
    channelType: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'hostname',
        'target',
        'query',
        'storeNumber',
        'showExpandedFields',
        'maxResults',
        'offset',
        'employee',
        'sortMethod',
        'priceFlag',
        'refinement',
        'showRefinements',
        'showRelatedCats',
        'showJumpNav',
        'showProdLocations',
        'showSpecs',
        'showReviews',
        'showRelatedItems',
        'showRequiredItems',
        'showEpp',
        'showMarketingBullets',
        'showQA',
        'showGuides',
        'showCustomerAlsoViewed',
        'showRestrictions',
        'inStock',
        'showEpc',
        'showRomanceCopy',
        'showURL',
        'rollUpVariants',
        'showVariants',
        'channelType',
        ''
    ],

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            let thisAPILink,
                thisData,
                theseValues = [],
                queryNumber;

            // set some defaults
            thisData = {
                "link": "net.get_api",
                "hostname": "m.lowes.com",
                "path": "/CatalogServices/product/productid/{{queryData.productId}}/v2_0",
                "port": "",
                "method": "GET",
                "headers": "",
                "target": "product",
                "dataValues": [],
                "protocol": "HTTPS"
            };

            // push in the local values
            thisData.hostname = this.hostname;
            thisData.target = this.target;
            switch (this.query) {
                case 'productid':
                    thisData.path = '/CatalogServices/product/productid/v2_0';
                    break;
                case 'productid_single':
                    queryNumber = workspace.queryData.productId;
                    thisData.path = '/CatalogServices/product/productid/' + queryNumber + '/v2_0';
                    break;
                case 'barcode':
                    queryNumber = workspace.queryData.barcode;
                    thisData.path = '/CatalogServices/product/barcode/' + queryNumber + '/v2_0';
                    break;
                case 'barcode_single':
                    thisData.path = '/CatalogServices/product/barcode/v2_0';
                    break;
                case 'keyword':
                    thisData.path = '/CatalogServices/product/keyword/v2_0';
                    break;
                case 'nvalue':
                    thisData.path = '/CatalogServices/product/nvalue/v2_0';
                    break;
                case 'itemnumber':
                    thisData.path = '/CatalogServices/product/itemnumber/v2_0';
                    break;
                case 'modelid':
                    thisData.path = '/CatalogServices/product/modelid/v2_0';
            }

            // get all the data values that are actually set
            if (workspace.queryData['productId']) { theseValues.push('productId=' + workspace.queryData['productId']) }
            if (Anklet.LinkManager.isTruthy(this.storeNumber)) { theseValues.push('storeNumber=' + this.storeNumber) }
            if (Anklet.LinkManager.isTruthy(this.showExpandedFields)) { theseValues.push('showExpandedFields=' + this.showExpandedFields) }
            if (Anklet.LinkManager.isTruthy(this.maxResults)) { theseValues.push('maxResults=' + this.maxResults) }
            if (Anklet.LinkManager.isTruthy(this.offset)) { theseValues.push('offset=' + this.offset) }
            if (Anklet.LinkManager.isTruthy(this.employee)) { theseValues.push('employee=' + this.employee) }
            if (Anklet.LinkManager.isTruthy(this.sortMethod)) { theseValues.push('sortMethod=' + this.sortMethod) }
            if (Anklet.LinkManager.isTruthy(this.priceFlag)) { theseValues.push('priceFlag=' + this.priceFlag) }
            if (Anklet.LinkManager.isTruthy(this.refinement)) { theseValues.push('refinement=' + this.refinement) }
            if (Anklet.LinkManager.isTruthy(this.showRefinements)) { theseValues.push('showRefinements=' + this.showRefinements) }
            if (Anklet.LinkManager.isTruthy(this.showRelatedCats)) { theseValues.push('showRelatedCats=' + this.showRelatedCats) }
            if (Anklet.LinkManager.isTruthy(this.showJumpNav)) { theseValues.push('showJumpNav=' + this.showJumpNav) }
            if (Anklet.LinkManager.isTruthy(this.showProdLocations)) { theseValues.push('showProdLocations=' + this.showProdLocations) }
            if (Anklet.LinkManager.isTruthy(this.showSpecs)) { theseValues.push('showSpecs=' + this.showSpecs) }
            if (Anklet.LinkManager.isTruthy(this.showReviews)) { theseValues.push('showReviews=' + this.showReviews) }
            if (Anklet.LinkManager.isTruthy(this.showRelatedItems)) { theseValues.push('showRelatedItems=' + this.showRelatedItems) }
            if (Anklet.LinkManager.isTruthy(this.showRequiredItems)) { theseValues.push('showRequiredItems=' + this.showRequiredItems) }
            if (Anklet.LinkManager.isTruthy(this.showEpp)) { theseValues.push('showEpp=' + this.showEpp) }
            if (Anklet.LinkManager.isTruthy(this.showMarketingBullets)) { theseValues.push('showMarketingBullets=' + this.showMarketingBullets) }
            if (Anklet.LinkManager.isTruthy(this.showQA)) { theseValues.push('showQA=' + this.showQA) }
            if (Anklet.LinkManager.isTruthy(this.showGuides)) { theseValues.push('showGuides=' + this.showGuides) }
            if (Anklet.LinkManager.isTruthy(this.showCustomerAlsoViewed)) { theseValues.push('showCustomerAlsoViewed=' + this.showCustomerAlsoViewed) }
            if (Anklet.LinkManager.isTruthy(this.showRestrictions)) { theseValues.push('showRestrictions=' + this.showRestrictions) }
            if (Anklet.LinkManager.isTruthy(this.inStock)) { theseValues.push('inStock=' + this.inStock) }
            if (Anklet.LinkManager.isTruthy(this.showEpc)) { theseValues.push('showEpc=' + this.showEpc) }
            if (Anklet.LinkManager.isTruthy(this.showRomanceCopy)) { theseValues.push('showRomanceCopy=' + this.showRomanceCopy) }
            if (Anklet.LinkManager.isTruthy(this.showURL)) { theseValues.push('showURL=' + this.showURL) }
            if (Anklet.LinkManager.isTruthy(this.rollUpVariants)) { theseValues.push('rollUpVariants=' + this.rollUpVariants) }
            if (Anklet.LinkManager.isTruthy(this.showVariants)) { theseValues.push('showVariants=' + this.showVariants) }
            if (Anklet.LinkManager.isTruthy(this.channelType)) { theseValues.push('channelType=' + this.channelType) }

            // push them into the data object
            thisData.dataValues = theseValues;

            // zombify the get_api link
            thisAPILink = Anklet.LinkManager.loadLink(thisData);

            // call it and return
            return thisAPILink.fetch(workspace);

        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * @type {string[]}
     */
    template: [
        "",
        "<style>.col-1-1-1-1 {grid-template-columns: 150px .6fr 1fr .6fr;}</style>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-hostname\" for=\"hostname-input\">hostname</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"hostname\" type=\"text\" class=\"form-control\" id=\"hostname-input\" value=\"{{hostname}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-target-checkbox-1\" for=\"target-checkbox\">target</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"target\" type=\"text\" class=\"form-control\" id=\"target-input\" value=\"{{target}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-query\" for=\"query-select-input\">query</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-query-2\" for=\"query-select-input\">",
        "            <select aria-describedby=\"ada-query-select-describedby\" aria-labelledby=\"ada-query-select-describedby\" name=\"query\" class=\"form-control\" id=\"query-select-input\">",
        "                <option value=\"{{query}}\">{{query}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"keyword\">keyword</option>",
        "                <option value=\"nvalue\">nvalue</option>",
        "                <option value=\"itemnumber\">itemnumber</option>",
        "                <option value=\"barcode_single\">barcode_single</option>",
        "                <option value=\"productid\">productid</option>",
        "                <option value=\"productid_single\">productid_single</option>",
        "                <option value=\"modelid\">modelid</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-storeNumber\" for=\"storeNumber-input\">storeNumber</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"storeNumber\" type=\"text\" class=\"form-control\" id=\"storeNumber-input\" value=\"{{storeNumber}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showExpandedFields-checkbox-1\" for=\"showExpandedFields-checkbox\">showExpandedFields</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showExpandedFields-checkbox-2\" for=\"showExpandedFields-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showExpandedFields-checkbox\" name=\"showExpandedFields\"{{#isEqual showExpandedFields 1}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}> </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-maxResults\" for=\"maxResults-input\">maxResults</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"maxResults\" type=\"text\" class=\"form-control\" id=\"maxResults-input\" value=\"{{maxResults}}\">",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-offset\" for=\"offset-input\">offset</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"offset\" type=\"text\" class=\"form-control\" id=\"offset-input\" value=\"{{offset}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-employee-checkbox-1\" for=\"employee-checkbox\">employee</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-employee-checkbox-2\" for=\"employee-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"employee-checkbox\" name=\"employee\"{{#isEqual employee 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-sortMethod\" for=\"sortMethod-select-input\">sortMethod</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-sortMethod-2\" for=\"sortMethod-select-input\">",
        "            <select aria-describedby=\"ada-sortMethod-select-describedby\" aria-labelledby=\"ada-sortMethod-select-describedby\" name=\"sortMethod\" class=\"form-control\" id=\"sortMethod-select-input\">",
        "                <option value=\"{{sortMethod}}\">{{sortMethod}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"sortBy_bestSellers\">sortBy_bestSellers</option>",
        "                <option value=\"sortBy_priceLowToHigh\">sortBy_priceLowToHigh</option>",
        "                <option value=\"sortBy_priceHighToLow\">sortBy_priceHighToLow</option>",
        "                <option value=\"sortBy_highestRated\">sortBy_highestRated</option>",
        "                <option value=\"sortBy_brand\">sortBy_brand</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-priceFlag\" for=\"priceFlag-select-input\">priceFlag</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-priceFlag-2\" for=\"priceFlag-select-input\">",
        "            <select aria-describedby=\"ada-priceFlag-select-describedby\" aria-labelledby=\"ada-priceFlag-select-describedby\" name=\"priceFlag\" class=\"form-control\" id=\"priceFlag-select-input\">",
        "                <option value=\"{{priceFlag}}\">{{priceFlag}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"status\">status</option>",
        "                <option value=\"balance\">balance</option>",
        "                <option value=\"rangeStatus\">rangeStatus</option>",
        "                <option value=\"rangeBalance\">rangeBalance</option>",
        "                <option value=\"enhancedStatus\">enhancedStatus</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-refinement\" for=\"refinement-input\">refinement</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"refinement\" type=\"text\" class=\"form-control\" id=\"refinement-input\" value=\"{{refinement}}\">",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "<div class=\"before-10 after-10\">",
        "    <hr />",
        "</div>",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRefinements-checkbox-1\" for=\"showRefinements-checkbox\">showRefinements</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRefinements-checkbox-2\" for=\"showRefinements-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRefinements-checkbox\" name=\"showRefinements\"{{#isEqual showRefinements '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRelatedCats-checkbox-1\" for=\"showRelatedCats-checkbox\">showRelatedCats</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRelatedCats-checkbox-2\" for=\"showRelatedCats-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRelatedCats-checkbox\" name=\"showRelatedCats\"{{#isEqual showRelatedCats '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showJumpNav-checkbox-1\" for=\"showJumpNav-checkbox\">showJumpNav</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showJumpNav-checkbox-2\" for=\"showJumpNav-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showJumpNav-checkbox\" name=\"showJumpNav\"{{#isEqual showJumpNav '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showProdLocations-checkbox-1\" for=\"showProdLocations-checkbox\">showProdLocations</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showProdLocations-checkbox-2\" for=\"showProdLocations-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showProdLocations-checkbox\" name=\"showProdLocations\"{{#isEqual showProdLocations '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showSpecs-checkbox-1\" for=\"showSpecs-checkbox\">showSpecs</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showSpecs-checkbox-2\" for=\"showSpecs-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showSpecs-checkbox\" name=\"showSpecs\"{{#isEqual showSpecs '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showReviews-checkbox-1\" for=\"showReviews-checkbox\">showReviews</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showReviews-checkbox-2\" for=\"showReviews-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showReviews-checkbox\" name=\"showReviews\"{{#isEqual showReviews '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRelatedItems-checkbox-1\" for=\"showRelatedItems-checkbox\">showRelatedItems</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRelatedItems-checkbox-2\" for=\"showRelatedItems-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRelatedItems-checkbox\" name=\"showRelatedItems\"{{#isEqual showRelatedItems '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRequiredItems-checkbox-1\" for=\"showRequiredItems-checkbox\">showRequiredItems</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRequiredItems-checkbox-2\" for=\"showRequiredItems-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRequiredItems-checkbox\" name=\"showRequiredItems\"{{#isEqual showRequiredItems '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showEpp-checkbox-1\" for=\"showEpp-checkbox\">showEpp</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showEpp-checkbox-2\" for=\"showEpp-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showEpp-checkbox\" name=\"showEpp\"{{#isEqual showEpp 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showMarketingBullets-checkbox-1\" for=\"showMarketingBullets-checkbox\">showMarketingBullets</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showMarketingBullets-checkbox-2\" for=\"showMarketingBullets-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showMarketingBullets-checkbox\" name=\"showMarketingBullets\"{{#isEqual showMarketingBullets 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showQA-checkbox-1\" for=\"showQA-checkbox\">showQA</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showQA-checkbox-2\" for=\"showQA-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showQA-checkbox\" name=\"showQA\"{{#isEqual showQA '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showGuides-checkbox-1\" for=\"showGuides-checkbox\">showGuides</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showGuides-checkbox-2\" for=\"showGuides-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showGuides-checkbox\" name=\"showGuides\"{{#isEqual showGuides '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showCustomerAlsoViewed-checkbox-1\" for=\"showCustomerAlsoViewed-checkbox\">showCustomerAlsoViewed</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showCustomerAlsoViewed-checkbox-2\" for=\"showCustomerAlsoViewed-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showCustomerAlsoViewed-checkbox\" name=\"showCustomerAlsoViewed\"{{#isEqual showCustomerAlsoViewed '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRestrictions-checkbox-1\" for=\"showRestrictions-checkbox\">showRestrictions</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRestrictions-checkbox-2\" for=\"showRestrictions-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRestrictions-checkbox\" name=\"showRestrictions\"{{#isEqual showRestrictions '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-inStock-checkbox-1\" for=\"inStock-checkbox\">inStock</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-inStock-checkbox-2\" for=\"inStock-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"inStock-checkbox\" name=\"inStock\"{{#isEqual inStock 'true'}} value=\"true\" checked=\"checked\"{{else}} value=\"false\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showEpc-checkbox-1\" for=\"showEpc-checkbox\">showEpc</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showEpc-checkbox-2\" for=\"showEpc-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showEpc-checkbox\" name=\"showEpc\"{{#isEqual showEpc '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1 short-line\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showRomanceCopy-checkbox-1\" for=\"showRomanceCopy-checkbox\">showRomanceCopy</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showRomanceCopy-checkbox-2\" for=\"showRomanceCopy-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showRomanceCopy-checkbox\" name=\"showRomanceCopy\"{{#isEqual showRomanceCopy '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showURL-checkbox-1\" for=\"showURL-checkbox\">showURL</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showURL-checkbox-2\" for=\"showURL-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showURL-checkbox\" name=\"showURL\"{{#isEqual showURL '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-1-1-1\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-rollUpVariants-checkbox-1\" for=\"rollUpVariants-checkbox\">rollUpVariants</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-rollUpVariants-checkbox-2\" for=\"rollUpVariants-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"rollUpVariants-checkbox\" name=\"rollUpVariants\"{{#isEqual rollUpVariants '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-showVariants-checkbox-1\" for=\"showVariants-checkbox\">showVariants</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <label id=\"label-showVariants-checkbox-2\" for=\"showVariants-checkbox\">",
        "            <input type=\"checkbox\" class=\"form-control-narrow\" id=\"showVariants-checkbox\" name=\"showVariants\"{{#isEqual showVariants '1'}} value=\"1\" checked=\"checked\"{{else}} value=\"0\"{{/isEqual}}></label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "",
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-channelType\" for=\"channelType-select-input\">channelType</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-channelType-2\" for=\"channelType-select-input\">",
        "            <select aria-describedby=\"ada-channelType-select-describedby\" aria-labelledby=\"ada-channelType-select-describedby\" name=\"channelType\" class=\"form-control\" id=\"channelType-select-input\">",
        "                <option value=\"{{channelType}}\">{{channelType}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"DESKTOP\">DESKTOP</option>",
        "                <option value=\"DESKTOP\">MOW</option>",
        "                <option value=\"DESKTOP\">IPHONE</option>",
        "                <option value=\"DESKTOP\">ANDROID</option>",
        "                <option value=\"DESKTOP\">TABLET</option>",
        "                <option value=\"DESKTOP\">LEGACYMOBILE</option>",
        "                <option value=\"DESKTOP\">VISUALIZATION</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        "<hr>",
        "<div class=\"col-grid col-1-2-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-object-tree\" for=\"object-tree-input\">test api response</label>",
        "    </div>",
        "    <div class=\"help-line\">",
        "        using GET string in view box below, view the api response in a separate window",
        "    </div>",
        "    <div class=\"align-center link-field\">",
        "        <div class=\"form-group\">",
        "            <input type=\"button\" class=\"form-control flat-button\" style=\"padding: 0\" id=\"object-tree-input\" name=\"object-tree\" value=\"Test\">",
        "            <input type=\"hidden\" id=\"path-input\" name=\"path\" value=\"%2FCatalogServices%2Fproduct%2Fproductid%2F%7B%7BqueryData.productId%7D%7D%2Fv2_0\">",
        "            <input type=\"hidden\" id=\"headers-input\" name=\"headers\" value=\"\">",
        "            <input type=\"hidden\" id=\"dataValues-input\" name=\"dataValues\" value=\"\">",
        "        </div>",
        "    </div>",
        "</div>",
        "",
        "<script language=\"JavaScript\">",
        "    (function($) {",
        "",
        "        // bind the click to the chain call",
        "        $('#object-tree-input').on('click', function (event) {",
        "            event.preventDefault();",
        "            viewAPI();",
        "        });",
        "",
        "    })(jQuery)",
        "</script>",
        ""
    ]
};

module.exports.product = product;