/**
 * <h4>Link.catalog.service_status<hr></h4>
 *
 * <p>Definition for the service_status link module</p>
 *
 * <p>param q = query type</p>
 * <p>Valid values are "all", "artifact", or the name of the specific service to be queried.
 * "artifact" displays information about the level of the current deployed code.
 * "all" displays information on all services.</p>
 *
 * @copyright 2018 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.catalog.service_status
 * @version 0.1
 * @type {{name: string, fullname: string, q: string, customList: string[], fetch: Link.catalog.service_status.fetch, template: string[]}}
 */

let service_status = {

    /**
     * <h4>name<hr></h4>
     * @type {string}
     */
    name: 'service_status',

    /**
     * <h4>fullname<hr></h4>
     * @type {string}
     */
    fullname: 'Anklet.links.catalog.service_status',

    /**
     * <h4>q<hr></h4>
     * @type {string}
     */
    q: '',

    /**
     * <h4>customList<hr></h4>
     * @type {string[]}
     */
    customList: [
        'q',
        ''
    ],

    /**
     * <h4>fetch()<hr></h4>
     * <p></p>
     * @param workspace
     */
    fetch: function(workspace) {

        try {
            workspace.output[this.id] = [
                '**',
                'This is a stub for fetch(). Please finish this stubbed function before completion.',
                '**'
            ].join('\n');
        }
        catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     *
     * @type {string[]}
     */
    template: [
        "",
        "<div class=\"col-grid col-1-3\">",
        "",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-q\" for=\"q-select-input\">q</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-q-2\" for=\"q-select-input\">",
        "            <select aria-describedby=\"ada-q-select-describedby\" aria-labelledby=\"ada-q-select-describedby\" name=\"q\" class=\"form-control\" id=\"q-select-input\">",
        "                <option value=\"\"></option>",
        "                <option value=\"\">----- Other values -----</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "",
        "</div>",
        ""
    ]
};

module.exports.service_status = service_status;