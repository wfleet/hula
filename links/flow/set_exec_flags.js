/**
 * <h4>Link.flow.set_exec_flags<hr></h4>
 *
 * <p>Definition for the html_section link module</p>
 *
 * <p>Emits an HTML page section.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.flow.set_exec_flags
 * @version 0.1
 * @type {{name: string, fullname: string}}
 */

let set_exec_flags = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'set_exec_flags',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.flow.set_exec_flags',

    /**
     * value, setLinks, counter, count
     */
    value: true,
    setLinks: [],
    counter: 'count',
    count: 0,

    /**
     * <h4>fetch()<hr></h4>
     * <p>overrides the core method</p>
     *
     * @param workspace
     */
    fetch: function(workspace) {
        let chain = workspace.chainObject,
            theseLinks = chain.links,
            thisLink,
            i;

        // ensure we have a count value to count against, if not yet set
        if (!workspace[this.counter] || isNaN(parseInt(workspace[this.counter]))) {
            workspace[this.counter] = 0;
        }

        if (workspace[this.counter] == this.count) {
            for (i = 0; i < theseLinks.length; i++) {
                thisLink = theseLinks[i];
                if (this.value) {
                    if (this.setLinks.includes(thisLink.id)) {
                        workspace.chainObject.links[i].exec = true;
                    }
                    else {
                        workspace.chainObject.links[i].exec = false;
                    }
                }
                else {
                    if (this.setLinks.includes(thisLink.id)) {
                        workspace.chainObject.links[i].exec = false;
                    }
                    else {
                        workspace.chainObject.links[i].exec = true;
                    }
                }
            }
        }
    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'value',
        'setLinks',
        'counter',
        'count'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        '        <div class="help-line">when workspace.[counter] equals [count]:</div>',
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-counter\" for=\"counter-input\">counter</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"counter\" type=\"text\" class=\"form-control\" id=\"counter-input\" value=\"{{counter}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-count\" for=\"count-input\">count</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"count\" type=\"text\" class=\"form-control\" id=\"count-input\" value=\"{{count}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-value\" for=\"value-input\">value</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-value-2\" for=\"value-select-input\">",
        "            <select aria-describedby=\"ada-value-select-describedby\" aria-labelledby=\"ada-value-select-describedby\" name=\"value\" class=\"form-control\" id=\"value-select-input\">",
        "                <option value=\"{{value}}\">{{value}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"true\">true</option>",
        "                <option value=\"false\">false</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-setLinks\" for=\"setLinks-input\">setLinks</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"setLinks\" type=\"text\" class=\"form-control\" id=\"setLinks-input\" rows=\"6\">{{arrayText setLinks}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]


};

module.exports.set_exec_flags = set_exec_flags;

