/**
 * <h4>Link.flow.chain_repeat<hr></h4>
 *
 * <p>Definition for the html_section link module</p>
 *
 * <p>Emits an HTML page section.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.flow.chain_repeat
 * @version 0.1
 * @type {{name: string, fullname: string, times: number, counter: string, startIndex: number, endIndex: string, render: Link.flow.chain_repeat.render, customList: string[], template: string[]}}
 */

let chain_repeat = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'chain_repeat',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.flow.chain_repeat',

    /**
     * times, counter, startindex, endIndex
     */
    times: 1,
    counter: 'count',
    startIndex: 0,
    endIndex: '',

    /**
     * <h4>render()<hr></h4>
     * <p>overrides the core method</p>
     *
     * @returns {*}
     * @param workspace
     */
    render: function(workspace) {

	    // advance the counter - at first run it's likely undefined or zero
	    let thisCounter = workspace[this.counter];
	    if (thisCounter && !isNaN(parseInt(thisCounter))) {
            workspace[this.counter] = ++thisCounter;
        }
        else {
            workspace[this.counter] = 1;
        }

        // run the chain again
	    return Anklet.dispatch(workspace.chainObject, null, null, true, workspace, this.startIndex, this.endIndex);
    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'times',
        'counter',
        'startIndex',
        'endIndex'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        '    <div class="help-line">for an additional run of the chain, sets the zero-based start and end indices for this pass.</div>',
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-times\" for=\"times-input\">times</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"times\" type=\"text\" class=\"form-control\" id=\"times-input\" value=\"{{times}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-counter\" for=\"counter-input\">counter</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"counter\" type=\"text\" class=\"form-control\" id=\"counter-input\" value=\"{{counter}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-startIndex\" for=\"startIndex-input\">startIndex</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"startIndex\" type=\"text\" class=\"form-control\" id=\"startIndex-input\" value=\"{{startIndex}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-endIndex\" for=\"endIndex-input\">endIndex</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"endIndex\" type=\"text\" class=\"form-control\" id=\"endIndex-input\" value=\"{{endIndex}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]


};

module.exports.chain_repeat = chain_repeat;

