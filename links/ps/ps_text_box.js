/**
 * <h4>Link.ps.ps_text_box<hr></h4>
 *
 * <p>Definition for the ps_text_box link module</p>
 *
 * <p>Places a text box object in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_text_box
 * @version 0.1
 * @type {{name: string, fullname: string, font_name: string, font_size: string, line_height: string, font_color: string, start_x: string, start_y: string, width: string, content: Array, customList: string[], render: Link.ps.ps_text_box.render, template: string[]}}
 */

let ps_text_box = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_text_box',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_text_box',

    /**
     * <h4>top, right, bottom, left<hr></h4>
     * <p>box values</p>
     *
     * @type {string}
     */
    font_name: 'Helvetica',
    font_size: '12',
    line_height: '',
    font_color: '0',
    start_x: '',
    start_y: '',
    width: '',

    /**
     * <h4>content<hr></h4>
     * <p>Holder for the text box content</p>
     *
     * @type {Array}
     */
    content: [],

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'font_name',
        'font_size',
        'line_height',
        'start_x',
        'start_y',
        'width',
        'font_color',
        'content'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisPS = [
                this.font_color + " setgray",
                "/" + this.font_name + " findfont",
                this.font_size + " scalefont setfont",
                "",
                "/xline " + this.start_x + " def",
                "/yline " + this.start_y + " def",
                "/lineheight " + this.line_height + " def",
                ""
            ].join('\n'),
                thatPS = [
                this.width,
                "{NextLine}",
                "BreakLines",
                ""
            ].join('\n');
            let outPS = thisPS + '\n',
                thisRow;

            // add it to the psStreams object
            if (Array.isArray(this.content)) {
                for (thisRow of this.content) {
                    outPS += "\n(" + Anklet.LinkManager.interpolateValue(thisRow, workspace) + ")\n" + thatPS;
                }
            }
            else {
                outPS += "\n(" + Anklet.LinkManager.interpolateValue(this.content, workspace) + ")\n" + thatPS;
            }

            workspace.psStreams[this.id] = outPS;

        } catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"grid-30 align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"grid-70\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_height\" for=\"line_height-input\">line_height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_height\" type=\"text\" class=\"form-control\" id=\"line_height-input\" value=\"{{line_height}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_x\" for=\"start_x-input\">start_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_x\" type=\"text\" class=\"form-control\" id=\"start_x-input\" value=\"{{start_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_y\" for=\"start_y-input\">start_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_y\" type=\"text\" class=\"form-control\" id=\"start_y-input\" value=\"{{start_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-content\" for=\"content-input\">content</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"content\" type=\"text\" class=\"form-control\" id=\"content-input\" rows=\"10\">{{arrayText content}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]

};

module.exports.ps_text_box = ps_text_box;

