/**
 * <h4>Link.ps.ps_page_stream<hr></h4>
 *
 * <p>Definition for the ps_page_stream link module</p>
 *
 * <p>Takes a basic postscript stream, and passes it through for rendering.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_page_stream
 * @version 0.1
 * @type {{name: string, fullname: string, outputType: string, outputFilename: string, fetch: Link.ps.ps_page_stream.fetch}}
 */

let ps_page_stream = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_page_stream',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_page_stream',

    /**
     * outputType
     * @type {string}
     */
    outputType: '',

    /**
     * outputFilename
     * @type {string}
     */
    outputFilename: '',

    /**
     * psStream
     * @type {string}
     */
    content: [],

    /**
     * <h4>render()<hr></h4>
     * <p>sets up the file types, names and headers for a ps page</p>
     *
     * @param workspace
     * @returns {boolean}
     */
    render: function(workspace) {
	    try {
            const path = require('path'),
                hula_root = __dirname.replace(/hula\/.*/, 'hula'),
                randomNumber = Math.random().toString().replace('.', '');

            // ensure we have a place for ps streams
            workspace.psStreams = {};

            // start the ps stream
            workspace.psStreams[this.id] = Anklet.LinkManager.pseudoDecodeSafeText(this.content.join('\n'));

            // set the output path and type and name
            workspace.outputPath = path.join(hula_root, Anklet.Config.constants.outputFolder, '/output_' + randomNumber);
            workspace.outputType = this.outputType;
            workspace.outputFilename = this.outputFilename;

            // choose between png and pdf output headers
            if (this.outputType === 'png') {
                // it's a PNG
                workspace.outputPath = workspace.outputPath + '.png';
                workspace.outputWriter = 'png16m';
                workspace.outputHeaders = [
                    'Content-Type: image/png',
                    'Content-Disposition: inline; filename="' + workspace.outputFilename + '.png"'
                ];
            }
            else {
                // it's a PDF
                workspace.outputPath = workspace.outputPath + '.pdf';
                workspace.outputWriter = 'pdfwrite';
                workspace.outputHeaders = [
                    'Content-Type: application/pdf; charset=utf-8',
                    'Content-Disposition: inline; filename="' + workspace.outputFilename + '.pdf"'
                ];
            }
        }
        catch(error) {
            console.error(error);
	        throw error;
        }

        return true;

    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'outputType',
        'content'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template:
        [
            "<div class=\"col-grid col-1-3 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-outputType\" for=\"outputType-select-input\">outputType</label>",
            "    </div>",
            "    <div class=\"\">",
            "        <div class=\"form-group select\">",
            "            <label id=\"label-outputType-2\" for=\"outputType-select-input\">",
            "            <select aria-describedby=\"ada-outputType-select-describedby\" aria-labelledby=\"ada-outputType-select-describedby\" name=\"outputType\" class=\"form-control\" id=\"outputType-select-input\">",
            "                <option value=\"{{outputType}}\">{{outputType}}</option>",
            "                <option value=\"\">----- Other values -----</option>",
            "                <option value=\"pdf\">pdf</option>",
            "                <option value=\"png\">png</option>",
            "            </select>",
            "            </label>",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid\">",
            "    <div class=\"align-left area-name\">",
            "        <label id=\"label-content\" for=\"content-input\">content</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <textarea name=\"content\" type=\"text\" class=\"form-control\" id=\"content-input\" rows=\"10\">{{arrayText content}}</textarea>",
            "        </div>",
            "    </div>",
            "</div>",
            ""
        ]
};

module.exports.ps_page_stream = ps_page_stream;

