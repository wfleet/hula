/**
 * <h4>Link.ps.ps_place_eps<hr></h4>
 *
 * <p>Definition for the ps_place_eps link module</p>
 *
 * <p>Places a single text line object in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_place_eps
 * @version 0.1
 * @type {{name: string, fullname: string, font_name: string, font_size: string, font_color: string, font_x: string, font_y: string, text: string, customList: [string], render: Link.ps.ps_text_line.render, template: [string]}}
 */

let ps_place_eps = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_place_eps',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_place_eps',
    eps_x: '',
    eps_y: '',
    eps_scale: '',
    eps_rotate: '',
    eps_content: '',

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'eps_x',
        'eps_y',
        'eps_scale',
        'eps_rotate',
        'eps_content'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     * @returns {boolean}
     */
    render: function(workspace) {

        try {
            const thisPS = [
                    "BeginEPSF                  % Prepare for the included EPS file",
                    this.eps_x + " " + this.eps_y + " translate",
                    this.eps_scale + " " + this.eps_scale + " scale",
                    this.eps_rotate + " rotate",
                    "",
                    "%%BeginDocument: " + this.id + "_EPSFile",
                    ""
                ].join('\n'),


                thatPS = [
                    "",
                    "%%EndDocument",
                    "EndEPSF % Restore state, and cleanup stacks",
                    ""
                ].join('\n');

            let thisEPS = Anklet.LinkManager.resolveValue(this.eps_content, workspace);

            // strip out EPS binary header and foot
            if (thisEPS.indexOf('%!PS') > 0) {
                thisEPS = '%!PS' + thisEPS.split(/%!PS/)[1];
            }
            thisEPS = thisEPS.split(/%%EOF/)[0] + '%%EOF\n';

            // add it to the psStreams object
            workspace.psStreams[this.id] = [this.placeEPSHeaders.join('\n'), thisPS, thisEPS, thatPS].join('\n');

        } catch (error) {
            console.error(error);
            throw error;
        }
    },

    /**
     * <h4>placeEPSHeaders<hr></h4>
     * <p>a snippet of PS to build the EPS</p>
     *
     * @type {string[]}
     */
    placeEPSHeaders:
        [
            "/BeginEPSF {",
            "	/b4_Inc_state save def",
            "	/dict_count countdictstack def",
            "	/op_count count 1 sub def",
            "	userdict begin",
            "	/showpage { } def",
            "	0 setgray 0 setlinecap",
            "	1 setlinewidth 0 setlinejoin",
            "	10 setmiterlimit [ ] 0 setdash newpath",
            "	/languagelevel where",
            "	{pop languagelevel",
            "	1 ne",
            "		{false setstrokeadjust false setoverprint",
            "		} if",
            "	} if",
            "} bind def",
            "",
            "/EndEPSF {",
            "	count op_count sub {pop} repeat",
            "	countdictstack dict_count sub {end} repeat",
            "	b4_Inc_state restore",
            "} bind def",
            "",
            ""
        ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page. <br>Rotation is in degrees, counterclockwise.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-eps_x\" for=\"eps_x-input\">eps_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"eps_x\" type=\"text\" class=\"form-control\" id=\"eps_x-input\" value=\"{{eps_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-eps_y\" for=\"eps_y-input\">eps_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"eps_y\" type=\"text\" class=\"form-control\" id=\"eps_y-input\" value=\"{{eps_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-eps_scale\" for=\"eps_scale-input\">eps_scale</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"eps_scale\" type=\"text\" class=\"form-control\" id=\"eps_scale-input\" value=\"{{eps_scale}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-eps_rotate\" for=\"eps_rotate-input\">eps_rotate</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"eps_rotate\" type=\"text\" class=\"form-control\" id=\"eps_rotate-input\" value=\"{{eps_rotate}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid\">",
        "    <div class=\"align-left area-name\">",
        "        <label id=\"label-eps_content\" for=\"eps_content-input\">eps_content</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <textarea name=\"eps_content\" type=\"text\" class=\"form-control\" id=\"eps_content-input\" rows=\"5\">{{arrayText eps_content}}</textarea>",
        "        </div>",
        "    </div>",
        "</div>",
    ]


};

module.exports.ps_place_eps = ps_place_eps;

