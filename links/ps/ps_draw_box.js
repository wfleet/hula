/**
 * <h4>Link.ps.ps_draw_box<hr></h4>
 *
 * <p>Definition for the ps_text_box link module</p>
 *
 * <p>Places a text box object in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_draw_box
 * @version 0.1
 * @type {{name: string, fullname: string, top: string, right: string, bottom: string, left: string, width: string, render: Link.ps.ps_draw_box.render}}
 */

let ps_draw_box = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_draw_box',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_draw_box',

    /**
     * <h4>top, right, bottom, left, width<hr></h4>
     * <p>box values</p>
     *
     * @type {string}
     */
    top: '',
    right: '',
    bottom: '',
    left: '',
    width: '',

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     * @returns {boolean}
     */
    render: function(workspace) {

        try {
            const thisPS = [
                "0 setgray",
                "newpath",
                this.right + " " + this.top + " moveto",
                this.left + " " + this.top + " lineto",
                this.left + " " + this.bottom + " lineto",
                this.right + " " + this.bottom + " lineto",
                "closepath",
                this.width + " setlinewidth",
                "stroke",
                ""
            ].join('\n');

            // add it to the psStreams object
            workspace.psStreams[this.id] = thisPS;

        } catch (error) {
            console.error(error);
            throw error;
        }

	    return true;
    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'top',
        'right',
        'bottom',
        'left',
        'width'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-left\" for=\"left-input\">left</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"left\" type=\"text\" class=\"form-control\" id=\"left-input\" value=\"{{left}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-top\" for=\"top-input\">top</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"top\" type=\"text\" class=\"form-control\" id=\"top-input\" value=\"{{top}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-right\" for=\"right-input\">right</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"right\" type=\"text\" class=\"form-control\" id=\"right-input\" value=\"{{right}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bottom\" for=\"bottom-input\">bottom</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bottom\" type=\"text\" class=\"form-control\" id=\"bottom-input\" value=\"{{bottom}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]


};

module.exports.ps_draw_box = ps_draw_box;

