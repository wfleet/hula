/**
 * <h4>Link.ps.ps_key_value_list<hr></h4>
 *
 * <p>Definition for the ps_key_value_list link module</p>
 *
 * <p>Places a list of key-value pair items in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_key_value_list
 * @version 0.1
 * @type {{name: string, fullname: string, col_1_font: string, col_2_font: string, font_size: string, line_height: string, start_x: string, start_y: string, width: string, col_type: string, font_color: string, content: Array, customList: string[], render: Link.ps.ps_key_value_list.render, template: string[]}}
 */

let ps_key_value_list = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_key_value_list',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_key_value_list',

    /**
     * <h4>top, right, bottom, left<hr></h4>
     * <p>box values</p>
     *
     * @type {string}
     */
    col_1_font: 'Helvetica',
    col_2_font: 'Helvetica-Bold',
    font_size: '12',
    line_height: '',
    start_x: '',
    start_y: '',
    width: '',
    col_type: '',
    font_color: '0',
    key_label: 'Key',
    value_label: 'Value',

    /**
     * <h4>content<hr></h4>
     * <p>Holder for the text box content</p>
     *
     * @type {Array}
     */
    content: [],

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'col_1_font',
        'col_2_font',
        'font_size',
        'line_height',
        'start_x',
        'start_y',
        'width',
        'col_type',
        'font_color',
        'key_label',
        'value_label',
        'content'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     * <p>yes, we're using Handlebars to compile postscript streams</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const Handlebars = require('handlebars'),
                startObj = {
                    'font_color': this.font_color,
                    'startX': this.start_x,
                    'startY': this.start_y,
                    'lineheight': this.line_height,
                    'width': this.width
                },
                startTemplate = [
                    "{{font_color}} setgray",
                    "/xline {{startX}} def",
                    "/yline {{startY}} def",
                    "/xright {{startX}} {{width}} add def",
                    "/xcentright {{width}} 2 div 5 add {{startX}} add def",
                    "/xcentleft {{width}} 2 div 5 sub {{startX}} add def",
                    "/lineheight {{lineheight}} def",
                    "/colwidth {{width}} def",
                    "/rightline { dup stringwidth pop xright exch sub yline moveto show} def",
                    "/centleftline { dup stringwidth pop xcentleft exch sub yline moveto show} def",
                    ""
                ].join('\n'),
                rowTemplate = [
                    "xline yline moveto",
                    "/{{col1font}} findfont {{fontSize}} scalefont setfont",
                    "({{key}}: ) show",
                    "/{{col2font}} findfont {{fontSize}} scalefont setfont",
                    "({{value}}) show",
                    "/yline yline lineheight sub def",
                    ""
                ].join('\n'),
                wideRowTemplate = [
                    "xline yline moveto",
                    "/{{col1font}} findfont {{fontSize}} scalefont setfont",
                    "({{key}}: ) show",
                    "/{{col2font}} findfont {{fontSize}} scalefont setfont",
                    "({{value}}) rightline",
                    "/yline yline lineheight sub def",
                    ""
                ].join('\n'),
                centerRowTemplate = [
                    "xline yline moveto",
                    "/{{col1font}} findfont {{fontSize}} scalefont setfont",
                    "({{key}}) centleftline",
                    "xcentright yline moveto",
                    "/{{col2font}} findfont {{fontSize}} scalefont setfont",
                    "({{value}}) show",
                    "/yline yline lineheight sub def",
                    ""
                ].join('\n'),
                thisContent = Anklet.LinkManager.resolveValue(this.content, workspace),
                thisStartTemplate = Handlebars.compile(startTemplate);

            let outPS = thisStartTemplate(startObj) + '\n',
                rowObj = {
                    'col1font': this.col_1_font,
                    'col2font': this.col_2_font,
                    'fontSize': this.font_size
                },
                thisRowTemplate,
                thisRow;

            // wide or left justified?
            if (this.col_type == 'wide') {
                thisRowTemplate = Handlebars.compile(wideRowTemplate);
            }
            else if (this.col_type == 'center') {
                thisRowTemplate = Handlebars.compile(centerRowTemplate);
            }
            else {
                thisRowTemplate = Handlebars.compile(rowTemplate);
            }

            for (thisRow of thisContent) {
                rowObj.key = thisRow[this.key_label];
                rowObj.value = thisRow[this.value_label];

                outPS += thisRowTemplate(rowObj);
            }

            // console.log(outPS);
            workspace.psStreams[this.id] = outPS;

        } catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-col_1_font\" for=\"col_1_font-select-input\">col_1_font</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-col_1_font-2\" for=\"col_1_font-select-input\">",
        "            <select aria-describedby=\"ada-col_1_font-select-describedby\" aria-labelledby=\"ada-col_1_font-select-describedby\" name=\"col_1_font\" class=\"form-control\" id=\"col_1_font-select-input\">",
        "                <option value=\"{{col_1_font}}\">{{col_1_font}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-col_2_font\" for=\"col_2_font-select-input\">col_2_font</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-col_2_font-2\" for=\"col_2_font-select-input\">",
        "            <select aria-describedby=\"ada-col_2_font-select-describedby\" aria-labelledby=\"ada-col_2_font-select-describedby\" name=\"col_2_font\" class=\"form-control\" id=\"col_2_font-select-input\">",
        "                <option value=\"{{col_2_font}}\">{{col_2_font}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_height\" for=\"line_height-input\">line_height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_height\" type=\"text\" class=\"form-control\" id=\"line_height-input\" value=\"{{line_height}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_x\" for=\"start_x-input\">start_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_x\" type=\"text\" class=\"form-control\" id=\"start_x-input\" value=\"{{start_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_y\" for=\"start_y-input\">start_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_y\" type=\"text\" class=\"form-control\" id=\"start_y-input\" value=\"{{start_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-key_label\" for=\"key_label-input\">key_label</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"key_label\" type=\"text\" class=\"form-control\" id=\"key_label-input\" value=\"{{key_label}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-value_label\" for=\"value_label-input\">value_label</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"value_label\" type=\"text\" class=\"form-control\" id=\"value_label-input\" value=\"{{value_label}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-col_type\" for=\"col_type-input\">col_type</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <select aria-describedby=\"ada-col_type-select-describedby\" aria-labelledby=\"ada-col_type-select-describedby\" name=\"col_type\" class=\"form-control\" id=\"col_type-select-input\">",
        "                <option value=\"{{col_type}}\">{{col_type}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"left\">left</option>",
        "                <option value=\"wide\">wide</option>",
        "                <option value=\"center\">center</option>",
        "            </select>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-content\" for=\"content-input\">content</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"content\" type=\"text\" class=\"form-control\" id=\"content-input\" value=\"{{content}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]

};

module.exports.ps_key_value_list = ps_key_value_list;

