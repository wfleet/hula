/**
 * <h4>Link.ps.render_ps_stream<hr></h4>
 *
 * <p>Definition for the render_ps_stream link module</p>
 *
 * <p>Closes the open postscript stream, calls the render engine, and emits the result.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.render_ps_stream
 * @version 0.1
 * @type {{name: string, fullname: string, render: Link.ps.render_ps_stream.render}}
 */

let render_ps_stream = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'render_ps_stream',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.render_ps_stream',

    /**
     * <h4>runGS()<hr></h4>
     * <p>runs the GhostScript PS interpreter as a child process and returns its output as a Buffer</p>
     *
     * @param thisPS
     * @param thisWriter
     * @returns {Buffer}
     */
    runGS: function(thisPS, thisWriter) {

        const { spawnSync } = require('child_process'),
            fs = require('fs');

        // ensure we've closed tha PS page stream
        thisPS += '\nshowpage\n';

        let args = [],
            options = {
                input: thisPS,
                stdio: 'pipe'
            },
            thisOutput,
            thisBuffer,
            thisErrorOut;

        if (thisWriter === 'pdfwrite') {
            args = [
                '-dSAFER',
                '-dBATCH',
                '-dNOPAUSE',
                '-sDEVICE=pdfwrite',
                '-sOutputFile=-',
                '-q',
                '-_'
            ]
        }
        else {
            args = [
                '-dSAFER',
                '-dBATCH',
                '-dNOPAUSE',
                '-sDEVICE=' + thisWriter,
                '-dTextAlphaBits=4',
                '-dGraphicsAlphaBits=4',
                '-sOutputFile=-',
                '-q',
                '-_'
            ]
        }
        thisOutput = spawnSync('gs', args, options);
	    thisBuffer = new Buffer( new Uint8Array(thisOutput.output[1]) );
	    thisErrorOut = new Buffer( new Uint8Array(thisOutput.output[2]) );
	    if (thisErrorOut.length > 0) {
            console.error(thisErrorOut.toString());
        }

	    return thisBuffer;
    },

    /**
     * <h4>render()<hr></h4>
     * <p>compiles the various ps streams in order, and gets ghostscript to run them</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        const thisWriter = workspace.outputWriter;

        try {
            let thisPS = '',
                thisLink,
                thisId,
                thisStream,
                thisOutput;

            // get the various ps streams in order
            for (thisLink of workspace.chainObject.links) {
                thisId = thisLink.id;
                if (workspace.psStreams[thisId]) {
                    thisStream = workspace.psStreams[thisId];
                    if (thisStream.indexOf('%!PS-') === 0) {
                        // it's the header section, don't wrap it
                        thisPS = thisStream + '\n\n%%EndHeaderSection: Link-' + thisId + '\n\n';
                    }
                    else {
                        thisPS += '%%BeginSection: Link-' + thisId + '\ngsave\n\n' + workspace.psStreams[thisId] + '\n\ngrestore\n%%EndSection: Link-' + thisId + '\n\n';
                    }
                }
            }

            // push this output into the output object
            thisOutput = this.runGS(thisPS, thisWriter);
            workspace.outputStream = thisOutput;

        }
        catch(error) {
            console.error(error);
	        throw error;
        }

    }
};

module.exports.render_ps_stream = render_ps_stream;

