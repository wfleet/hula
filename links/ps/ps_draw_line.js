/**
 * <h4>Link.ps.ps_draw_line<hr></h4>
 *
 * <p>Definition for the ps_text_box link module</p>
 *
 * <p>Places a text box object in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_draw_line
 * @version 0.1
 * @type {{name: string, fullname: string, start_x: string, start_y: string, end_x: string, end_y: string, width: string, render: Link.ps.ps_draw_line.render, customList: [string , string , string , string , string], template: [string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string]}}
 */

let ps_draw_line = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_draw_line',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_draw_line',

    /**
     * <h4>start_x, start_y, end_x, end_y, width<hr></h4>
     * <p>line values</p>
     *
     * @type {string}
     */
    start_x: '',
    start_y: '',
    end_x: '',
    end_y: '',
    width: '',

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined line</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisPS = [
                "0 setgray",
                "newpath",
                this.start_x + " " + this.start_y + " moveto",
                this.end_x + " " + this.end_y + " lineto",
                this.width + " setlinewidth",
                "0 setgray",
                "stroke",
                ""
            ].join('\n');

            // add it to the psStreams object
            workspace.psStreams[this.id] = thisPS;

        } catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'start_x',
        'start_y',
        'end_x',
        'end_y',
        'width'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_x\" for=\"start_x-input\">start_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_x\" type=\"text\" class=\"form-control\" id=\"start_x-input\" value=\"{{start_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_y\" for=\"start_y-input\">start_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_y\" type=\"text\" class=\"form-control\" id=\"start_y-input\" value=\"{{start_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-end_x\" for=\"end_x-input\">end_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"end_x\" type=\"text\" class=\"form-control\" id=\"end_x-input\" value=\"{{end_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-end_y\" for=\"end_y-input\">end_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"end_y\" type=\"text\" class=\"form-control\" id=\"end_y-input\" value=\"{{end_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]


};

module.exports.ps_draw_line = ps_draw_line;

