/**
 * <h4>Link.ps.ps_text_line<hr></h4>
 *
 * <p>Definition for the ps_text_line link module</p>
 *
 * <p>Places a single text line object in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_text_line
 * @version 0.1
 * @type {{name: string, fullname: string, font_name: string, font_size: string, font_color: string, font_x: string, font_y: string, text: string, customList: [string , string , string , string , string], render: Link.ps.ps_text_line.render, template: [string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string , string]}}
 */

let ps_text_line = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_text_line',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_text_line',

    font_name: 'Helvetica',
    font_size: '12',
    font_color: '0',
    font_x: '',
    font_y: '',
    text: '',

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'font_name',
        'font_size',
        'font_color',
        'font_x',
        'font_y'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisLine = Anklet.LinkManager.interpolateValue(this.text, workspace),
                thisPS = [
                this.font_color + " setgray",
                "/" + this.font_name + " findfont",
                this.font_size + " scalefont setfont",
                this.font_x + " " + this.font_y + " moveto",
                "(" + thisLine + ") show",
                ""
            ].join('\n');

            // add it to the psStreams object
            if (thisLine && thisLine !== 'undefined') {
                workspace.psStreams[this.id] = thisPS;
            }

        } catch (error) {
            console.error(error);
            throw error;
        }

    },


    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_x\" for=\"font_x-input\">font_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_x\" type=\"text\" class=\"form-control\" id=\"font_x-input\" value=\"{{font_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_y\" for=\"font_y-input\">font_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_y\" type=\"text\" class=\"form-control\" id=\"font_y-input\" value=\"{{font_y}}\">",
        "        </div>",
        "    </div>",
        "</div>"
    ]
};

module.exports.ps_text_line = ps_text_line;

