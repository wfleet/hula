/**
 * <h4>Link.ps.ps_text_bullets<hr></h4>
 *
 * <p>Definition for the ps_text_bullets link module</p>
 *
 * <p>Places a series of bulleted text items in the ps stream and sets its content and appearance.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.ps_text_bullets
 * @version 0.1
 * @type {{name: string, fullname: string, font_name: string, font_size: string, font_color: string, start_x: string, start_y: string, width: string, line_height: string, bullet_type: string, bullet_size: string, bullet_indent: string, bullet_space: string, content: Array, customList: string[], render: Link.ps.ps_text_bullets.render, template: string[]}}
 */

let ps_text_bullets = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'ps_text_bullets',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.ps_text_bullets',

    /**
     * <h4>top, right, bottom, left<hr></h4>
     * <p>box values</p>
     *
     * @type {string}
     */
    font_name: 'Helvetica',
    font_size: '12',
    font_color: '0',
    start_x: '',
    start_y: '',
    width: '',
    line_height: '14',
    bullet_type: 'round',
    bullet_size: '6',
    bullet_indent: '15',
    bullet_space: '3',

    /**
     * <h4>content<hr></h4>
     * <p>Holder for the text box content</p>
     *
     * @type {Array}
     */
    content: [],

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     *
     * @type {string[]}
     */
    customList: [
        'font_name',
        'font_size',
        'font_color',
        'start_x',
        'start_y',
        'width',
        'line_height',
        'bullet_type',
        'bullet_size',
        'bullet_indent',
        'bullet_space',
        'content'
    ],

    /**
     * <h4>render()<hr></h4>
     * <p>builds a ps stream for the defined box</p>
     * <p>saves it in workspace.psStreams by id</p>
     *
     * @param workspace
     */
    render: function(workspace) {

        try {
            const thisPS = [
                this.font_color + " setgray",
                "/" + this.font_name + " findfont",
                this.font_size + " scalefont setfont",
                "",
                "/xline " + (parseInt(this.start_x) + parseInt(this.bullet_indent)).toString() + " def",
                "/yline " + this.start_y + " def",
                "/lineheight " + this.line_height + " def",
                "\n"
            ].join('\n'),
                thatPS = [
                    (parseInt(this.width) - parseInt(this.bullet_indent)).toString(),
                "{NextLine}",
                "BreakLines",
                ""
            ].join('\n');
            let outPS = thisPS + '\n',
                thisContent = Anklet.LinkManager.interpolateValue(this.content, workspace),
                bullet_x,
                bullet_y,
                bullet_radius,
                thisRow;

            // add it to the psStreams object
            if (Array.isArray(thisContent)) {
                // figure out offsets
                bullet_radius = parseInt(this.bullet_size) / 2;
                bullet_x = parseInt(this.bullet_indent) - bullet_radius;
                bullet_y = bullet_radius + ((parseInt(this.font_size) - bullet_radius) / 8);

                for (thisRow of thisContent) {
                    // add the bullet
                    outPS += "newpath xline " + bullet_x + " sub yline " + bullet_y + " add moveto\n";
                    outPS += "xline " + bullet_x + " sub yline " + bullet_y + " add " + bullet_radius.toString() + " 0 360 arc closepath 0 setgray fill\n";
                    // show the bullet content
                    outPS += "\n(" + thisRow + " )\n" + thatPS;
                    // add the leading value
                    outPS += "/yline yline " + this.bullet_space + " sub def\n";
                }
            }
            else {
                // add the bullet
                outPS += "newpath xline " + bullet_x + " sub yline " + bullet_y + " add moveto\n";
                outPS += "xline " + bullet_x + " sub yline " + bullet_y + " add " + bullet_radius.toString() + " 0 360 arc closepath 0 setgray fill\n";
                // show the bullet content
                outPS += "\n(" + thisContent + " )\n" + thatPS;
            }

            // console.log(outPS);
            workspace.psStreams[this.id] = outPS;

        } catch (error) {
            console.error(error);
            throw error;
        }

    },

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template: [
        "<div class=\"after-10\">",
        "    <div class=\"help-line\">",
        "        Values are in points (1/72\"), and zero point is at bottom left of the page.",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-3 link-field\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_name\" for=\"font_name-select-input\">font_name</label>",
        "    </div>",
        "    <div class=\"\">",
        "        <div class=\"form-group select\">",
        "            <label id=\"label-font_name-2\" for=\"font_name-select-input\">",
        "            <select aria-describedby=\"ada-font_name-select-describedby\" aria-labelledby=\"ada-font_name-select-describedby\" name=\"font_name\" class=\"form-control\" id=\"font_name-select-input\">",
        "                <option value=\"{{font_name}}\">{{font_name}}</option>",
        "                <option value=\"\">----- Other values -----</option>",
        "                <option value=\"Helvetica\">Helvetica</option>",
        "                <option value=\"Helvetica-Oblique\">Helvetica-Oblique</option>",
        "                <option value=\"Helvetica-Bold\">Helvetica-Bold</option>",
        "                <option value=\"Helvetica-BoldOblique\">Helvetica-BoldOblique</option>",
        "                <option value=\"Times-Roman\">Times-Roman</option>",
        "                <option value=\"Times-Italic\">Times-Italic</option>",
        "                <option value=\"Times-Bold\">Times-Bold</option>",
        "                <option value=\"Times-BoldItalic\">Times-BoldItalic</option>",
        "                <option value=\"Courier\">Courier</option>",
        "                <option value=\"Courier-Oblique\">Courier-Oblique</option>",
        "                <option value=\"Courier-Bold\">Courier-Bold</option>",
        "                <option value=\"Courier-BoldOblique\">Courier-BoldOblique</option>",
        "                <option value=\"Symbol\">Symbol</option>",
        "                <option value=\"ZapfDingbats\">ZapfDingbats</option>",
        "            </select>",
        "            </label>",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_size\" for=\"font_size-input\">font_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_size\" type=\"text\" class=\"form-control\" id=\"font_size-input\" value=\"{{font_size}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-line_height\" for=\"line_height-input\">line_height</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"line_height\" type=\"text\" class=\"form-control\" id=\"line_height-input\" value=\"{{line_height}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_x\" for=\"start_x-input\">start_x</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_x\" type=\"text\" class=\"form-control\" id=\"start_x-input\" value=\"{{start_x}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-start_y\" for=\"start_y-input\">start_y</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"start_y\" type=\"text\" class=\"form-control\" id=\"start_y-input\" value=\"{{start_y}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-width\" for=\"width-input\">width</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"width\" type=\"text\" class=\"form-control\" id=\"width-input\" value=\"{{width}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-font_color\" for=\"font_color-input\">font_color</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"font_color\" type=\"text\" class=\"form-control\" id=\"font_color-input\" value=\"{{font_color}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"after-10\">",
        "    <hr />",
        "</div>",
        "<div class=\"col-grid col-1-3\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-content\" for=\"content-input\">content</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"content\" type=\"text\" class=\"form-control\" id=\"content-input\" value=\"{{content}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"after-10\">",
        "    <hr />",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet_type\" for=\"bullet_type-input\">bullet_type</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bullet_type\" type=\"text\" class=\"form-control\" id=\"bullet_type-input\" value=\"{{bullet_type}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet_size\" for=\"bullet_size-input\">bullet_size</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bullet_size\" type=\"text\" class=\"form-control\" id=\"bullet_size-input\" value=\"{{bullet_size}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        "<div class=\"col-grid col-1-1-1-1\">",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet_indent\" for=\"bullet_indent-input\">bullet_indent</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bullet_indent\" type=\"text\" class=\"form-control\" id=\"bullet_indent-input\" value=\"{{bullet_indent}}\">",
        "        </div>",
        "    </div>",
        "    <div class=\"align-right field-name\">",
        "        <label id=\"label-bullet_space\" for=\"bullet_space-input\">bullet_space</label>",
        "    </div>",
        "    <div class=\"link-field\">",
        "        <div class=\"form-group\">",
        "            <input name=\"bullet_space\" type=\"text\" class=\"form-control\" id=\"bullet_space-input\" value=\"{{bullet_space}}\">",
        "        </div>",
        "    </div>",
        "</div>",
        ""
    ]

};

module.exports.ps_text_bullets = ps_text_bullets;

