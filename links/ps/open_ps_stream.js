/**
 * <h4>Link.ps.open_ps_stream<hr></h4>
 *
 * <p>Definition for the open_ps_stream link module</p>
 *
 * <p>Begins a basic postscript stream, sets page size and other params, and gets ready for objects to draw.</p>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Link.ps.open_ps_stream
 * @version 0.1
 * @type {{name: string, fullname: string, outputType: string, outputFilename: string, fetch: Link.ps.open_ps_stream.fetch}}
 */

let open_ps_stream = {

	/**
	 * <h4>name<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	name: 'open_ps_stream',

	/**
	 * <h4>fullname<hr></h4>
	 * <p>may be used for self-reference, but primarily here so I can be sure of what I'm actually debugging</p>
	 * @type {string}
	 */
	fullname: 'Anklet.links.ps.open_ps_stream',

    /**
     * outputType
     * @type {string}
     */
    outputType: '',

    /**
     * outputFilename
     * @type {string}
     */
    outputFilename: '',

    /**
     * <h4>render()<hr></h4>
     * <p>sets up the file types, names and headers for a ps page</p>
     *
     * @param workspace
     */
    render: function(workspace) {
	    try {
            const path = require('path'),
                hula_root = __dirname.replace(/hula\/.*/, 'hula'),
                randomNumber = Math.random().toString().replace('.', '');

            // ensure we have a place for ps streams
            workspace.psStreams = {};

            // start the ps stream
            workspace.psStreams[this.id] = [
                "%!PS-Adobe-3.0",
                "% PS Stream from hula",
                "% ",
                "% set page size",
                "/pageWidth " + this.pageWidth + " def",
                "/pageHeight " + this.pageHeight + " def",
                "<<  /PageSize [pageWidth pageHeight] ",
                "    /ImagingBBox null >> setpagedevice\n",
                "",
                "% ",
                "% start the gspace and save it",
                "newpath gsave",
                "",
                "% ",
                "% define the BreakLines function",
                "% ",
                "% to use, push in the content string, the width in points, the function to move to the next line,",
                "% and then the BreakLines function",
                "% ",
                "/wordbreak ( ) def",
                "/BreakLines",
                "  { /proc exch def",
                "	/linelength exch def",
                "	/textstring exch def",
                "",
                "	/breakwidth wordbreak stringwidth pop def",
                "	/curwidth 0 def",
                "	/lastwordbreak 0 def",
                "	",
                "	/startchar 0 def",
                "	/restoftext textstring def",
                "	",
                "	{ restoftext wordbreak search",
                "	  { /nextword exch def pop",
                "		/restoftext exch def",
                "		/wordwidth nextword stringwidth pop def",
                "		",
                "		curwidth wordwidth add linelength gt",
                "		{ textstring startchar",
                "		  lastwordbreak startchar sub",
                "		  getinterval proc",
                "		  /startchar lastwordbreak def",
                "		  /curwidth wordwidth breakwidth add def }",
                "		{ /curwidth curwidth wordwidth add",
                "			breakwidth add def ",
                "		} ifelse",
                "",
                "		/lastwordbreak lastwordbreak",
                "		  nextword length add 1 add def",
                "		}",
                "		{pop exit}",
                "		ifelse",
                "	  } loop",
                "	/lastchar textstring length def",
                "	textstring startchar lastchar startchar sub ",
                "	  getinterval proc",
                "  } def",
                "",
                "% ",
                "% the NextLine function, for use by BreakLines",
                "/NextLine { xline yline moveto show /yline yline lineheight sub def } def",
                ""
            ].join('\n');

            // set the output path and type and name
            workspace.outputPath = path.join(hula_root, Anklet.Config.constants.outputFolder, '/output_' + randomNumber);
            workspace.outputType = this.outputType;
            workspace.outputFilename = this.outputFilename;

            // choose between png and pdf output headers
            if (this.outputType === 'png') {
                // it's a PNG
                workspace.outputPath = workspace.outputPath + '.png';
                workspace.outputWriter = 'png16m';
                workspace.outputHeaders = [
                    'Content-Type: image/png',
                    'Content-Disposition: inline; filename="' + workspace.outputFilename + '.png"'
                ];
            }
            else {
                // it's a PDF
                workspace.outputPath = workspace.outputPath + '.pdf';
                workspace.outputWriter = 'pdfwrite';
                workspace.outputHeaders = [
                    'Content-Type: application/pdf; charset=utf-8',
                    'Content-Disposition: inline; filename="' + workspace.outputFilename + '.pdf"'
                ];
            }
        }
        catch(error) {
            console.error(error);
	        throw error;
        }

    },

    /**
     * <h4>customList<hr></h4>
     * <p>static list of field names used in the custom template</p>
     * <p>if it's used in the template, put it here, otherwise it will repeat in the body of the edit panel</p>
     *
     * @type {string[]}
     */
    customList: [
        'outputType',
        'pageWidth',
        'pageHeight'
    ],

    /**
     * <h4>template<hr></h4>
     * <p>The custom template, for those special fields</p>
     *
     * @type {string[]}
     */
    template:
        [
            "<div class=\"after-10\">",
            "    <div class=\"help-line\">",
            "        Values are in points (1/72\"). Presets: <a href=\"#\" class=\"preset\">letter</a> : <a href=\"#\" class=\"preset\">legal</a> : <a href=\"#\" class=\"preset\">tabloid</a> - Rotate: <a href=\"#\" class=\"preset\">swap</a>",
            "    </div>",
            "</div>",
            "<div class=\"col-grid col-1-1-1-1\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-pageWidth\" for=\"pageWidth-input\">pageWidth</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"pageWidth\" type=\"text\" class=\"form-control\" id=\"pageWidth-input\" value=\"{{pageWidth}}\">",
            "        </div>",
            "    </div>",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-pageHeight\" for=\"pageHeight-input\">pageHeight</label>",
            "    </div>",
            "    <div class=\"link-field\">",
            "        <div class=\"form-group\">",
            "            <input name=\"pageHeight\" type=\"text\" class=\"form-control\" id=\"pageHeight-input\" value=\"{{pageHeight}}\">",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<div class=\"col-grid col-1-3 link-field\">",
            "    <div class=\"align-right field-name\">",
            "        <label id=\"label-outputType\" for=\"outputType-select-input\">outputType</label>",
            "    </div>",
            "    <div class=\"\">",
            "        <div class=\"form-group select\">",
            "            <label id=\"label-outputType-2\" for=\"outputType-select-input\">",
            "            <select aria-describedby=\"ada-outputType-select-describedby\" aria-labelledby=\"ada-outputType-select-describedby\" name=\"outputType\" class=\"form-control\" id=\"outputType-select-input\">",
            "                <option value=\"{{outputType}}\">{{outputType}}</option>",
            "                <option value=\"\">----- Other values -----</option>",
            "                <option value=\"pdf\">pdf</option>",
            "                <option value=\"png\">png</option>",
            "            </select>",
            "            </label>",
            "        </div>",
            "    </div>",
            "</div>",
            "",
            "<script type=\"application/javascript\">",
            "    (function($) {",
            "        // attach event handlers to the presets",
            "        $('.preset').on('click', function(event) {",
            "            var thisElement = event.currentTarget;",
            "",
            "            switch (thisElement.innerHTML) {",
            "                case 'letter':",
            "                    $('#pageWidth-input').val(612);",
            "                    $('#pageHeight-input').val(792);",
            "                    break;",
            "                case 'legal':",
            "                    $('#pageWidth-input').val(612);",
            "                    $('#pageHeight-input').val(1008);",
            "                    break;",
            "                case 'tabloid':",
            "                    $('#pageWidth-input').val(792);",
            "                    $('#pageHeight-input').val(1224);",
            "                    break;",
            "                case 'swap':",
            "                    var thisValue = $('#pageWidth-input').val();",
            "                    $('#pageWidth-input').val($('#pageHeight-input').val());",
            "                    $('#pageHeight-input').val(thisValue);",
            "                    break;",
            "                default:",
            "                    $('#pageWidth-input').val(612);",
            "                    $('#pageHeight-input').val(792);",
            "            };",
            "        });",
            "",
            "    })(jQuery);",
            "</script>",
            ""
        ]
};

module.exports.open_ps_stream = open_ps_stream;

