/**
 * <h4>Utils<hr></h4>
 *
 * <p>The Utils componenet for Anklet web calls.</p>
 * <ul>
 *     <li>Utils allows Developers to edit Chains more conveniently
 *     <li>Utils provides a GUI (web) front end to visualize chains
 *     <li>Utils is optional; if it is not needed, the /Utils folder can be omitted
 * </ul>
 *
 * @copyright 2017 William Fleet, Lowe's Companies. All rights reserved.
 * @author William Fleet <william.h.fleet@lowes.com>
 * @class Utils
 * @version 0.1
 * @type {{}}
 */
let Utils = {

};

module.exports.Utils = Utils;
